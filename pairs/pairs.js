(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 637,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/_123_nums.png", id:"_123_nums"},
		{src:"images/bg.png", id:"bg"},
		{src:"images/cards4x4.png", id:"cards4x4"},
		{src:"images/cards4x4_bars.png", id:"cards4x4_bars"},
		{src:"images/cards5x4.png", id:"cards5x4"},
		{src:"images/cards5x4_bars.png", id:"cards5x4_bars"},
		{src:"images/cards6x5.png", id:"cards6x5"},
		{src:"images/cards6x5_bars.png", id:"cards6x5_bars"},
		{src:"images/images.png", id:"images"},
		{src:"images/level1_0001.png", id:"level1_0001"},
		{src:"images/level1_0002.png", id:"level1_0002"},
		{src:"images/level1_0003.png", id:"level1_0003"},
		{src:"images/level1_0004.png", id:"level1_0004"},
		{src:"images/level1_0005.png", id:"level1_0005"},
		{src:"images/level1_0006.png", id:"level1_0006"},
		{src:"images/level1_0007.png", id:"level1_0007"},
		{src:"images/level1_0008.png", id:"level1_0008"},
		{src:"images/level1_0009.png", id:"level1_0009"},
		{src:"images/level1_0010.png", id:"level1_0010"},
		{src:"images/level1_0011.png", id:"level1_0011"},
		{src:"images/level1_0012.png", id:"level1_0012"},
		{src:"images/level1_0013.png", id:"level1_0013"},
		{src:"images/level1_0014.png", id:"level1_0014"},
		{src:"images/level1_0015.png", id:"level1_0015"},
		{src:"images/level1_0016.png", id:"level1_0016"},
		{src:"images/level1_0017.png", id:"level1_0017"},
		{src:"images/level1_0018.png", id:"level1_0018"},
		{src:"images/level1_0019.png", id:"level1_0019"},
		{src:"images/level1_0020.png", id:"level1_0020"},
		{src:"images/level1_0021.png", id:"level1_0021"},
		{src:"images/level1_0022.png", id:"level1_0022"},
		{src:"images/level1_0023.png", id:"level1_0023"},
		{src:"images/level1_0024.png", id:"level1_0024"},
		{src:"images/level1_0025.png", id:"level1_0025"},
		{src:"images/level1_0026.png", id:"level1_0026"},
		{src:"images/level1_0027.png", id:"level1_0027"},
		{src:"images/level1_0028.png", id:"level1_0028"},
		{src:"images/level1_0029.png", id:"level1_0029"},
		{src:"images/level1_0030.png", id:"level1_0030"},
		{src:"images/level1_0031.png", id:"level1_0031"},
		{src:"images/level20001.png", id:"level20001"},
		{src:"images/level20002.png", id:"level20002"},
		{src:"images/level20003.png", id:"level20003"},
		{src:"images/level20004.png", id:"level20004"},
		{src:"images/level20005.png", id:"level20005"},
		{src:"images/level20006.png", id:"level20006"},
		{src:"images/level20007.png", id:"level20007"},
		{src:"images/level20008.png", id:"level20008"},
		{src:"images/level20009.png", id:"level20009"},
		{src:"images/level20010.png", id:"level20010"},
		{src:"images/level20011.png", id:"level20011"},
		{src:"images/level20012.png", id:"level20012"},
		{src:"images/level20013.png", id:"level20013"},
		{src:"images/level20014.png", id:"level20014"},
		{src:"images/level20015.png", id:"level20015"},
		{src:"images/level20016.png", id:"level20016"},
		{src:"images/level20017.png", id:"level20017"},
		{src:"images/level20018.png", id:"level20018"},
		{src:"images/level20019.png", id:"level20019"},
		{src:"images/level20020.png", id:"level20020"},
		{src:"images/level20021.png", id:"level20021"},
		{src:"images/level20022.png", id:"level20022"},
		{src:"images/level20023.png", id:"level20023"},
		{src:"images/level20024.png", id:"level20024"},
		{src:"images/level20025.png", id:"level20025"},
		{src:"images/level20026.png", id:"level20026"},
		{src:"images/level20027.png", id:"level20027"},
		{src:"images/level20028.png", id:"level20028"},
		{src:"images/level20029.png", id:"level20029"},
		{src:"images/level20030.png", id:"level20030"},
		{src:"images/level20031.png", id:"level20031"},
		{src:"images/level20032.png", id:"level20032"},
		{src:"images/level20033.png", id:"level20033"},
		{src:"images/level20034.png", id:"level20034"},
		{src:"images/level20035.png", id:"level20035"},
		{src:"images/level3_0001.png", id:"level3_0001"},
		{src:"images/level3_0002.png", id:"level3_0002"},
		{src:"images/level3_0003.png", id:"level3_0003"},
		{src:"images/level3_0004.png", id:"level3_0004"},
		{src:"images/level3_0005.png", id:"level3_0005"},
		{src:"images/level3_0006.png", id:"level3_0006"},
		{src:"images/level3_0007.png", id:"level3_0007"},
		{src:"images/level3_0008.png", id:"level3_0008"},
		{src:"images/level3_0009.png", id:"level3_0009"},
		{src:"images/level3_0010.png", id:"level3_0010"},
		{src:"images/level3_0011.png", id:"level3_0011"},
		{src:"images/level3_0012.png", id:"level3_0012"},
		{src:"images/level3_0013.png", id:"level3_0013"},
		{src:"images/level3_0014.png", id:"level3_0014"},
		{src:"images/level3_0015.png", id:"level3_0015"},
		{src:"images/level3_0016.png", id:"level3_0016"},
		{src:"images/level3_0017.png", id:"level3_0017"},
		{src:"images/level3_0018.png", id:"level3_0018"},
		{src:"images/level3_0019.png", id:"level3_0019"},
		{src:"images/level3_0020.png", id:"level3_0020"},
		{src:"images/level3_0021.png", id:"level3_0021"},
		{src:"images/level3_0022.png", id:"level3_0022"},
		{src:"images/level3_0023.png", id:"level3_0023"},
		{src:"images/level3_0024.png", id:"level3_0024"},
		{src:"images/level3_0025.png", id:"level3_0025"},
		{src:"images/level3_0026.png", id:"level3_0026"},
		{src:"images/level3_0027.png", id:"level3_0027"},
		{src:"images/level3_0028.png", id:"level3_0028"},
		{src:"images/level3_0029.png", id:"level3_0029"},
		{src:"images/level3_0030.png", id:"level3_0030"},
		{src:"images/level3_0031.png", id:"level3_0031"},
		{src:"images/level3_0032.png", id:"level3_0032"},
		{src:"images/level3_0033.png", id:"level3_0033"},
		{src:"images/level3_0034.png", id:"level3_0034"},
		{src:"images/level3_0035.png", id:"level3_0035"},
		{src:"images/level3_0036.png", id:"level3_0036"},
		{src:"images/level3_0037.png", id:"level3_0037"},
		{src:"images/level3_0038.png", id:"level3_0038"},
		{src:"images/level3_0039.png", id:"level3_0039"},
		{src:"images/level3_0040.png", id:"level3_0040"},
		{src:"images/level3_0041.png", id:"level3_0041"},
		{src:"images/level3_0042.png", id:"level3_0042"},
		{src:"images/level3_0043.png", id:"level3_0043"},
		{src:"images/level3_0044.png", id:"level3_0044"},
		{src:"images/level3_0045.png", id:"level3_0045"},
		{src:"images/sidebar.png", id:"sidebar"},
		{src:"images/start.png", id:"start"},
		{src:"sounds/correct.mp3", id:"correct"},
		{src:"sounds/wrong.mp3", id:"wrong"}
	]
};



// symbols:



(lib._123_nums = function() {
	this.initialize(img._123_nums);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,94,42);


(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards4x4 = function() {
	this.initialize(img.cards4x4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards4x4_bars = function() {
	this.initialize(img.cards4x4_bars);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards5x4 = function() {
	this.initialize(img.cards5x4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards5x4_bars = function() {
	this.initialize(img.cards5x4_bars);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards6x5 = function() {
	this.initialize(img.cards6x5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.cards6x5_bars = function() {
	this.initialize(img.cards6x5_bars);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,960,637);


(lib.images = function() {
	this.initialize(img.images);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1407,933);


(lib.level1_0001 = function() {
	this.initialize(img.level1_0001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0002 = function() {
	this.initialize(img.level1_0002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0003 = function() {
	this.initialize(img.level1_0003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0004 = function() {
	this.initialize(img.level1_0004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0005 = function() {
	this.initialize(img.level1_0005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0006 = function() {
	this.initialize(img.level1_0006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0007 = function() {
	this.initialize(img.level1_0007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0008 = function() {
	this.initialize(img.level1_0008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0009 = function() {
	this.initialize(img.level1_0009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0010 = function() {
	this.initialize(img.level1_0010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0011 = function() {
	this.initialize(img.level1_0011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0012 = function() {
	this.initialize(img.level1_0012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0013 = function() {
	this.initialize(img.level1_0013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0014 = function() {
	this.initialize(img.level1_0014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0015 = function() {
	this.initialize(img.level1_0015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0016 = function() {
	this.initialize(img.level1_0016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0017 = function() {
	this.initialize(img.level1_0017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0018 = function() {
	this.initialize(img.level1_0018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0019 = function() {
	this.initialize(img.level1_0019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0020 = function() {
	this.initialize(img.level1_0020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0021 = function() {
	this.initialize(img.level1_0021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0022 = function() {
	this.initialize(img.level1_0022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0023 = function() {
	this.initialize(img.level1_0023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0024 = function() {
	this.initialize(img.level1_0024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0025 = function() {
	this.initialize(img.level1_0025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0026 = function() {
	this.initialize(img.level1_0026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0027 = function() {
	this.initialize(img.level1_0027);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0028 = function() {
	this.initialize(img.level1_0028);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0029 = function() {
	this.initialize(img.level1_0029);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0030 = function() {
	this.initialize(img.level1_0030);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level1_0031 = function() {
	this.initialize(img.level1_0031);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


(lib.level20001 = function() {
	this.initialize(img.level20001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20002 = function() {
	this.initialize(img.level20002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20003 = function() {
	this.initialize(img.level20003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20004 = function() {
	this.initialize(img.level20004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20005 = function() {
	this.initialize(img.level20005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20006 = function() {
	this.initialize(img.level20006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20007 = function() {
	this.initialize(img.level20007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20008 = function() {
	this.initialize(img.level20008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20009 = function() {
	this.initialize(img.level20009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20010 = function() {
	this.initialize(img.level20010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20011 = function() {
	this.initialize(img.level20011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20012 = function() {
	this.initialize(img.level20012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20013 = function() {
	this.initialize(img.level20013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20014 = function() {
	this.initialize(img.level20014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20015 = function() {
	this.initialize(img.level20015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20016 = function() {
	this.initialize(img.level20016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20017 = function() {
	this.initialize(img.level20017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20018 = function() {
	this.initialize(img.level20018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20019 = function() {
	this.initialize(img.level20019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20020 = function() {
	this.initialize(img.level20020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20021 = function() {
	this.initialize(img.level20021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20022 = function() {
	this.initialize(img.level20022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20023 = function() {
	this.initialize(img.level20023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20024 = function() {
	this.initialize(img.level20024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20025 = function() {
	this.initialize(img.level20025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20026 = function() {
	this.initialize(img.level20026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20027 = function() {
	this.initialize(img.level20027);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20028 = function() {
	this.initialize(img.level20028);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20029 = function() {
	this.initialize(img.level20029);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20030 = function() {
	this.initialize(img.level20030);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20031 = function() {
	this.initialize(img.level20031);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20032 = function() {
	this.initialize(img.level20032);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20033 = function() {
	this.initialize(img.level20033);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20034 = function() {
	this.initialize(img.level20034);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level20035 = function() {
	this.initialize(img.level20035);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level3_0001 = function() {
	this.initialize(img.level3_0001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0002 = function() {
	this.initialize(img.level3_0002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0003 = function() {
	this.initialize(img.level3_0003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0004 = function() {
	this.initialize(img.level3_0004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0005 = function() {
	this.initialize(img.level3_0005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0006 = function() {
	this.initialize(img.level3_0006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0007 = function() {
	this.initialize(img.level3_0007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0008 = function() {
	this.initialize(img.level3_0008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0009 = function() {
	this.initialize(img.level3_0009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0010 = function() {
	this.initialize(img.level3_0010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0011 = function() {
	this.initialize(img.level3_0011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0012 = function() {
	this.initialize(img.level3_0012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0013 = function() {
	this.initialize(img.level3_0013);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0014 = function() {
	this.initialize(img.level3_0014);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0015 = function() {
	this.initialize(img.level3_0015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0016 = function() {
	this.initialize(img.level3_0016);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0017 = function() {
	this.initialize(img.level3_0017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0018 = function() {
	this.initialize(img.level3_0018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0019 = function() {
	this.initialize(img.level3_0019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0020 = function() {
	this.initialize(img.level3_0020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0021 = function() {
	this.initialize(img.level3_0021);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0022 = function() {
	this.initialize(img.level3_0022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0023 = function() {
	this.initialize(img.level3_0023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0024 = function() {
	this.initialize(img.level3_0024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0025 = function() {
	this.initialize(img.level3_0025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0026 = function() {
	this.initialize(img.level3_0026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0027 = function() {
	this.initialize(img.level3_0027);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0028 = function() {
	this.initialize(img.level3_0028);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0029 = function() {
	this.initialize(img.level3_0029);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0030 = function() {
	this.initialize(img.level3_0030);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0031 = function() {
	this.initialize(img.level3_0031);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0032 = function() {
	this.initialize(img.level3_0032);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0033 = function() {
	this.initialize(img.level3_0033);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0034 = function() {
	this.initialize(img.level3_0034);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0035 = function() {
	this.initialize(img.level3_0035);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0036 = function() {
	this.initialize(img.level3_0036);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0037 = function() {
	this.initialize(img.level3_0037);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0038 = function() {
	this.initialize(img.level3_0038);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0039 = function() {
	this.initialize(img.level3_0039);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0040 = function() {
	this.initialize(img.level3_0040);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0041 = function() {
	this.initialize(img.level3_0041);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0042 = function() {
	this.initialize(img.level3_0042);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0043 = function() {
	this.initialize(img.level3_0043);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0044 = function() {
	this.initialize(img.level3_0044);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level3_0045 = function() {
	this.initialize(img.level3_0045);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.sidebar = function() {
	this.initialize(img.sidebar);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,170,597);


(lib.start = function() {
	this.initialize(img.start);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,113,113);


(lib.tick = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#009933").s().p("Am2gBICLhTIBaD5IJCoRIBGBRIq6KHg");
	this.shape.setTransform(43.9,36.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,87.9,73);


(lib.start_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.start();

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ao1I/IAAx9IRrAAIAAR9g");
	this.shape.setTransform(56.1,57.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:0,y:0}}]}).to({state:[{t:this.instance,p:{scaleX:1.13,scaleY:1.13,x:-7.3,y:-7.3}}]},1).to({state:[{t:this.instance,p:{scaleX:1.13,scaleY:1.13,x:-4.5,y:-4.5}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,113,113);


(lib.redOutline2G = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D40000").ss(8,1,1).p("AvJsLIeTAAIAAYXI+TAAg");
	this.shape.setTransform(1,65,0.807,1,0,0,0,-96,-13);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.8,-4,164.6,164);


(lib.redOutline1G = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D40000").ss(8,1,1).p("AvJsLIeTAAIAAYXI+TAAg");
	this.shape.setTransform(97,78);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,202,164);


(lib.levelNumMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img._123_nums, null, new cjs.Matrix2D(1,0,0,1,-13,-21)).s().p("AiBDSIAAmjIEDAAIAAGjg");
	this.shape.setTransform(13,21);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(img._123_nums, null, new cjs.Matrix2D(1,0,0,1,-44.5,-21)).s().p("AiuDSIAAmjIFcAAIAAGjg");
	this.shape_1.setTransform(13.6,21);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.bf(img._123_nums, null, new cjs.Matrix2D(1,0,0,1,-77.5,-21)).s().p("AijDSIAAmjIFHAAIAAGjg");
	this.shape_2.setTransform(12.5,21);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Level3G = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("LEVEL 3", "bold 96px 'Thomas Headline'", "#D40000");
	this.text.textAlign = "center";
	this.text.lineHeight = 93;
	this.text.lineWidth = 391;
	this.text.setTransform(195.5,0);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,395,100);


(lib.Level2G = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("LEVEL 2", "bold 96px 'Thomas Headline'", "#D40000");
	this.text.textAlign = "center";
	this.text.lineHeight = 93;
	this.text.lineWidth = 391;
	this.text.setTransform(195.5,0);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,395,100);


(lib.Level1G = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("LEVEL 1", "bold 96px 'Thomas Headline'", "#D40000");
	this.text.textAlign = "center";
	this.text.lineHeight = 93;
	this.text.lineWidth = 391;
	this.text.setTransform(195.5,0);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,395,100);


(lib.greenOutline2G = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#009933").ss(8,1,1).p("AsIsLIYRAAIAAYXI4RAAg");
	this.shape.setTransform(78.1,78);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.6,-4,163.4,164);


(lib.greenOutline1G = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#009933").ss(8,1,1).p("AvJsLIeTAAIAAYXI+TAAg");
	this.shape.setTransform(97,78);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,202,164);


(lib.cross = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D40000").s().p("Al4D5ID3j0Ij5kLIAWgUIBrh1ID1EQID6kQICFCWIkDEEIDiD8Ih0CEIjrkAIj3EEg");
	this.shape.setTransform(38,34.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-5.3,75.9,80);


(lib.wrong2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		playSound("wrong");
	}
	this.frame_35 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(34).call(this.frame_35).wait(1));

	// Layer 2
	this.instance = new lib.cross("synched",0);
	this.instance.setTransform(79.1,78.5,0.3,0.3,0,0,0,43.9,36.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:84.2},21).to({scaleX:1.77,scaleY:1.77,x:87.1,alpha:0},13).wait(1));

	// Layer 1
	this.instance_1 = new lib.redOutline2G("synched",0);
	this.instance_1.setTransform(97,78,1,1,0,0,0,97,78);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({startPosition:0},21).to({alpha:0},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.wrong1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		playSound("wrong");
	}
	this.frame_35 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(34).call(this.frame_35).wait(1));

	// Layer 2
	this.instance = new lib.cross("synched",0);
	this.instance.setTransform(101.1,78.5,0.3,0.3,0,0,0,43.9,36.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:101.2},21).to({scaleX:1.77,scaleY:1.77,x:101.1,alpha:0},13).wait(1));

	// Layer 1
	this.instance_1 = new lib.redOutline1G("synched",0);
	this.instance_1.setTransform(97,78,1,1,0,0,0,97,78);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({startPosition:0},21).to({alpha:0},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.LevelMessageMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{Level1:1,Level2:35,Level3:69});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.gotoAndStop(0);
	}
	this.frame_68 = function() {
		this.gotoAndStop(0);
	}
	this.frame_102 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(34).call(this.frame_68).wait(34).call(this.frame_102).wait(1));

	// Layer 1
	this.instance = new lib.Level1G("synched",0);
	this.instance.setTransform(197.5,50,0.47,0.47,0,0,0,197.5,50);
	this.instance._off = true;

	this.instance_1 = new lib.Level2G("synched",0);
	this.instance_1.setTransform(197.5,50,0.47,0.47,0,0,0,197.5,50);
	this.instance_1._off = true;

	this.instance_2 = new lib.Level3G("synched",0);
	this.instance_2.setTransform(197.5,50,0.47,0.47,0,0,0,197.5,50);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1.43,scaleY:1.43,x:197.6},22).to({scaleX:1.91,scaleY:1.91,x:197.5,alpha:0},11).to({_off:true},1).wait(68));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(35).to({_off:false},0).to({scaleX:1.43,scaleY:1.43,x:197.6},22).to({scaleX:1.91,scaleY:1.91,x:197.5,alpha:0},11).to({_off:true},1).wait(34));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(69).to({_off:false},0).to({scaleX:1.43,scaleY:1.43,x:197.6},22).to({scaleX:1.91,scaleY:1.91,x:197.5,alpha:0},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.correct2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		playSound("correct");
	}
	this.frame_35 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(34).call(this.frame_35).wait(1));

	// Layer 2
	this.instance = new lib.tick("synched",0);
	this.instance.setTransform(77.1,78.5,0.3,0.3,0,0,0,43.9,36.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:78.2},21).to({scaleX:1.77,scaleY:1.77,x:78.1,alpha:0},13).wait(1));

	// Layer 1
	this.instance_1 = new lib.greenOutline2G("synched",0);
	this.instance_1.setTransform(97,78,1,1,0,0,0,97,78);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({startPosition:0},21).to({alpha:0},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.correct1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		playSound("correct");
	}
	this.frame_35 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(34).call(this.frame_35).wait(1));

	// Layer 2
	this.instance = new lib.tick("synched",0);
	this.instance.setTransform(101.1,78.5,0.3,0.3,0,0,0,43.9,36.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:101.2},21).to({scaleX:1.77,scaleY:1.77,x:101.1,alpha:0},13).wait(1));

	// Layer 1
	this.instance_1 = new lib.greenOutline1G("synched",0);
	this.instance_1.setTransform(97,78,1,1,0,0,0,97,78);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({startPosition:0},21).to({alpha:0},13).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.level3Cards = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.wrong_mc = new lib.wrong2MC();
	this.wrong_mc.setTransform(-2,-0.5,0.823,0.804);

	this.timeline.addTween(cjs.Tween.get(this.wrong_mc).wait(45));

	// Layer 3
	this.correct_mc = new lib.correct2MC();
	this.correct_mc.setTransform(-2,0.5,0.828,0.803,0,0,0,0,1);

	this.timeline.addTween(cjs.Tween.get(this.correct_mc).wait(45));

	// Layer 1
	this.instance = new lib.level3_0001();

	this.instance_1 = new lib.level3_0002();

	this.instance_2 = new lib.level3_0003();

	this.instance_3 = new lib.level3_0004();

	this.instance_4 = new lib.level3_0005();

	this.instance_5 = new lib.level3_0006();

	this.instance_6 = new lib.level3_0007();

	this.instance_7 = new lib.level3_0008();

	this.instance_8 = new lib.level3_0009();

	this.instance_9 = new lib.level3_0010();

	this.instance_10 = new lib.level3_0011();

	this.instance_11 = new lib.level3_0012();

	this.instance_12 = new lib.level3_0013();

	this.instance_13 = new lib.level3_0014();

	this.instance_14 = new lib.level3_0015();

	this.instance_15 = new lib.level3_0016();

	this.instance_16 = new lib.level3_0017();

	this.instance_17 = new lib.level3_0018();

	this.instance_18 = new lib.level3_0019();

	this.instance_19 = new lib.level3_0020();

	this.instance_20 = new lib.level3_0021();

	this.instance_21 = new lib.level3_0022();

	this.instance_22 = new lib.level3_0023();

	this.instance_23 = new lib.level3_0024();

	this.instance_24 = new lib.level3_0025();

	this.instance_25 = new lib.level3_0026();

	this.instance_26 = new lib.level3_0027();

	this.instance_27 = new lib.level3_0028();

	this.instance_28 = new lib.level3_0029();

	this.instance_29 = new lib.level3_0030();

	this.instance_30 = new lib.level3_0031();

	this.instance_31 = new lib.level3_0032();

	this.instance_32 = new lib.level3_0033();

	this.instance_33 = new lib.level3_0034();

	this.instance_34 = new lib.level3_0035();

	this.instance_35 = new lib.level3_0036();

	this.instance_36 = new lib.level3_0037();

	this.instance_37 = new lib.level3_0038();

	this.instance_38 = new lib.level3_0039();

	this.instance_39 = new lib.level3_0040();

	this.instance_40 = new lib.level3_0041();

	this.instance_41 = new lib.level3_0042();

	this.instance_42 = new lib.level3_0043();

	this.instance_43 = new lib.level3_0044();

	this.instance_44 = new lib.level3_0045();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_30}]},1).to({state:[{t:this.instance_31}]},1).to({state:[{t:this.instance_32}]},1).to({state:[{t:this.instance_33}]},1).to({state:[{t:this.instance_34}]},1).to({state:[{t:this.instance_35}]},1).to({state:[{t:this.instance_36}]},1).to({state:[{t:this.instance_37}]},1).to({state:[{t:this.instance_38}]},1).to({state:[{t:this.instance_39}]},1).to({state:[{t:this.instance_40}]},1).to({state:[{t:this.instance_41}]},1).to({state:[{t:this.instance_42}]},1).to({state:[{t:this.instance_43}]},1).to({state:[{t:this.instance_44}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,127,125);


(lib.level2Cards = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.wrong_mc = new lib.wrong2MC();
	this.wrong_mc.setTransform(95,77.5,1,1,0,0,0,97,78);

	this.timeline.addTween(cjs.Tween.get(this.wrong_mc).wait(35));

	// Layer 2
	this.correct_mc = new lib.correct2MC();
	this.correct_mc.setTransform(95,77.5,1,1,0,0,0,97,78);

	this.timeline.addTween(cjs.Tween.get(this.correct_mc).wait(35));

	// Layer 1
	this.instance = new lib.level20001();

	this.instance_1 = new lib.level20002();

	this.instance_2 = new lib.level20003();

	this.instance_3 = new lib.level20004();

	this.instance_4 = new lib.level20005();

	this.instance_5 = new lib.level20006();

	this.instance_6 = new lib.level20007();

	this.instance_7 = new lib.level20008();

	this.instance_8 = new lib.level20009();

	this.instance_9 = new lib.level20010();

	this.instance_10 = new lib.level20011();

	this.instance_11 = new lib.level20012();

	this.instance_12 = new lib.level20013();

	this.instance_13 = new lib.level20014();

	this.instance_14 = new lib.level20015();

	this.instance_15 = new lib.level20016();

	this.instance_16 = new lib.level20017();

	this.instance_17 = new lib.level20018();

	this.instance_18 = new lib.level20019();

	this.instance_19 = new lib.level20020();

	this.instance_20 = new lib.level20021();

	this.instance_21 = new lib.level20022();

	this.instance_22 = new lib.level20023();

	this.instance_23 = new lib.level20024();

	this.instance_24 = new lib.level20025();

	this.instance_25 = new lib.level20026();

	this.instance_26 = new lib.level20027();

	this.instance_27 = new lib.level20028();

	this.instance_28 = new lib.level20029();

	this.instance_29 = new lib.level20030();

	this.instance_30 = new lib.level20031();

	this.instance_31 = new lib.level20032();

	this.instance_32 = new lib.level20033();

	this.instance_33 = new lib.level20034();

	this.instance_34 = new lib.level20035();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_30}]},1).to({state:[{t:this.instance_31}]},1).to({state:[{t:this.instance_32}]},1).to({state:[{t:this.instance_33}]},1).to({state:[{t:this.instance_34}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,151,156);


(lib.level1Cards = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.wrong_mc = new lib.wrong1MC();
	this.wrong_mc.setTransform(95,77.5,1,1,0,0,0,97,78);

	this.timeline.addTween(cjs.Tween.get(this.wrong_mc).wait(31));

	// Layer 2
	this.correct_mc = new lib.correct1MC();
	this.correct_mc.setTransform(95,77.5,1,1,0,0,0,97,78);

	this.timeline.addTween(cjs.Tween.get(this.correct_mc).wait(31));

	// Layer 1
	this.instance = new lib.level1_0001();

	this.instance_1 = new lib.level1_0002();

	this.instance_2 = new lib.level1_0003();

	this.instance_3 = new lib.level1_0004();

	this.instance_4 = new lib.level1_0005();

	this.instance_5 = new lib.level1_0006();

	this.instance_6 = new lib.level1_0007();

	this.instance_7 = new lib.level1_0008();

	this.instance_8 = new lib.level1_0009();

	this.instance_9 = new lib.level1_0010();

	this.instance_10 = new lib.level1_0011();

	this.instance_11 = new lib.level1_0012();

	this.instance_12 = new lib.level1_0013();

	this.instance_13 = new lib.level1_0014();

	this.instance_14 = new lib.level1_0015();

	this.instance_15 = new lib.level1_0016();

	this.instance_16 = new lib.level1_0017();

	this.instance_17 = new lib.level1_0018();

	this.instance_18 = new lib.level1_0019();

	this.instance_19 = new lib.level1_0020();

	this.instance_20 = new lib.level1_0021();

	this.instance_21 = new lib.level1_0022();

	this.instance_22 = new lib.level1_0023();

	this.instance_23 = new lib.level1_0024();

	this.instance_24 = new lib.level1_0025();

	this.instance_25 = new lib.level1_0026();

	this.instance_26 = new lib.level1_0027();

	this.instance_27 = new lib.level1_0028();

	this.instance_28 = new lib.level1_0029();

	this.instance_29 = new lib.level1_0030();

	this.instance_30 = new lib.level1_0031();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_26}]},1).to({state:[{t:this.instance_27}]},1).to({state:[{t:this.instance_28}]},1).to({state:[{t:this.instance_29}]},1).to({state:[{t:this.instance_30}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,190,156);


// stage content:
(lib.pairs01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		initGame();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 8
	this.levelNum_mc = new lib.levelNumMC();
	this.levelNum_mc.setTransform(954.9,56,1,1,0,0,0,47,21);

	this.timeline.addTween(cjs.Tween.get(this.levelNum_mc).wait(1));

	// Layer 5
	this.timeSecs_txt = new cjs.Text("00", "bold 28px 'Thomas Headline'", "#998859");
	this.timeSecs_txt.name = "timeSecs_txt";
	this.timeSecs_txt.lineHeight = 25;
	this.timeSecs_txt.lineWidth = 56;
	this.timeSecs_txt.setTransform(865.9,167.5);

	this.pairs_txt = new cjs.Text("0 of 0", "bold 28px 'Thomas Headline'", "#D40000");
	this.pairs_txt.name = "pairs_txt";
	this.pairs_txt.lineHeight = 25;
	this.pairs_txt.lineWidth = 100;
	this.pairs_txt.setTransform(1186,215.1);

	this.level_txt = new cjs.Text("0 of 3", "bold 28px 'Thomas Headline'", "#D40000");
	this.level_txt.name = "level_txt";
	this.level_txt.lineHeight = 25;
	this.level_txt.lineWidth = 100;
	this.level_txt.setTransform(1186,174.5);

	this.start_btn = new lib.start_btn();
	this.start_btn.setTransform(861,373,1,1,0,0,0,56.6,56.6);
	new cjs.ButtonHelper(this.start_btn, 0, 1, 2, false, new lib.start_btn(), 3);

	this.timeMins_txt = new cjs.Text("00", "bold 28px 'Thomas Headline'", "#998859");
	this.timeMins_txt.name = "timeMins_txt";
	this.timeMins_txt.textAlign = "right";
	this.timeMins_txt.lineHeight = 25;
	this.timeMins_txt.lineWidth = 56;
	this.timeMins_txt.setTransform(853,167.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.timeMins_txt},{t:this.start_btn},{t:this.level_txt},{t:this.pairs_txt},{t:this.timeSecs_txt}]}).wait(1));

	// Layer 7
	this.instance = new lib.sidebar();
	this.instance.setTransform(774.9,34);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4
	this.level_mc = new lib.LevelMessageMC();
	this.level_mc.setTransform(386.5,326,1,1,0,0,0,197.5,50);

	this.instance_1 = new lib.level3Cards();
	this.instance_1.setTransform(1059.5,417,1,1,0,0,0,63.5,62.5);

	this.instance_2 = new lib.level2Cards();
	this.instance_2.setTransform(1071.5,262,1,1,0,0,0,75.5,78);

	this.instance_3 = new lib.level1Cards();
	this.instance_3.setTransform(1091,87.5,1,1,0,0,0,95,78);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.level_mc}]}).wait(1));

	// Layer 3
	this.instance_4 = new lib.cards4x4_bars();

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 1
	this.instance_5 = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(479,317.5,1291,639);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;