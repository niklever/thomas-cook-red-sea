// Avoid `console` errors in browsers that lack a console.
if(!(window.console && console.log)) {
  console = {
    log: function(){},
    debug: function(){},
    info: function(){},
    warn: function(){},
    error: function(){}
  };
}

// JavaScript Document
var baseUrlApi = '../ws/';
var gameId = 3;
var sentTime;
var sendTimeID = -1;
var game = { startTime:0, level:0, cards:null, pairs:0, pairsTarget:-1, selectedCards:new Array(), btnsActive:false };
var prize_draw = false;

function keydown(e) {
	// use e.which
	console.log("down " + e.which);
	if (e.preventDefault!=null) e.preventDefault();
	switch(e.which){
		case 81://q
		gameOver(true);
		break;
		case 80://p
		gameOver(false);
		break;
	}
}

function initGame(){
	console.log("initGame");
	
	//document.onkeydown = keydown;
	
	exportRoot.start_btn.on("mousedown", function(event) {
		console.log('start mousedown');
		createjs.Ticker.addEventListener("tick", gameLoop);
		var d = new Date();
		game.startTime = d.getTime();
		game.level = 1;
		initLevel(1);
		sentTime = 0;
		sendTimeID = setInterval( sendTimeInt, 5000 );
		sendInitGame();
		exportRoot.start_btn.visible = false;
	});
}

function initLevel(idx){
	if (game.cards!=null && game.cards.length>0){
		while(game.cards.length>0){
			exportRoot.removeChild(game.cards.pop());
		}
	}
	exportRoot.level_txt.text = idx + " of 3";
	exportRoot.levelNum_mc.gotoAndStop(idx);
	game.level = idx;
	game.pairs = 0;
	game.cards = new Array();
	var i=0, first;
	switch(idx){
		case 1:
		for(var row=0; row<4; row++){
			for(var col=0; col<4; col++){
				var card = new lib.level1Cards();
				exportRoot.addChild(card);
				card.x = col * 190 + 8;
				card.y = row * 156 + 8;
				card.index = i;
				card.gotoAndStop(i++);
				card.facedown = true;
				game.cards.push(card);
			}
		}
		first = 16;
		break;
		case 2:
		for(var row=0; row<4; row++){
			for(var col=0; col<5; col++){
				var card = new lib.level2Cards();
				exportRoot.addChild(card);
				card.x = col * 151 + 8;
				card.y = row * 156 + 8;
				card.index = i;
				card.gotoAndStop(i++);
				card.facedown = true;
				game.cards.push(card);
			}
		}
		first = 20;
		break;
		case 3:
		for(var row=0; row<5; row++){
			for(var col=0; col<6; col++){
				var card = new lib.level3Cards();
				exportRoot.addChild(card);
				card.x = col * 127 + 8;
				card.y = row * 125 + 8;
				card.index = i;
				card.gotoAndStop(i++);
				card.facedown = true;
				game.cards.push(card);
			}
		}
		first = 30;
		break;
	}
	
	var cards = new Array();
	for(var i=0; i<game.cards.length; i++){
		var card = game.cards[i];
		cards.push(card);
		card.on("mousedown", function(event) {
			if (!game.btnsActive) return;
			console.log('card mousedown');
			var card = event.target.parent;
			if (card.facedown){
				card.gotoAndStop(card.card);
				card.facedown = false;
				game.selectedCards.push(card);
				if (game.selectedCards.length==2){
					game.btnsActive = false;
					if (game.selectedCards[0].card == game.selectedCards[1].card){
						game.selectedCards[0].correct_mc.gotoAndPlay(1);
						game.selectedCards[1].correct_mc.gotoAndPlay(1);
						setTimeout( correctPair, 1000 );
					}else{
						game.selectedCards[0].wrong_mc.gotoAndPlay(1);
						game.selectedCards[1].wrong_mc.gotoAndPlay(1);
						setTimeout( wrongPair, 1000 );
					}
				}
			}
		});
	}
	//Jumble array
	for (i=0; i<60; i++){
		var card = cards.splice(Math.floor(Math.random()*cards.length), 1);
		cards.push(card[0]);
	}
	
	game.pairsTarget = first/2;
	exportRoot.pairs_txt.text = "0 of " + game.pairsTarget;
	var index = first;
	//Assign the cards to each pair of cards
	for(i=0; i<cards.length; i+=2){
		cards[i].card = cards[i+1].card = index;
		index++;
	}
	
	var level_mc = exportRoot.level_mc;
	exportRoot.removeChild(level_mc);
	exportRoot.addChild(level_mc);
	level_mc.gotoAndPlay("Level" + idx);
	
	game.btnsActive = true;
}

function correctPair(){
	for(var i=0; i<2; i++){
		game.selectedCards[i].on("mousedown", function(){});
	}
	game.pairs++;
	exportRoot.pairs_txt.text = game.pairs + " of " + game.pairsTarget;
	game.selectedCards = new Array();
	//gameOver(true);
	if (game.pairs==game.pairsTarget){
		if (game.level==3){
			gameOver(true);
		}else{
			initLevel(game.level + 1);
		}
		return;
	}
	game.btnsActive = true;
}

function wrongPair(){
	for(var i=0; i<2; i++){
		var card = game.selectedCards[i];
		card.gotoAndStop(card.index);
		card.facedown = true;
	}
	game.selectedCards = new Array();
	game.btnsActive = true;
}

function gameLoop(){
	updateGameHUD();
}

function updateMessage(frm){
	var mc = exportRoot.message_mc;
	mc.gotoAndStop(frm);
	mc.visible = true;
	clearTimeout(messageID);
	messageID = setTimeout( hideMessage, 3000 );
}

function hideMessage(){
	var mc = exportRoot.message_mc;
	mc.visible = false;
}

function updateGameHUD(){
	var d = new Date();
	var secs = Math.floor((d.getTime() - game.startTime)/1000.0);
	var mins = Math.floor(secs / 60);
	secs %= 60;
	exportRoot.timeMins_txt.text = (mins>9) ? mins : "0" + mins;
	exportRoot.timeSecs_txt.text = (secs>9) ? secs : "0" + secs;
}

function gameOver(completed){
	clearInterval(sendTimeID);	
	createjs.Sound.removeAllSounds();
	playSound("game_over");
	if (completed){
		var d = new Date();
		var now = d.getTime();
		var elapsedTime = Math.floor((now - game.startTime)/100);
		var offset = elapsedTime - sentTime;
		sentTime += offset;
		console.log("gameOver: sentTime=" + sentTime + " offset=" + offset);
		score = { offset:offset, time:elapsedTime};
		if (prize_draw){
			setTimeout(sendFinalScore, 100);
		}else{
			window.parent.change_page('well_done');
		}
		//updateMessage("Well done!");
	}else{
		window.parent.change_page('sorry');
		//updateMessage("You ran out of lives!");
	}
	createjs.Ticker.removeEventListener("tick", gameLoop);
	
	setTimeout(resetGame, 1000);
	game.btnsActive = false;
}

function resetGame(){
	exportRoot.timeMins_txt.text = "00";
	exportRoot.timeSecs_txt.text = "00";
	exportRoot.pairs_txt.text = "0 of 0";
	exportRoot.level_txt.text = "0 of 3";
	exportRoot.levelNum_mc.gotoAndStop(0);
	exportRoot.start_btn.visible = true;
	if (game.cards!=null && game.cards.length>0){
		while(game.cards.length>0){
			exportRoot.removeChild(game.cards.pop());
		}
	}
}

//Called after the small time update is sent
function sendFinalScore(){
	email = localStorage.getItem("email");
	
	console.log("sendFinalScore email:" + email);
	
	if (email!=null){
		console.log("sendFinalScore sendscore:" + JSON.stringify(score));
		// user win.
		$.ajax({
			type:'post',
			url: baseUrlApi + "sendscore.php",
			dataType:"json",
			data:{score:score.time, offset:score.offset, email:email, gameId:gameId},
			success: function(data){
				console.log("sendFinalScore:returned " + JSON.stringify(data));
				if(data.topten == true){
					window.parent.change_page('congratulations');
				}else{
					if(data.is_nearhigh == true){
						window.parent.change_page('well_done');
					}else{
						window.parent.change_page('sorry');
					}
				}			
			},
			error: function(data){
				console.log("Error:" + JSON.stringify(data));
			}
		});
	}else{
		window.parent.change_page('sorry');
	}
}

//Initialise the server session
function sendInitGame(){
	$.ajax({
		type:'post',
		url: baseUrlApi + 'init-game.php',
		data:{gameId:gameId},
		success: function(data){
			console.log("init-game:returned " + data);
		}
	});
}

//Called every 5 secs
function sendTimeInt(){
	var inc = 50;
	sentTime += inc;
	incScore(inc);
}

//Make small incremental increase/decrease to server score
function incScore(score){
	$.ajax({
		type:'post',
		url: baseUrlApi + 'inc.php',
		data:{score:score, gameId:gameId},
		success: function(data){
			console.log("inc:returned " + data);
		}
	});
}
