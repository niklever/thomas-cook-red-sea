<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Thomas Cook</title>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, target-densitydpi=device-dpi">
<link rel="stylesheet" href="style.css">

<style type="text/css" media="screen">
	<?php
		include 'style.css';
	?>
	*{
		-webkit-user-select: none;  /* Chrome all / Safari all */
		-moz-user-select: none;     /* Firefox all */
		-ms-user-select: none;      /* IE 10+ */

		/* No support for these yet, use at own risk */
		-o-user-select: none;
		user-select: none;  
	}
	.hide{
		display: none
	}
	body
	{
		background-color: white;
		margin:auto;
		padding:auto;
    overflow: hidden;
	}
	canvas{
		background-image:url('images_web/bg_cv.jpg');
	}
	#progress-bar{
		position:absolute;
		top:280px;
		left: 365px;
		height:20px;
		width: 200px;overflow:hidden;
		border:1px solid white;
	}
	#progress{
		position:relative;
		width: 100%;
		height:20px;
		left:-100%;
		/*background:#2088b8;*/
		background:#AA0000;
	}
	#start-button{
		height:113px;display: none;
		width:113px;
		left: 739px;
	    position: absolute;
	    top: 460px;
		background: url('images_web/start.png');
		z-index: 1;
	}
	.bg_cavas{
		background:url('images_web/new_bg.jpg') right no-repeat;
	}
	#loader{
		left: 465px;
    position: absolute;
    top: 300px;
	}
	#controls{
		height: 170px;
    left: 708px;
    position: absolute;
    top: 435px;display: none;
    width: 170px;
	}
	#btn_up{
		background: url('images/up.png') no-repeat center;
		display: inline-block;
    height: 35px;
    left: 66px;
    position: absolute;
    top: 0;
    width: 39px;
	}
	#btn_left{
		background: url('images/left.png') no-repeat center;
		cursor: pointer;
    display: inline-block;
    height: 39px;
    position: absolute;
    top: 66px;
    width: 35px;
	}
	#btn_right{
		background: url('images/right.png') no-repeat center;
		cursor: pointer;
    display: inline-block;
    height: 39px;
    position: absolute;
    right: 0;
    top: 66px;
    width: 35px;
	}
	#btn_down{
		background: url('images/down.png') no-repeat center;
		cursor: pointer;
    display: inline-block;
    height: 35px;
    left: 63px;
    position: absolute;
    top: 127px;
    width: 39px;
	}
</style>
<script type="text/javascript">
	<?php
include "lib-js/easeljs-0.7.0.min.js";
include "lib-js/tweenjs-0.5.0.min.js";
include "lib-js/movieclip-0.7.0.min.js";
include "lib-js/preloadjs-0.4.0.min.js";
include "lib-js/soundjs-0.5.2.min.js";
include "lib-js/jquery-1-11-0.js";
include "lib-js/jquery.cookie.js";
include "lib-js/hammer.min.js";
include "maps.js";
	?>
</script>
<script src="pearl-diver.js"></script>
<script src="game.js"></script>


<script>
var canvas, stage, exportRoot, progress_el, gameId;

gameId = 2;

function detectmob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}

var is_ipad = detectmob();


function handleProgress(event) {
	percent = 100*event.progress - 100;

	progress_el.style.left = percent + '%';
}
function init() {
	canvas = document.getElementById("canvas");
	images = images||{};

	var loader = new createjs.LoadQueue(false);
	loader.installPlugin(createjs.Sound);
	loader.addEventListener("fileload", handleFileLoad);
	loader.addEventListener("complete", handleComplete);
	loader.loadManifest(lib.properties.manifest);

	loader.addEventListener('progress', handleProgress);

	progress_el = document.getElementById("progress");

	document.getElementById("start-button").onclick = function(){
		gameActive = true;

		this.style.display = 'none';
	}


	Hammer(document.getElementById('btn_up')).on('touch', function(){
		keydown({which:38});
		forwardTouch = true;
	});
	Hammer(document.getElementById('btn_down')).on('touch', function(){
		keydown({which:40});
		forwardTouch = true;
	});
	Hammer(document.getElementById('btn_left')).on('touch', function(){
		keydown({which:37});
		forwardTouch = true;
	});
	Hammer(document.getElementById('btn_right')).on('touch', function(){
		keydown({which:39});
		forwardTouch = true;
	});
	
	if(is_ipad){
		Hammer(document.getElementById('btn_up')).on('touchend', function(){
			keyup({which:38});
			forwardTouch = false;
		});
		Hammer(document.getElementById('btn_down')).on('touchend', function(){
			keyup({which:40});
			forwardTouch = false;
		});
		Hammer(document.getElementById('btn_left')).on('touchend', function(){
			keyup({which:37});
			forwardTouch = false;
		});
		Hammer(document.getElementById('btn_right')).on('touchend', function(){
			keyup({which:39});
			forwardTouch = false;
		});
	}else{
		$('#btn_up').mouseup(function(event) {
			keyup({which:38});
			forwardTouch = false;
		});
		
		$('#btn_down').mouseup(function(event) {
			keyup({which:40});
			forwardTouch = false;
		});
		
		$('#btn_left').mouseup(function(event) {
			keyup({which:37});
			forwardTouch = false;
		});
		
		$('#btn_right').mouseup(function(event) {
			keyup({which:39});
			forwardTouch = false;
		});
	}


}

function handleFileLoad(evt) {
	if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function handleComplete() {
	canvas.className = 'bg_cavas';
	document.getElementById("progress-bar").style.display = 'none';

	document.getElementById("start-button").style.display = 'block';
	document.getElementById("controls").style.display = 'block';

	exportRoot = new lib.pearldiver01_v43();

	stage = new createjs.Stage(canvas);
	createjs.Touch.enable(stage);
	stage.addChild(exportRoot);
	stage.update();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
	gameActive = false;
}
function playSound(id, loop) {
	createjs.Sound.play(id, createjs.Sound.INTERRUPT_EARLY, 0, 0, loop);
}
</script>
</head>

<body onload="init()">
	<!-- <div id="main-container">
	<div id="main-background" class="container">
    	<div id="main-sub-background"> -->
			<img id="loader" src="images_web/loader.gif" class="hide" />
        	<div id="progress-bar">
				<div id="progress"></div>
			</div>
			<div id='start-button'></div>
        	<canvas id="canvas" width="960" height="637"></canvas>
            <p id="fps-number" style="float:right"></p>
			<div id="controls">
				<span id="btn_up"></span>
				<span id="btn_left"></span>
				<span id="btn_right"></span>
				<span id="btn_down"></span>
			</div>
        <!-- </div>
	</div>
</div> -->
	<img src="images_web/new_bg.jpg" class="hide" />
</html>