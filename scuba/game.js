// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


var baseUrlApi = 'http://catalystpics.co.uk/clients/foundry/thomascook/v2/ws/';

var url_api = {
	'inc' : 'inc.php',
	'sendscore' : 'sendscore.php',
	'socialscore' : 'socialscore.php'
}


// JavaScript Document
Array.prototype.indexOf = function arrayObjectIndexOf(item) {
    for (var i = 0, len = this.length; i < len; i++) {
        if (this[i] === item) return i;
    }
    return -1;
}


var diverLimits = null;
var script = null;
var caves = null;
var caves_mc;
var bg_mc;
var fish_anim_mc;
var map_mc;
var diver_cell;
var diver_mc;

var gameover;
var nextlevel;
var replay_btn;
var quit_btn;
var score_board;

var quit;
var txtPearls_mc;
var txtScore_mc;
var txtNumLife_mc;
var pearls;
var baddies;
var speed = 0;
var forward;
var forwardTouch;
var previousKey = 40;
var mapDiverCell = { row:0, col:6 };
var pearlsCollectedCount = 0;
var current_score = 0;
var numberLife = 2;
var gameActive = false;

var totalNumberPearl = 0;

var currentLevel = 1;

var height_top_bg = 500;

var navButtonScale = 0.8;

var isfirstKeyPress = 0;

var time_start = 0;
var time_current = 0;
var second_number_start = 0;
var min_time_complete_level = 40;
var max_time_complete_level = 120;

function keydown(e) {
	// use e.which
	if (!gameActive) return;

	// set flag isHittingWall again.
	diver_mc.isHittingWall = false;
	
	
	if(e.which == 76){
		levelComplete();
	}

	switch(e.which){
		case 37://left
		if (e.preventDefault!=null) e.preventDefault();
		// check able turning left or not. 
		var legalMove = getLegalMoveFromLocation(diver_mc.x, diver_mc.y);
		if(!legalMove.left){
			forward = true;
			return;
		}

		if (previousKey!=37){
			diver_mc.rotation = 180;

			diver_mc.scaleY = -0.7;
			var cell = getCellFromLocation(diver_mc.x, diver_mc.y);
			var pt = getLocationFromCell(cell.row, cell.col);
			diver_mc.x = pt.x;
			diver_mc.y = pt.y;
			speed = 7;
			mapDiverCell.mc.rotation = diver_mc.rotation;
		}else{
			speed += 2;
			if (speed>MAXSPEED) speed = MAXSPEED;
			forward = true;
		}
		previousKey = e.which;
		break;
		case 38://up
		if (e.preventDefault!=null) e.preventDefault();
		var legalMove = getLegalMoveFromLocation(diver_mc.x, diver_mc.y);
		if(!legalMove.up){
			forward = true;
			return;
		}

		if (previousKey!=38){
			diver_mc.rotation = 270;

			diver_mc.scaleY = 0.7;
			var cell = getCellFromLocation(diver_mc.x, diver_mc.y);
			var pt = getLocationFromCell(cell.row, cell.col);
			diver_mc.x = pt.x;
			diver_mc.y = pt.y;
			speed = 7;
			mapDiverCell.mc.rotation = diver_mc.rotation;
		}else{
			speed += 2;
			if (speed>MAXSPEED) speed = MAXSPEED;
			forward = true;
			// console.log('up speed',speed, forward);
		}
		previousKey = e.which;
		break;
		case 39://right
		if (e.preventDefault!=null) e.preventDefault();
		// check able turning right or not. 
		var legalMove = getLegalMoveFromLocation(diver_mc.x, diver_mc.y);
		if(!legalMove.right){
			forward = true;
			return;
		}

		if (previousKey!=39){
			diver_mc.rotation = 0;

			diver_mc.scaleY = 0.7;
			var cell = getCellFromLocation(diver_mc.x, diver_mc.y);
			var pt = getLocationFromCell(cell.row, cell.col);
			diver_mc.x = pt.x;
			diver_mc.y = pt.y;
			speed = 7;
			mapDiverCell.mc.rotation = diver_mc.rotation;
		}else{
			speed += 2;
			if (speed>MAXSPEED) speed = MAXSPEED;
			forward = true;
		}
		previousKey = e.which;
		break;
		case 40://down
		if (e.preventDefault!=null) e.preventDefault();
		// check able turning down or not. 
		var legalMove = getLegalMoveFromLocation(diver_mc.x, diver_mc.y);
		if(!legalMove.down){
			forward = true;
			return;
		}

		if (previousKey!=40){
			diver_mc.rotation = 90;

			diver_mc.scaleY = 0.7;
			var cell = getCellFromLocation(diver_mc.x, diver_mc.y);
			var pt = getLocationFromCell(cell.row, cell.col);
			diver_mc.x = pt.x;
			diver_mc.y = pt.y;
			speed = 7;
			mapDiverCell.mc.rotation = diver_mc.rotation;
		}else{
			speed += 2;
			if (speed>MAXSPEED) speed = MAXSPEED;
			forward = true;
		}
		previousKey = e.which;
		break;
	}

	isfirstKeyPress += 1;
};

function keyup(e) {
	if (!gameActive) return;
	// use e.which
	/*switch(e.which){
		case 37://left
		case 38:
		case 39://right
		case 40:
		forward = false;
		break;
	}*/

	forward = false;
};
					  
function initCaves(level){

	console.log('initCaves', level);

	if(typeof level == 'undefined'){
		// init level 1.
		level = 1;
	}

	script = JSON.parse(JSON.stringify(scripts[level]));

	caves = JSON.parse(JSON.stringify(script.map));
	mapDiverCell = JSON.parse(JSON.stringify(script.startCellDiver));
	diverLimits = { left:40, right:caves[0].length*80-40, top:40, bottom:caves.length*80-40 }

	caves_mc = new createjs.MovieClip();
	map_mc = new createjs.MovieClip();
	bg_mc = new lib.background();
	stage.addChildAt(bg_mc,0);
	stage.addChild(caves_mc);
	//stage.addChild(map_mc);

	fish_anim_mc = new lib.fish();
	var fish_anim_mask =  new createjs.Shape(
     new createjs.Graphics().drawRect(135,0,540, 427));	
	fish_anim_mc.mask = fish_anim_mask;
	fish_anim_mc.x = 60;
	fish_anim_mc.y = 270;
	bg_mc.addChild(fish_anim_mc);

	var index = 0;
	var left = 0;
	var top = 0;
	
	for(var row=0; row<caves.length; row++){
		left = 0;
		for(var col=0; col<caves[row].length; col++){
			var cell = new lib.cells();
			// cell.gotoAndStop(caves[row][col]);
			cell.gotoAndStop(16);
			cell.x = left;
			cell.y = top;
			caves_mc.addChild(cell);
			// console.log('row', row, 'col', col, 'left', left, 'top', top);
			cell = new lib.cells();
			cell.gotoAndStop(caves[row][col]);
			cell.x = left;
			cell.y = top;
			left += 79;
			map_mc.addChild(cell);
		}
		top += 80;
	}
	
	map_mc.x = 20;
	map_mc.y = 160;
	map_mc.scaleX = 0.15;
	map_mc.scaleY = 0.15;
	var index = mapDiverCell.row * caves[0].length + mapDiverCell.col;
	mapDiverCell.mc = map_mc.children[index].diverIcon_mc;
	mapDiverCell.mc.rotation = script.configDiver.rotation;
	mapDiverCell.mc.visible = true;	
	
	// caves_mc.x = (825 - caves[0].length*80)/2 - 7;
	caves_mc.x = 0;
	caves_mc.y = height_top_bg;

	// var bg_image = new createjs.Bitmap('images/bg3.png');
	// bg_image.x = caves_mc.x;
	// bg_image.y = 0;
	// bg_mc.addChild(bg_image);	
	bg_mc.x = caves_mc.x;
	bg_mc.y = 0;

	caves_mc.scaleX = caves_mc.scaleY = bg_mc.scaleX = bg_mc.scaleY = 1.2;

/*
	gameover = new createjs.Bitmap('images/game over.png');
	//gameover.x = 105;
	//gameover.y = 150;
	gameover.x = 0;
	gameover.y = 0;
	gameover.visible = false;
	stage.addChild(gameover);


	replay_btn = new createjs.Bitmap('images/replay.png');
	replay_btn.x = 315;
	replay_btn.y = 300;
	replay_btn.visible = false;
	stage.addChild(replay_btn);
	replay_btn.on("click", function(event){
		// location.reload();
		endGame();
		initCaves(1);
		createjs.Ticker.addEventListener("tick", gameLoop);
		// init();
		
	});

	quit_btn = new createjs.Bitmap('images/quit game.png');
	quit_btn.x = 315;
	quit_btn.y = 380;
	quit_btn.visible = false;
	stage.addChild(quit_btn);
	quit_btn.on("click", function(event) {
		endGame();
		initCaves(1);
		createjs.Ticker.addEventListener("tick", gameLoop);
		window.parent.change_page('postcard_b');
		// window.history.back();
	 });*/


	diver_mc = new lib.diver();
	var pt = getLocationFromCell(script.startCellDiver.row, script.startCellDiver.col);
	diver_mc.x = pt.x;
	diver_mc.y = pt.y;
	diver_mc.scaleX = script.configDiver.scaleX;
	diver_mc.scaleY = script.configDiver.scaleY;
	diver_mc.rotation = script.configDiver.rotation;
	diver_mc['isHittingWall'] = false;
	diver_mc['isLosingLife'] = false;
	caves_mc.addChild(diver_mc);

	

	/*
	btn_up = new lib.btn_up();
	btn_up.x = 774;
	btn_up.y = 435;
	// btn_up.rotation = -90;
	// btn_up.scaleX = navButtonScale;
	// btn_up.scaleY = navButtonScale;
	btn_up.on("mousedown", function(event) {
		console.log('mousedown');
		keydown({'which':38})
	});
	btn_up.on("mouseup", function(event) {
		keyup();
	});
	stage.addChild(btn_up);

	btn_down = new lib.btn_right();
	
	btn_down.regX = btn_down.nominalBounds.width/2;
	btn_down.regY = btn_down.nominalBounds.height/2;
	btn_down.x = 792;
	btn_down.y = 580;
	// btn_down.scaleX = navButtonScale;
	// btn_down.scaleY = navButtonScale;
	btn_down.rotation = 90;
	//btn_down.rotation = 180;
	btn_down.on("mousedown", function(event) {
		keydown({'which':40})
	});
	btn_down.on("mouseup", function(event) {
		keyup()
	});
	stage.addChild(btn_down);

	btn_left = new lib.btn_left();
	
	btn_left.regX = btn_left.nominalBounds.width/2;
	btn_left.regY = btn_left.nominalBounds.height/2;
	btn_left.x = 727;
	btn_left.y = 520;
	// btn_left.scaleX = navButtonScale;
	// btn_left.scaleY = navButtonScale;	
	//btn_left.rotation = 270;
	btn_left.on("mousedown", function(event) {
		keydown({'which':37})
	});
	btn_left.on("mouseup", function(event) {
		keyup()
	});
	stage.addChild(btn_left);

	btn_right = new lib.btn_right();
	btn_right.regX = btn_right.nominalBounds.width/2;
	btn_right.regY = btn_right.nominalBounds.height/2;
	btn_right.x = 860;
	btn_right.y = 520;
	// btn_right.scaleX = navButtonScale;
	// btn_right.scaleY = navButtonScale;
	//btn_right.rotation = 90;
	btn_right.on("mousedown", function(event) {
		keydown({'which':39})
	});
	btn_right.on("mouseup", function(event) {
		keyup()
	});
	stage.addChild(btn_right);
*/

	// controls = lib.control();



	console.log(script.configDiver);
	
	initLevel(level);

	
	nextlevel = new createjs.Bitmap('images/popup.png');
	nextlevel.x = 240;
	nextlevel.y = 135;
	nextlevel.visible = false;
	stage.addChild(nextlevel);
	nextlevel.on('click', function(event){
		console.log('Next level click');
		clearStage();
		initCaves(currentLevel);
	});


	if(txtNumLife_mc != null){
		var len = txtNumLife_mc.children.length;
		for (var i = len - 1; i >= 0; i--) {
			txtNumLife_mc.removeChildAt(i);
		};
		
		heart_w = 17;
		heart_h = 30;
		heart_y = -80;
		for(var k=0;k<numberLife;k++){
			var heart = new lib.heart();
			heart.x = k*heart_w - 280;
			heart.y = heart_y;
			txtNumLife_mc.addChild(heart);
		}
	}

	
	document.onkeydown = keydown;
	
	document.onkeyup = keyup;

	isfirstKeyPress = 0;
}

function initLevel(index){
	var hearts = new Array();
	
	for(var i=0; i<(5-index); i++){
		hearts.push(Math.floor(Math.random()*map_mc.children.length));
	}
	
	maxNumberHeart = 5 - index;

	totalNumberPearl = map_mc.children.length - maxNumberHeart;
	
	for(var i=0; i<map_mc.children.length; i++){
		caves_mc.children[i].pearl.visible = true;
		map_mc.children[i].pearl.visible = true;
		map_mc.children[i].pearl.scaleX = 2.0;
		map_mc.children[i].pearl.scaleY = 2.0;
		caves_mc.children[i].pearl.gotoAndStop(0);
		map_mc.children[i].pearl.gotoAndStop(0);
		for(var j=0; j<hearts.length; j++){
			if (i==hearts[j]){
				caves_mc.children[i].pearl.gotoAndStop(1);
				map_mc.children[i].pearl.gotoAndStop(1);
				map_mc.children[i].pearl['isheart'] = true;
				break;
			}
			map_mc.children[i].pearl['isheart'] = false;
		}
	}
	
	if (baddies!=null){
		for(var i=0; i<baddies.jellyfish.length; i++) caves_mc.removeChild(baddies.jellyfish[i].sprite);
		for(var i=0; i<baddies.crabs.length; i++) caves_mc.removeChild(baddies.crabs[i].sprite);
		for(var i=0; i<baddies.squid.length; i++) caves_mc.removeChild(baddies.squid[i].sprite);
	}
	
	baddies = { jellyfish:new Array, crabs:new Array(), squid:new Array };
	//Directions 0-right 1-left 2-up 3-down
	
	// var locs = new Array( {row:4, col:0}, {row:1, col:5}, {row:6, col:1}, {row:12, col:3} );
	var locs = script.positionBaddies.jellyfish;
	for(var i=0; i<index; i++){
		var idx =  Math.floor(Math.random()*locs.length);
		var jellyfish = new lib.jellyfish_new_1();
		var pt = getLocationFromCell(locs[idx].row, locs[idx].col);
		console.log( "jellyfish cell(" + locs[idx].row + "," + locs[idx].col + ") pos(" + pt.x + ", " + pt.y + ")");
		jellyfish.x = pt.x-20;
		jellyfish.y = pt.y+50;
		// jellyfish.stop();
		caves_mc.addChild(jellyfish);
		baddies.jellyfish.push({ sprite:jellyfish, direction:3 });
	}
	
	// var locs = new Array( {row:17, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2} );
	var locs = script.positionBaddies.crab;
	for(var i=0; i<index; i++){
		var idx =  Math.floor(Math.random()*locs.length);
		var crab = new lib.crabs_new_1();
		var pt = getLocationFromCell(locs[idx].row, locs[idx].col);
		console.log( "crab cell(" + locs[idx].row + "," + locs[idx].col + ") pos(" + pt.x + ", " + pt.y + ")");
		crab.x = pt.x + 5;
		crab.y = pt.y - 15;
		// crab.stop();
		caves_mc.addChild(crab);
		baddies.crabs.push({ sprite:crab, direction:0 } );
	}
	
	console.log('initLevel', index);
	/*if (index>2){
		var locs = script.positionBaddies.squid;
		for(var i=0; i<(index-2); i++){
			var idx =  Math.floor(Math.random()*locs.length);
			var squid = new lib.squid();
			var pt = getLocationFromCell(locs[idx].row, locs[idx].col);
			console.log( "squid cell(" + locs[idx].row + "," + locs[idx].col + ") pos(" + pt.x + ", " + pt.y + ")");
			squid.x = pt.x;
			squid.y = pt.y;
			caves_mc.addChild(squid);
			baddies.squid.push({ sprite:squid, direction:3 } );
		}
	}*/
	
	//Bring diver to front
	var diverIndex = caves_mc.children.indexOf(diver_mc);
	caves_mc.children.splice(diverIndex, 1);
	caves_mc.children.push(diver_mc);

/*
	score_board = new lib.score_bg;
	score_board.x = 655;
	score_board.y = 10;
	stage.addChild(score_board);*/
	
	// Draw text score
	txtScore_mc = new lib.txt_score();
	txtScore_mc.x = 935;
	txtScore_mc.y = 170;
	txtScore_mc.text.font = "20pt 'thomas_headlinebold'";
	txtScore_mc.text.text = current_score;
	txtScore_mc.text.lineWidth = 97;
	stage.addChild(txtScore_mc);

	// Draw text number remain pearls
	txtPearls_mc = new lib.txt_score();
	txtPearls_mc.x = 935;
	txtPearls_mc.y = 225;
	txtPearls_mc.text.font = "20pt 'thomas_headlinebold'";
	txtPearls_mc.text.text = totalNumberPearl - pearlsCollectedCount;
	txtPearls_mc.text.lineWidth = 97;
	stage.addChild(txtPearls_mc);
	
	// Draw text numberLife
	txtNumLife_mc = new lib.txt_numlife();
	txtNumLife_mc.x = 965;
	txtNumLife_mc.y = 266;
	//txtNumLife_mc.text.text = numberLife;
	stage.addChild(txtNumLife_mc);
	
	
	createjs.Ticker.addEventListener("tick", gameLoop);
	gameActive = true;

}

function getLocationFromCell(row, col){
	return { x:col*80+40, y:row*80+40 };
}

function getCellFromLocation(x, y){
	return { col:Math.floor((x-0)/80), row:Math.floor((y-0)/80) };
}

function getLegalMoveFromLocation(x, y){
	var cell = getCellFromLocation(x, y);
	if (cell.row>=caves.length || cell.row<0 || cell.col>=caves[0].length || cell.col<0) console.log( cell.row + "," + cell.col );
	return getLegalMoveFromCell(cell.row, cell.col);
}

function getLegalMoveFromCell(row, col){

	var num = -1;
	
	if (row>=0 && row<caves.length && col>=0 && col<caves[0].length) num = caves[row][col];
	
	if (num==-1){
		console.log( row + ", " + col);
	}
	
	var result = {left:false, right:false, up:false, down:false};
	
	switch(num){
		case 0:
		result = {left:false, right:true, up:true, down:true};
		break;
		case 1:
		result = {left:true, right:true, up:true, down:false};
		break;
		case 2:
		result = {left:true, right:false, up:true, down:true};
		break;
		case 3:
		result = {left:true, right:true, up:false, down:true};
		break;
		case 4:
		result = {left:false, right:true, up:false, down:true};
		break;
		case 5:
		result = {left:false, right:true, up:true, down:false};
		break;
		case 6:
		result = {left:true, right:false, up:true, down:false};
		break;
		case 7:
		result = {left:true, right:false, up:false, down:true};
		break;
		case 8:
		result = {left:false, right:false, up:true, down:false};
		break;
		case 9:
		result = {left:true, right:false, up:false, down:false};
		break;
		case 10:
		result = {left:false, right:false, up:false, down:true};
		break;
		case 11:
		result = {left:false, right:true, up:false, down:false};
		break;
		case 12:
		result = {left:false, right:false, up:true, down:true};
		break;
		case 13:
		result = {left:true, right:true, up:false, down:false};
		break;
		case 14:
		result = {left:true, right:true, up:true, down:true};
		break;
	}
	//console.log('getLegalMoveFromCell', row, col, num, result);
	return result;
}

var timerHideShip = 0;
var hideShipStartTime;
var boatOffsetY;
var startBoatOffsetY = 272;

function hideShipOfBG(){
	//console.log('hideShipOfBG');
	var d = new Date();
	hideShipStartTime = d.getTime();
	boatOffsetY = startBoatOffsetY;
	
	timerHideShip = setInterval(function(){
		var d = new Date();
		var elapsedTime = (d.getTime() - hideShipStartTime) / 2000.0;
		//console.log("elapsedTime:" + elapsedTime + " hideShipStartTime:" + hideShipStartTime);
		if (elapsedTime>1){
			clearInterval(timerHideShip);
			boatOffsetY = 0;
		}else{
			boatOffsetY = startBoatOffsetY * (1.0 - elapsedTime);//Value goes from startBoatOffsetY to 0 over 1 second
			//console.log("elapsedTime:" + elapsedTime + " boatOffsetY:" + boatOffsetY);
		}
	}, 10);
}

function centreCaves(){
	//console.log('caves_mc.y:' + caves_mc.y + " diver_mc.y:" + diver_mc.y + " height_top_bg:" + height_top_bg);
	y_diver = diver_mc.y;
	
// console.log('diver_mc.y',diver_mc.y);
	y_diver = diver_mc.y;
	if(y_diver < height_top_bg){
		var targetY = 270 - y_diver;
		// console.log('targetY', targetY);
		// if (targetY>0) targetY = height_top_bg;
		if (targetY<-((caves.length*90))) targetY = -((caves.length*90));
		caves_mc.y = targetY + boatOffsetY;
		bg_mc.y = caves_mc.y - height_top_bg;	
		// fish_anim_mc.y = bg_mc.y + 245;
	}else{
		var targetY = 318.5 - y_diver;
		if (targetY>0) targetY =  height_top_bg - y_diver/2; 
		if (targetY<-((caves.length*90)-150)) targetY = -((caves.length*90)-150);
		caves_mc.y = targetY-50 + boatOffsetY;
		bg_mc.y = caves_mc.y - height_top_bg ;	
		// fish_anim_mc.y = bg_mc.y + 245;
	}


/*
	y1 = y_diver + height_top_bg;
	if( y1 < 553){

	}else{
		var targetY = 270 - y_diver;
		if (targetY>0) targetY =  height_top_bg - y_diver/2; 
		if (targetY<-((caves.length*80)-540)) targetY = -((caves.length*80)-540);
		caves_mc.y = targetY;
		bg_mc.y = caves_mc.y - height_top_bg ;	
		fish_anim_mc.y = bg_mc.y + 245;
		
	}*/

	
}

function levelComplete(){
	// console.log("Level complete need to spawn a new level");
	// createjs.Ticker.removeEventListener("tick", gameLoop);
	// gameActive = false;
	
	// removeMapsAndDiver();

	// len = stage.children.length;
	// for (var i = len - 1; i > 0; i--) {
	// 	stage.removeChildAt(i);
	// };

	// stage.removeChildAt(0);
	// pearlsCollectedCount = 0;

	second_number_start = second_number_start < min_time_complete_level ? min_time_complete_level : second_number_start;
	if(second_number_start >= min_time_complete_level && second_number_start <= max_time_complete_level){
		bonus_score = (1.0 - ((second_number_start - min_time_complete_level)/(max_time_complete_level - min_time_complete_level))) * 100;
		bonus_score = Math.ceil(bonus_score);

		$.ajax({
			type:'post',
			url: baseUrlApi + url_api.inc,
			data:{score:bonus_score, gameId:gameId},
			success: function(data){

			}
		});

		current_score += bonus_score;
	}


	if(currentLevel < MAXLEVEL){
		playSound('level');
		createjs.Ticker.removeEventListener("tick", gameLoop);
		currentLevel++;		
		nextlevel.visible = true;

		//setTimeout(initCaves( currentLevel ), 10000);		
	}else{		
		clearStage();
		// email = localStorage.getItem("email");
		
		//complete 4 levels
		window.parent.change_page('well_done');

		sendScore();
	}

}

var score_before_send = 0;
function sendScore(){
	email = localStorage.getItem("email");
	// email = $.cookie('email');
	// console.log('email', email);
	// if(localStorage.getItem("email")){
	if(email){

		gameActive = false;
		isfirstKeyPress = 0;
		createjs.Ticker.removeEventListener("tick", gameLoop);

		document.getElementById('loader').style.display = 'block';


		// console.log('$.cookie("scores")', $.cookie("scores"));
		// bonus_score_share = typeof $.cookie("scores") == 'undefined' ? 0 : parseInt($.cookie("scores"));
		bonus_score_share = localStorage.getItem("scores") == null ? 0 : parseInt(localStorage.getItem("scores"));

		score_before_send = current_score + bonus_score_share;

		endGame();

		if(bonus_score_share > 0){
			$.ajax({
				type:'post',
				url: baseUrlApi + url_api.socialscore,
				data:{score:bonus_score_share, email:email, gameId:gameId},
				success: function(data){
					// user win.
					$.ajax({
						type:'post',
						url: baseUrlApi + url_api.sendscore,
						dataType:"json",
						data:{score:score_before_send, email:email, gameId:gameId},
						success: function(data){

							/*if(data.topten == true){
								window.parent.change_page('congratulations');
							}else{
								if(data.is_nearhigh == true){
									window.parent.change_page('well_done');
								}else{
									window.parent.change_page('sorry');
								}
							}*/

							set_cookie('scores', 0);

							initCaves();

							document.getElementById('loader').style.display = 'none';
						}
					});
				}
			});
		}else{
			// user win.
			$.ajax({
				type:'post',
				url: baseUrlApi + url_api.sendscore,
				dataType:"json",
				data:{score:score_before_send, email:email, gameId:gameId},
				success: function(data){
					/*if(data.topten == true){
						window.parent.change_page('congratulations');
					}else{
						if(data.is_nearhigh == true){
							window.parent.change_page('well_done');
						}else{
							window.parent.change_page('sorry');
						}
					}*/

					initCaves();
					
					document.getElementById('loader').style.display = 'none';
				}
			});
		}

		
	}else{
		endGame();
	}
}

function clearStage(){
	removeMapsAndDiver();

	len = stage.children.length;
	for (var i = len - 1; i >= 0; i--) {
		stage.removeChildAt(i);
	};

	pearlsCollectedCount = 0;
}

function gameLoop(event){
	// console.log("caves_mc.y:" + caves_mc.y);
	
	if(!gameActive && isfirstKeyPress == 0){return;}

	if(isfirstKeyPress == 1){
		time_start = Math.floor((new Date()).getTime() / 1000);
		hideShipOfBG();
		isfirstKeyPress = 2;
	}

	if(forwardTouch){
		keydown({which:previousKey});
	}


	time_current = Math.floor((new Date()).getTime() / 1000);
	second_number_start = time_current - time_start;

	//Move diver
	if (speed>0){
		var legalMove = getLegalMoveFromLocation(diver_mc.x, diver_mc.y);
		// console.log('gameloop', legalMove, speed);
		
		x_current_diver = diver_mc.x;
		y_current_diver = diver_mc.y;
		
		switch(diver_mc.rotation){
			case 0:
			if (legalMove.right) diver_mc.x += speed;
			break;
			case 90:
			if (legalMove.down){
				diver_mc.y += speed;
				centreCaves();
			}
			break;
			case 180:
			if (legalMove.left) diver_mc.x -= speed;
			break;
			case 270:
			if (legalMove.up){
				diver_mc.y -= speed;
				centreCaves();
			}
			break;
		}
		
		// if not change x and y. this is hitwall animation.
		if(x_current_diver == diver_mc.x && y_current_diver == diver_mc.y){
			if(!diver_mc.isHittingWall){
				diver_mc.gotoAndPlay('hitwall');
				diver_mc.isHittingWall = true;
			}
		}
		
		// if (!forward) speed--;
		if (speed > MINSPEED && !forward) speed--;
		
		if(x_current_diver != diver_mc.x || y_current_diver != diver_mc.y){
			if (diver_mc.x>diverLimits.right) diver_mc.x = diverLimits.right;
			if (diver_mc.x<diverLimits.left) diver_mc.x = diverLimits.left;
			if (diver_mc.y<diverLimits.top) diver_mc.y = diverLimits.top;
			if (diver_mc.y>diverLimits.bottom) diver_mc.y = diverLimits.bottom;	
			
			var cell = getCellFromLocation(diver_mc.x, diver_mc.y);
			
			if (cell.row != mapDiverCell.row || cell.col!=mapDiverCell.col){
				mapDiverCell.mc.visible = false;
				mapDiverCell.row = cell.row;
				mapDiverCell.col = cell.col;
				var index = mapDiverCell.row * caves[0].length + mapDiverCell.col;
				mapDiverCell.mc = map_mc.children[index].diverIcon_mc;
				mapDiverCell.mc.visible = true;
				mapDiverCell.mc.rotation = diver_mc.rotation;
				pearl = map_mc.children[index].pearl;
				if (pearl.visible){
					pearl.visible = false;
					caves_mc.children[index].pearl.visible = false;
					if(!pearl.isheart){
						// playSound('collectpearl');
						pearlsCollectedCount++;
						current_score++;
						// change score
						txtScore_mc.text.text = current_score;
						txtPearls_mc.text.text = totalNumberPearl - pearlsCollectedCount;
						// console.log(pearlsCollectedCount , map_mc.children.length - maxNumberHeart, map_mc.children.length, maxNumberHeart);
						
						$.ajax({
							type:'post',
							url: baseUrlApi + url_api.inc,
							data:{score:1, gameId:gameId},
							success: function(data){

							}
						});

						if (pearlsCollectedCount == totalNumberPearl) 
							levelComplete();
					}else{
						playSound('life');
						numberLife++;
						// change life						
						//txtNumLife_mc.text.text = numberLife;
					}
				}
			}
		}
		
	}

	// redraw hearts for numlife
	if(txtNumLife_mc != null){
		var len = txtNumLife_mc.children.length;
		for (var i = len - 1; i >= 0; i--) {
			txtNumLife_mc.removeChildAt(i);
		};
		
		heart_w = 17;
		heart_h = 30;
		heart_y = -80;
		for(var k=0;k<numberLife;k++){
			var heart = new lib.heart();
			heart.x = k*heart_w - 280;
			heart.y = heart_y;
			txtNumLife_mc.addChild(heart);
		}
	}
	
	
	//Move baddies
	for(var i=0; i<baddies.jellyfish.length; i++) moveBaddie(baddies.jellyfish[i]);
	for(var i=0; i<baddies.crabs.length; i++) moveBaddie(baddies.crabs[i]);

/*
	if(currentLevel > 2){
		for(var i=0; i<baddies.squid.length; i++) moveBaddie(baddies.squid[i]);
	}*/
}

function moveBaddie(baddie){

	if(diver_mc == null) return;

	//{sprite direction}
	//Directions 0-right 1-left 2-up 3-down
	var pt = { x:baddie.sprite.x, y:baddie.sprite.y };
	switch(baddie.direction){
		case 0://right
		pt.x += currentLevel;
		break;
		case 1://left
		pt.x -= currentLevel;
		break;
		case 2://
		pt.y -= currentLevel;
		break;
		case 3:
		pt.y += currentLevel;
		break;
	}
	
	var legalMove = getLegalMoveFromLocation(pt.x, pt.y);
	var ok = true;
	
	switch(baddie.direction){
		case 0://right
		ok = legalMove.right;
		if (!ok) baddie.direction = 1;
		break;
		case 1://left
		ok = legalMove.left;
		if (!ok) baddie.direction = 0;
		break;
		case 2://
		ok = legalMove.up;
		if (!ok) baddie.direction = 3;
		break;
		case 3:
		ok = legalMove.down;
		if (!ok) baddie.direction = 2;
		break;
	}
	
	if (ok){
		baddie.sprite.x = pt.x;
		baddie.sprite.y = pt.y;
	}
	//console.log('moveBaddie', pt.x, diver_mc.x, pt.y, diver_mc.y, baddie);
	
	cell1 = getCellFromLocation(diver_mc.x, diver_mc.y);
	cell2 = getCellFromLocation(baddie.sprite.x, baddie.sprite.y);

	if (cell1.row == cell2.row && cell1.col==cell2.col && !diver_mc.isLosingLife){
		numberLife--;
		if(numberLife < 0){
			numberLife = 0;
		}
		
		//txtNumLife_mc.text.text = numberLife;
		
		if(numberLife != 0){
			diver_mc.gotoAndPlay('lostlife');

			diver_mc.isLosingLife = true;

			setTimeout(function(){
				diver_mc.isLosingLife = false;
			}, 2000);
		}else{
			// game over
			/*createjs.Tween.get(gameover, {loop:true})
                         .to({x: 175, y: 150, visible: true }, 500, createjs.Ease.linear)
                         .wait(5000);*/		

			diver_mc.gotoAndPlay('die');

			// gameover.visible = replay_btn.visible = quit_btn.visible = true;
			
			document.onkeydown = null;
			document.onkeyup = null;

			window.parent.change_page('sorry');

			sendScore();

			endGame();

			// endGame();
			// initCaves(1);
			// window.parent.change_page('sorry');					
		}
	}
}

function resetStartDiver(){
	var pt = getLocationFromCell(script.startCellDiver.row, script.startCellDiver.col);
	console.log('resetStartDiver', pt, scripts, script.startCellDiver);
	diver_mc.x = pt.x;
	diver_mc.y = pt.y;
	diver_mc.scaleX = script.configDiver.scaleX;
	diver_mc.scaleY = script.configDiver.scaleY;
	diver_mc.rotation = script.configDiver.rotation;
	diver_mc.isHittingWall = false;
	diver_mc.baddieDirectionHit = -1;
}

function removeMapsAndDiver(){
	stage.removeChild(caves_mc, map_mc, diver_mc, bg_mc, txtScore_mc, txtPearls_mc, txtNumLife_mc);

	// remove txt Score
	stage.removeChildAt(3);	
	stage.removeChildAt(2);	
	stage.removeChildAt(1);	

	speed = 0;
	caves_mc = null;
	map_mc = null;
	diver_mc = null;
	bg_mc = null;
	txtNumLife_mc = null;
	txtScore_mc = null;
	txtPearls_mc = null;
}

function endGame(){
	removeMapsAndDiver();

	len = stage.children.length;
	for (var i = len - 1; i >= 0; i--) {
		stage.removeChildAt(i);
	};

	
	txtScore_mc = null;
	txtNumLife_mc = null;
	txtPearls_mc = null;

	// stage.children[0].play();

	gameActive = false;

	pearlsCollectedCount = 0;
	current_score = 0;
	numberLife = 2;
	currentLevel = 1;

	createjs.Ticker.removeEventListener("tick", gameLoop);
}

function restartGame(){
	speed = 0;
	forward = false;
	previousKey = 40;
	mapDiverCell = { row:0, col:6 };
	pearlsCollectedCount = 0;
	current_score = 0;
	numberLife = 2;
	gameActive = false;
	isfirstKeyPress = 0;

	time_start = 0;
	time_current = 0;
	second_number_start = 0;

	totalNumberPearl = 0;
}

function set_cookie(key,value){
	/*var options ={
	  	path: "/",
	  	expires:365
	  }
	 $.cookie(key, value, options);*/
	 localStorage.setItem(key, value);
}