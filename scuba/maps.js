

/*
var caves = new Array(new Array( 1, 1, 1, 1, 1, 1, 2), new Array( 4, 7, 4, 3, 7,10,12), new Array(12, 5, 2, 5, 2, 0, 2), new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), new Array(12, 8, 0, 1, 1, 2,12), new Array(12, 4, 6,10, 4, 1, 2), new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), new Array(11, 2, 5, 2,10, 4, 6), new Array(10,12, 4, 6, 0,14, 7), new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), new Array( 0,13, 2, 0, 9,10,12), new Array(12, 4, 2,12,11, 2,12), new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), new Array( 5,13, 6,11, 6, 5, 6));
*/


var MAXSPEED = 15;
var MINSPEED = 5;
var maxNumberHeart = 5;
var MAXLEVEL = 4;



var scripts = new Array();

// Level 1
scripts[1] = {"map" : new Array(new Array( 13, 13, 13, 13, 13, 13,2), 
						new Array( 4, 7, 4, 3, 7,10,12), 
						new Array(12, 5, 2, 5, 2, 0, 2), 
						new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), 
					  new Array(12, 8, 0, 1, 1, 2,12), 
					  new Array(12, 4, 6,10, 4, 1, 2), 
					  new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), 
					  new Array(11, 2, 5, 2,10, 4, 6), 
					  new Array(10,12, 4, 6, 0,14, 7), 
					  new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), 
					  new Array( 0,13, 2, 0, 9,10,12), 
					  new Array(12, 4, 2,12,11, 2,12), 
					  new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), 
					  new Array( 5,13, 6,11, 6, 5, 6))
			,"startCellDiver":{row:0,col:6}
			,"configDiver":{
				"rotation": 90,
				"scaleX":0.7,
				"scaleY":0.7
			}
			,"positionBaddies":{
				"jellyfish":[{row:4, col:0}, {row:1, col:5}, {row:6, col:1}, {row:12, col:3}],
				"crab":[{row:17, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2}],
				"squid":[{row:7, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2}]
			}
		};


// Level 2
scripts[2] = {"map" : new Array(new Array( 13, 13, 13, 13, 13, 13,2), 
						new Array( 4, 7, 4, 3, 7,10,12), 
						new Array(12, 5, 2, 5, 2, 0, 2), 
						new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), 
					  new Array(12, 8, 0, 1, 1, 2,12), 
					  new Array(12, 4, 6,10, 4, 1, 2), 
					  new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), 
					  new Array(11, 2, 5, 2,10, 4, 6), 
					  new Array(10,12, 4, 6, 0,14, 7), 
					  new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), 
					  new Array( 0,13, 2, 0, 9,10,12), 
					  new Array(12, 4, 2,12,11, 2,12), 
					  new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), 
					  new Array( 5,13, 6,11, 6, 5, 6))
			,"startCellDiver":{row:0,col:6}
			,"configDiver":{
				"rotation": 90,
				"scaleX":0.7,
				"scaleY":0.7
			}
			,"positionBaddies":{
				"jellyfish":[{row:4, col:0}, {row:1, col:5}, {row:6, col:1}, {row:12, col:3}],
				"crab":[{row:17, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2}]
			}
		};

// Level 3
scripts[3] = {"map" : new Array(new Array( 13, 13, 13, 13, 13, 13,2), 
						new Array( 4, 7, 4, 3, 7,10,12), 
						new Array(12, 5, 2, 5, 2, 0, 2), 
						new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), 
					  new Array(12, 8, 0, 1, 1, 2,12), 
					  new Array(12, 4, 6,10, 4, 1, 2), 
					  new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), 
					  new Array(11, 2, 5, 2,10, 4, 6), 
					  new Array(10,12, 4, 6, 0,14, 7), 
					  new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), 
					  new Array( 0,13, 2, 0, 9,10,12), 
					  new Array(12, 4, 2,12,11, 2,12), 
					  new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), 
					  new Array( 5,13, 6,11, 6, 5, 6))
			,"startCellDiver":{row:0,col:6}
			,"configDiver":{
				"rotation": 90,
				"scaleX":0.7,
				"scaleY":0.7
			}
			,"positionBaddies":{
				"jellyfish":[{row:4, col:0}, {row:1, col:5}, {row:6, col:1}, {row:12, col:3}],
				"crab":[{row:17, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2}],
				"squid":[ {row:9, col:6}, {row:15, col:0} ]
			}
		};

// Level 4
scripts[4] = {"map" : new Array(new Array( 13, 13, 13, 13, 13, 13,2), 
						new Array( 4, 7, 4, 3, 7,10,12), 
						new Array(12, 5, 2, 5, 2, 0, 2), 
						new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), 
					  new Array(12, 8, 0, 1, 1, 2,12), 
					  new Array(12, 4, 6,10, 4, 1, 2), 
					  new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), 
					  new Array(11, 2, 5, 2,10, 4, 6), 
					  new Array(10,12, 4, 6, 0,14, 7), 
					  new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), 
					  new Array( 0,13, 2, 0, 9,10,12), 
					  new Array(12, 4, 2,12,11, 2,12), 
					  new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), 
					  new Array( 5,13, 6,11, 6, 5, 6))
			,"startCellDiver":{row:0,col:6}
			,"configDiver":{
				"rotation": 180,
				"scaleX":0.7,
				"scaleY":-0.7
			}
			,"positionBaddies":{
				"jellyfish":[{row:4, col:0}, {row:1, col:5}, {row:6, col:1}, {row:12, col:3}],
				"crab":[{row:17, col:0}, {row:17, col:5}, {row:3, col:3}, {row:7, col:2}]
			}
		};
/*
scripts[] = new Array(new Array( 13, 13, 13, 13, 13, 13,7), 
						new Array( 4, 7, 4, 3, 7,10,12), 
						new Array(12, 5, 2, 5, 2, 0, 2), 
						new Array(5, 7, 0,13, 1, 2,12),
					  new Array( 4, 2,12, 4, 7,12,12), 
					  new Array(12, 8, 0, 1, 1, 2,12), 
					  new Array(12, 4, 6,10, 4, 1, 2), 
					  new Array( 0, 2,11, 1, 1, 7,12),
					  new Array( 8,12, 4, 3,13, 6,12), 
					  new Array(11, 2, 5, 2,10, 4, 6), 
					  new Array(10,12, 4, 6, 0,14, 7), 
					  new Array(12,12,12,11,14, 6,12),
					  new Array( 0, 6, 0, 3, 6,11, 2), 
					  new Array( 0,13, 2, 0, 9,10,12), 
					  new Array(12, 4, 2,12,11, 2,12), 
					  new Array( 8, 0, 1, 2, 4,14, 2),
					  new Array( 4, 6, 4, 1,14, 2,12), 
					  new Array( 5,13, 6,11, 6, 5, 6));
*/