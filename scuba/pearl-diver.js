(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 825,
	height: 553,
	fps: 24,
	color: "#6699FF",
	manifest: [
		{src:"images/bg3.jpg", id:"bg3"},
		{src:"images/Bitmap19.png", id:"Bitmap19"},
		{src:"images/Bitmap20.png", id:"Bitmap20"},
		{src:"images/Bitmap23.png", id:"Bitmap23"},
		{src:"images/Bitmap24.png", id:"Bitmap24"},
		{src:"images/circle.png", id:"circle"},
		{src:"images/crabs_new.png", id:"crabs_new"},
		{src:"images/down.png", id:"down"},
		{src:"images/fish.png", id:"fish"},
		{src:"images/heart.png", id:"heart"},
		{src:"images/heart2.png", id:"heart2"},
		{src:"images/Image101.png", id:"Image101"},
		{src:"images/Image103.png", id:"Image103"},
		{src:"images/Image105.png", id:"Image105"},
		{src:"images/Image107.png", id:"Image107"},
		{src:"images/Image109.png", id:"Image109"},
		{src:"images/Image111.png", id:"Image111"},
		{src:"images/Image113.png", id:"Image113"},
		{src:"images/Image115.png", id:"Image115"},
		{src:"images/Image117.png", id:"Image117"},
		{src:"images/Image119.png", id:"Image119"},
		{src:"images/Image121.png", id:"Image121"},
		{src:"images/Image123.png", id:"Image123"},
		{src:"images/Image125.png", id:"Image125"},
		{src:"images/Image127.png", id:"Image127"},
		{src:"images/Image129.png", id:"Image129"},
		{src:"images/Image131.png", id:"Image131"},
		{src:"images/Image133.png", id:"Image133"},
		{src:"images/Image135.png", id:"Image135"},
		{src:"images/Image137.png", id:"Image137"},
		{src:"images/Image139.png", id:"Image139"},
		{src:"images/Image141.png", id:"Image141"},
		{src:"images/Image143.png", id:"Image143"},
		{src:"images/Image145.png", id:"Image145"},
		{src:"images/Image147.png", id:"Image147"},
		{src:"images/Image149.png", id:"Image149"},
		{src:"images/Image151.png", id:"Image151"},
		{src:"images/Image153.png", id:"Image153"},
		{src:"images/Image155.png", id:"Image155"},
		{src:"images/Image157.png", id:"Image157"},
		{src:"images/Image159.png", id:"Image159"},
		{src:"images/Image161.png", id:"Image161"},
		{src:"images/Image163.png", id:"Image163"},
		{src:"images/Image165.png", id:"Image165"},
		{src:"images/Image167.png", id:"Image167"},
		{src:"images/Image169.png", id:"Image169"},
		{src:"images/Image171.png", id:"Image171"},
		{src:"images/Image173.png", id:"Image173"},
		{src:"images/Image175.png", id:"Image175"},
		{src:"images/Image177.png", id:"Image177"},
		{src:"images/Image179.png", id:"Image179"},
		{src:"images/Image181.png", id:"Image181"},
		{src:"images/Image183.png", id:"Image183"},
		{src:"images/Image185.png", id:"Image185"},
		{src:"images/Image187.png", id:"Image187"},
		{src:"images/Image189.png", id:"Image189"},
		{src:"images/Image191.png", id:"Image191"},
		{src:"images/Image193.png", id:"Image193"},
		{src:"images/Image195.png", id:"Image195"},
		{src:"images/Image197.png", id:"Image197"},
		{src:"images/Image199.png", id:"Image199"},
		{src:"images/Image201.png", id:"Image201"},
		{src:"images/Image203.png", id:"Image203"},
		{src:"images/Image205.png", id:"Image205"},
		{src:"images/Image207.png", id:"Image207"},
		{src:"images/Image209.png", id:"Image209"},
		{src:"images/Image211.png", id:"Image211"},
		{src:"images/Image213.png", id:"Image213"},
		{src:"images/Image215.png", id:"Image215"},
		{src:"images/Image217.png", id:"Image217"},
		{src:"images/Image219.png", id:"Image219"},
		{src:"images/Image221.png", id:"Image221"},
		{src:"images/Image223.png", id:"Image223"},
		{src:"images/Image225.png", id:"Image225"},
		{src:"images/Image227.png", id:"Image227"},
		{src:"images/Image229.png", id:"Image229"},
		{src:"images/Image231.png", id:"Image231"},
		{src:"images/Image233.png", id:"Image233"},
		{src:"images/Image235.png", id:"Image235"},
		{src:"images/Image237.png", id:"Image237"},
		{src:"images/Image77.png", id:"Image77"},
		{src:"images/Image79.png", id:"Image79"},
		{src:"images/Image81.png", id:"Image81"},
		{src:"images/Image83.png", id:"Image83"},
		{src:"images/Image85.png", id:"Image85"},
		{src:"images/Image87.png", id:"Image87"},
		{src:"images/Image89.png", id:"Image89"},
		{src:"images/Image91.png", id:"Image91"},
		{src:"images/Image93.png", id:"Image93"},
		{src:"images/Image95.png", id:"Image95"},
		{src:"images/Image97.png", id:"Image97"},
		{src:"images/Image99.png", id:"Image99"},
		{src:"images/jellyfish_new.png", id:"jellyfish_new"},
		{src:"images/left.png", id:"left"},
		{src:"images/octopus_new.png", id:"octopus_new"},
		{src:"images/right.png", id:"right"},
		{src:"images/Symbol20003.png", id:"Symbol20003"},
		{src:"images/transparent_cell.png", id:"transparent_cell"},
		{src:"images/up.png", id:"up"},
		{src:"sounds/bonus.mp3", id:"bonus"},
		{src:"sounds/CLICK.mp3", id:"CLICK"},
		{src:"sounds/collectpearl.mp3", id:"collectpearl"},
		{src:"sounds/endgame.mp3", id:"endgame"},
		{src:"sounds/hit.mp3", id:"hit"},
		{src:"sounds/level.mp3", id:"level"},
		{src:"sounds/life.mp3", id:"life"},
		{src:"sounds/lostlife.mp3", id:"lostlife"},
		{src:"sounds/SPLASH14.mp3", id:"SPLASH14"},
		{src:"sounds/SPLASH8.mp3", id:"SPLASH8"},
		{src:"sounds/SPLOSH01.mp3", id:"SPLOSH01"}
	]
};



// symbols:



(lib.bg3 = function() {
	this.initialize(img.bg3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,533,1902);


(lib.Bitmap19 = function() {
	this.initialize(img.Bitmap19);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.Bitmap20 = function() {
	this.initialize(img.Bitmap20);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,12,12);


(lib.Bitmap23 = function() {
	this.initialize(img.Bitmap23);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,47);


(lib.Bitmap24 = function() {
	this.initialize(img.Bitmap24);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,24,24);


(lib.circle = function() {
	this.initialize(img.circle);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,76);


(lib.crabs_new = function() {
	this.initialize(img.crabs_new);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,40);


(lib.down = function() {
	this.initialize(img.down);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,35,39);


(lib.fish = function() {
	this.initialize(img.fish);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,389,109);


(lib.heart = function() {
	this.initialize(img.heart);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,22,22);


(lib.heart2 = function() {
	this.initialize(img.heart2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,22,22);


(lib.Image101 = function() {
	this.initialize(img.Image101);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image103 = function() {
	this.initialize(img.Image103);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image105 = function() {
	this.initialize(img.Image105);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image107 = function() {
	this.initialize(img.Image107);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image109 = function() {
	this.initialize(img.Image109);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image111 = function() {
	this.initialize(img.Image111);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image113 = function() {
	this.initialize(img.Image113);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image115 = function() {
	this.initialize(img.Image115);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image117 = function() {
	this.initialize(img.Image117);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image119 = function() {
	this.initialize(img.Image119);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image121 = function() {
	this.initialize(img.Image121);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image123 = function() {
	this.initialize(img.Image123);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image125 = function() {
	this.initialize(img.Image125);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image127 = function() {
	this.initialize(img.Image127);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image129 = function() {
	this.initialize(img.Image129);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image131 = function() {
	this.initialize(img.Image131);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image133 = function() {
	this.initialize(img.Image133);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image135 = function() {
	this.initialize(img.Image135);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image137 = function() {
	this.initialize(img.Image137);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image139 = function() {
	this.initialize(img.Image139);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image141 = function() {
	this.initialize(img.Image141);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image143 = function() {
	this.initialize(img.Image143);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image145 = function() {
	this.initialize(img.Image145);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image147 = function() {
	this.initialize(img.Image147);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image149 = function() {
	this.initialize(img.Image149);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image151 = function() {
	this.initialize(img.Image151);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image153 = function() {
	this.initialize(img.Image153);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image155 = function() {
	this.initialize(img.Image155);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image157 = function() {
	this.initialize(img.Image157);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image159 = function() {
	this.initialize(img.Image159);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image161 = function() {
	this.initialize(img.Image161);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image163 = function() {
	this.initialize(img.Image163);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image165 = function() {
	this.initialize(img.Image165);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image167 = function() {
	this.initialize(img.Image167);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image169 = function() {
	this.initialize(img.Image169);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image171 = function() {
	this.initialize(img.Image171);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image173 = function() {
	this.initialize(img.Image173);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image175 = function() {
	this.initialize(img.Image175);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image177 = function() {
	this.initialize(img.Image177);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image179 = function() {
	this.initialize(img.Image179);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image181 = function() {
	this.initialize(img.Image181);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image183 = function() {
	this.initialize(img.Image183);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image185 = function() {
	this.initialize(img.Image185);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image187 = function() {
	this.initialize(img.Image187);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image189 = function() {
	this.initialize(img.Image189);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image191 = function() {
	this.initialize(img.Image191);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image193 = function() {
	this.initialize(img.Image193);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image195 = function() {
	this.initialize(img.Image195);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image197 = function() {
	this.initialize(img.Image197);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image199 = function() {
	this.initialize(img.Image199);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image201 = function() {
	this.initialize(img.Image201);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image203 = function() {
	this.initialize(img.Image203);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image205 = function() {
	this.initialize(img.Image205);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image207 = function() {
	this.initialize(img.Image207);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image209 = function() {
	this.initialize(img.Image209);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image211 = function() {
	this.initialize(img.Image211);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image213 = function() {
	this.initialize(img.Image213);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image215 = function() {
	this.initialize(img.Image215);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image217 = function() {
	this.initialize(img.Image217);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image219 = function() {
	this.initialize(img.Image219);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image221 = function() {
	this.initialize(img.Image221);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image223 = function() {
	this.initialize(img.Image223);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image225 = function() {
	this.initialize(img.Image225);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image227 = function() {
	this.initialize(img.Image227);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image229 = function() {
	this.initialize(img.Image229);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image231 = function() {
	this.initialize(img.Image231);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image233 = function() {
	this.initialize(img.Image233);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image235 = function() {
	this.initialize(img.Image235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image237 = function() {
	this.initialize(img.Image237);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image77 = function() {
	this.initialize(img.Image77);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image79 = function() {
	this.initialize(img.Image79);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image81 = function() {
	this.initialize(img.Image81);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image83 = function() {
	this.initialize(img.Image83);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image85 = function() {
	this.initialize(img.Image85);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image87 = function() {
	this.initialize(img.Image87);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image89 = function() {
	this.initialize(img.Image89);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image91 = function() {
	this.initialize(img.Image91);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image93 = function() {
	this.initialize(img.Image93);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image95 = function() {
	this.initialize(img.Image95);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image97 = function() {
	this.initialize(img.Image97);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.Image99 = function() {
	this.initialize(img.Image99);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.jellyfish_new = function() {
	this.initialize(img.jellyfish_new);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,50);


(lib.left = function() {
	this.initialize(img.left);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.octopus_new = function() {
	this.initialize(img.octopus_new);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,40);


(lib.right = function() {
	this.initialize(img.right);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.Symbol20003 = function() {
	this.initialize(img.Symbol20003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.transparent_cell = function() {
	this.initialize(img.transparent_cell);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,80,80);


(lib.up = function() {
	this.initialize(img.up);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.upper_leg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1D2725").s().p("AAJgBIgJACIgIABIARgDg");
	this.shape.setTransform(23.2,7.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F6666").s().p("AgYArIgIAAQgYAAgPgCIgBAAIgHAAQgeAAghgIQgPgEgWgOIAggPIAQAHQAQAIALAEIABAAIABgBIAIAAIAIgBIAKgEIATgEQAdgIAMgBQAugFAxAAIAIgBQAXgPAVgKQAOgHAHgDIAGAAIABAAIAVgBQgNAVghAVQgZASgSAEIgIACIgLACIgMADQgoALgTADIgLABIgOgBg");
	this.shape_1.setTransform(31.9,5.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AjxA6QhRg+B2g2IgKgHIAJAGQAXAPAPAEQAgAHAeABIAHAAIABAAQAPACAYgBIAJAAQAOACANgCQASgCAngLIAMgDIALgDIAIgBQARgFAZgRQAigYANgVQDJBGiIBtQh+AmiRASQgOACgOAAQhQAAhSg9g");
	this.shape_2.setTransform(34.4,13.3);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(6.9,1.4,55.1,23.8);


(lib.txt_score = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Score 0", "24px 'Times New Roman'", "#C90101");
	this.text.lineHeight = 26;
	this.text.lineWidth = 87;
	this.text.setTransform(-245.9,-98.7);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-245.9,-98.7,90.9,30.6);


(lib.txt_numlife = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Life 1", "24px 'Times New Roman'", "#C90101");
	this.text.lineHeight = 26;
	this.text.lineWidth = 100;
	this.text.setTransform(-274.9,-84.4);

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-274.9,-84.4,104,30.6);


(lib.Tween16 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.octopus_new();
	this.instance.setTransform(-20,-20);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-20,40,40);


(lib.Tween15 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.octopus_new();
	this.instance.setTransform(-20,-20);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-20,40,40);


(lib.Tween14 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.jellyfish_new();
	this.instance.setTransform(-20,-25);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-25,40,50);


(lib.Tween13 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.jellyfish_new();
	this.instance.setTransform(-20,-25);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-25,40,50);


(lib.Tween12 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.crabs_new();
	this.instance.setTransform(-20,-20);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-20,40,40);


(lib.Tween11 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.crabs_new();
	this.instance.setTransform(-20,-20);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-20,-20,40,40);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Image237, null, new cjs.Matrix2D(1,0,0,1,-90,-40)).s().p("AuDGQIAAsfIcHAAIAAMfg");
	this.shape.setTransform(90,40);

	this.instance = new lib.Symbol20003();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.instance}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,180,80);


(lib.pearl = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 1
	this.instance = new lib.Bitmap20();
	this.instance.setTransform(-1,-1);

	this.instance_1 = new lib.heart2();
	this.instance_1.setTransform(-4,-4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,12,12);


(lib.lower_leg1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ACTC0IgFgEIgBAAQgMgIgCAAQipiAi4hoQgYgjgGgkIgBgLIACgKQACgKAOgMQAOgNAegDQAfgDAQARQARASAVAwIAAgBIABAEIAAACQAPA2A/AXQAkAOBPARQASAEAhAYQAfAXATAFQAVAFATACIABAAQAcAeACAPQAMBEhQAUQgggGgJgJgABbCJIACADIABABIgEgFIABABg");
	this.shape.setTransform(25.8,17.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F6666").s().p("AAjCAIgCgDIgBgBIAEAFgACoAyIgBAAQgTgDgVgFQgTgEgfgXQghgXgSgFQhNgSgmgOQg/gXgPg1IAAgDIgBgDIAAAAQAhAjA/AXQAlANBFATQAVAGAfATIA6AgQAKAHAKAPIAFAIIgBAAg");
	this.shape_1.setTransform(31.6,18.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-2.5,51.7,39.2);


(lib.head = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1D2725").s().p("AA/A/QgIgNgFgEIgGgGIAGACQADADAEAHIAJAMIAJAKQgGgEgGgHgAhKg/IABgBIALgJIgMAMIAAgCgAhKg/IABgBIgBABg");
	this.shape.setTransform(11.1,12);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#679495").s().p("AgBAVIgHgHQgFgGgCgGQgBgEAEgEIgFgDIAQgOIAGARQAEAIAFADIgFgCIAEAFIAAABQACABADgBIgFAGQgJAHgEACIgBgDg");
	this.shape_1.setTransform(6.5,5.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#205869").s().p("AADAxIAAgDQAEgWAUgYQAcgkAFgIIAKgDIANgDQADAdgZAmQgXAigXAAQgGAAgGgCgAhKAxQgGAAgBgKQgBgHABgGIAOgDIAFAGIgEAMQgDAIgFAAIAAAAg");
	this.shape_2.setTransform(20.8,25.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9B7E45").s().p("AhUA4IgFgFQgHgHgBgGQgBgFABgMIAGAAQAAgIAFgIQAGgJAVgMQAagPATgIQAKgFAFAGIAMAKIACABIAAABIABABIAFAFIAIAKIAMAKQAJAHgDACQgQAQgFAKQgDAMgDAGQgEAGgOAAQgXgCgNABQgOACgFADQgEADgDAAQgNgBgLgJgAA0gRIgEgDIAAAAIgJgKIgJgMQgEgIgDgCIgGgCIgBgBQAAAAAAgBQAAAAAAgBQABAAABgBQAAAAABgBIAMgFQAbgGAdAPIALAIQgCAHgJAGIgPAJIgNAJIgCABIgFgCg");
	this.shape_3.setTransform(13.8,21.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3F6666").s().p("ABHByQgLgHgGgCQACgJAJgJIAQgQQAGgFANgIQAQgKACAFQACACAEABIAGAWQgQAJgHAHQgDACgGAJQgGAIgDACQgDACgDAAQgFAAgHgDgAAaBSIgKgKIgCgBIABgBIgCgBIgFgEQgIgHgIgMIgSgTIgdgcQgQgSgGgPIgMALQgKAIgHABIgDADQgLALgEgMQgDAHgCgIQAAgBAcguIAAABIALgLQAMgMAQgJQAlgWAfgCQAhgCAlAeQAoAhgJAiIAAABIgGALQgHgDgNgMQgMgJgKgEQgcgKgcgEIgcgEIgVgBQgIADgFADIgDgFIAFACQgGgDgEgKIgIgRIgPAOIAFADQgEAEABAGQABAGAGAGIAHAHIADADQAEgCAIgHIAYAeQAQATANANIAVAWIABABIAHAFQAFAFAIANQAFAHAGAEIABAAIAEADQgEAFgDAIIgBAAQgHAAgLgLgAh3gkIAAAAIAEgHIAOgSIAAAAIgUAeIACgFg");
	this.shape_4.setTransform(13.8,11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAADCQgagCgcgOIgBAAIgBgBIgCgBIAAABQgDgBghgbIgEgEIABgCQg3gxABhHQgOgLgMgPQgXgiALgkQAKgkCGhUIgCADIgBABIgNASIgEAGIAAABIgDAEIAUgdIABgBIgBABQgcAvABABQABAIADgHQAEAMAMgMIACgCQAIgCAKgIIAMgLQAGAPAPASIAbAfIASATQALAMAIAGIAEAFIgMgLQgHgFgKAEQgTAIgYAPQgVANgGAKQgFAIAAAIIgGABQgBAJABAFQABAHAHAGIAFAFIgGgCQgyAZgmgMQAAArAsA0IABAAIASARQAlAXAlgHQALgCAFgJIAGgKIAAgDIAAgDIgcABIgCgOQgCgLAAgGQABgHADgEIAEgEQADgDABAAQABgHgDgOQgCgKgEgGQgCgDgEgDQgBgDgFgDIgBgEQADAAAEgDQADgEAOgCQANgBAZACQAOAAAEgGQADgFADgMQAFgIAQgQQADgDgJgJIgMgJIgIgKIgFgGIAKALQAMALAHAAQADgIAEgFQAEACADgBIANgJIAPgKQAJgFACgHIgLgJQgdgPgbAHIgMAFQgBAAgBABQAAAAgBABQAAAAAAAAQAAABAAAAIgYgVQgNgOgQgUIgVgfIAFgFQgDABgCgCIAAAAQAFgEAFgDIAWACIAeAEQAcADAcALQAKADALAKQAOALAGAEIAGgLIADAIQADAJADAWQACAVAEAKQAFADgBAGIAHAOQAGAJACAIIgLACQgEAJgdAiQgUAagDAWQgLABgMgEIgLgHQgFgDgFAAIgDgCIAAACQgLABgHAJQgJAPAHANQACAEAIAGQAHAFACAFQACAFAAAJQADAJgEADIgOAHIgBAFIABAAIgDAPQgBAKgGAGQgGAIgRALQgJAFgSAAIgGAAgAAoAkQgBAGABAHQACAJAFABQAFAAAEgJIAEgMIgGgGIgOAEgACZhFQgNAIgFAGIgQAPQgJAKgDAIQAGACAMAHQALAFAGgDQAEgCAGgIQAGgJADgDQAHgGAPgKIgGgVQgEgBgBgDQgBAAAAAAQAAgBAAAAQgBAAAAAAQgBAAAAAAQgFAAgLAGg");
	this.shape_5.setTransform(8.5,24.5);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.9,0,40.2,44);


(lib.flipper2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AihA2QgFgGABgPQACgRgCgGQgCgFgJgIIgCgDIgOgLIAHAAIgBgBQABAAAAAAQAAABABAAQAAAAABAAQAAAAABAAIAAAAQALAIAUAAIAmgBIBQAAQBdAAA4gPQAOgEAXgKQAXgMALgEIADAKQAFARgFASIgHAAQgDAFgJADQgEACgRACIgzANQgeAIgVAAQhWADgUACQg+AGgqAWIABgCg");
	this.shape.setTransform(19.3,6.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F6666").s().p("Ai5AVIgDgDQARgDAbABIAsACQA5AABhgLQARgCAUgFQAOgEAWgKQAXgKALgCQAXgGAFAOQABADgDgBIAAgCIgBACIgCABQgLAEgXALQgXAJgOAEQg4APhdgBIhQAAIgmACQgUgBgLgHg");
	this.shape_1.setTransform(19.6,2.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.6,12.5);


(lib.flipper1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("ACIBdIgXgDIgvgHQgQgCgYgJIglgQQg1gVgagMIgsgPIgZgIIgOgDQgHgCgIgIIgBABIABgBIgBAAIABAAQAJgGgDgcQgHgnABgFQAHgEAEAIIABADQAGALAMALQALAIATALQAPAJAaALIApAUQA9AeAOAGQAQAGAXADIAEAAIACABIAEABQALADAMAAIAKgBIANgCIAYgDQAOAAAJAEQANAEAEALQAFALgHAMQgWALgEgBQgEgBgDABIgFAAIgMAAg");
	this.shape.setTransform(19,9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F6666").s().p("ABxBDIgFgBIgBAAIgEgBQgXgDgQgGQgOgGg+ggIgogSQgagLgPgJQgUgLgKgIQgNgLgFgLIgBgCIgCgGIABADIACACQAEAFAHAFIAIAGIALAGIAjAVIAKAFIAQAHIAJAEIAjAMIAiARQATAKARAGIAZAJIAPAGIAUAHIANAGQABABAHAAIgJABIgDAAQgLAAgJgDg");
	this.shape_1.setTransform(15.6,7.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#1D2725").s().p("AgHgCIALAEIAEABIgPgFg");
	this.shape_2.setTransform(9.6,5.7);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.2,18.8);


(lib.diver_icon = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap23();
	this.instance.setTransform(-1,-1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,39,47);


(lib.btn_up = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.up();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.btn_right = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.right();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.btn_left = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.left();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.btn_down = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.down();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,35,39);


(lib.body = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1D2725").s().p("AhWBpIABAAIgCABIABgBgABYhpIgBAOQgBAOgEALQACgVAEgSg");
	this.shape.setTransform(23.1,18);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B7A22F").s().p("AlhCDQAKAIAMAHIgJghQgGgUgBgPQgBgmANgcICAgSQAXgDAugDQAGgBAXgHQASgGAKACIAEABIAAABQgBAZAEAvQACAYAGAaIgaAJIgBAAIg+AOIgFAAQgcAIgTACIgEABIgWACIgBAAIgLAAIgBAAIgFABIhZALIgNgRgAAUBZQgRgHgDgsQAAgrABgmQAGgKAFgTIAAACQgDAZgGAKQANgKAegFIAvgFQAPgCAegHQAegHAQgCQALAAAogJQAfgFATAAIAeACQATABAKgHQALgHAAgIIACAAIAIASIACAEIAAACIAAAMQACABgBAKQAAAKACAMQABALgBAKQgBAMgFAHIgBACQgCANAAAGIgKAOQgZAPgQAEIgGACIhCAJQghAAhYALQhRAKgOAAIgCAAgAlughIAKgQIDqgkIhNANQhcAOgkAIQgbAFgPARIADgFgAhLg5IADgXIABAFQgEASgBAVIgCAFIADgagAAWhrIEDgpIANAAQAPACAOAGQAOAHALANQgKgLgRgFQgKgDgNAAIgKgBQgSAAgbAFIgsAIQg0AGgmAIIgzAFQgcAFgMAJIAEgNgAFihuIgBgCIACACIgBAAg");
	this.shape_1.setTransform(39.1,14.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFEC36").s().p("AlTCBQgjgegCg9QgBgsAWgYQAPgRAbgFQAkgIBcgOIBNgNIAugJQADAFABAJIgDAXIgDAaIgDAHQgKgCgSAGQgXAHgGABQguADgXADIiAASQgNAcABAmQABAPAGAUIAJAhQgMgHgKgIgAAahIIAAgCIAAgBIAAgBIAAAAIAGgUQAMgJAcgFIAzgFQAmgIA0gGIAsgIQAbgFASAAIAKABQANAAAKADQARAFAKALIACACIADAGIABACIABAAIAIACIgGACIgCAAQAAAIgLAHQgKAHgTgBIgegCQgTAAgfAFQgoAJgLAAQgQACgeAHQgeAHgPACIgvAFQgeAFgNAKQAGgKADgZg");
	this.shape_2.setTransform(37.7,15.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#205869").s().p("AiUBuQgNgDgBgHQgCgOAVgBQALAAAQABIAOAJQgRAQgMABIgEABQgGAAgHgDgAj8gJIABhVIAigMQAFgDAMgDIgFAsQgCAaACATIAKAoQAGAbAAAOQgGADgQACQgPACgHAFQgRgjgCgsgADdAeQgUgRAHgRQAKgDAKAEIATAIIAGAYQgGAJgIAAQgHAAgLgIg");
	this.shape_3.setTransform(59.4,46.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("ADxEkQgDgIAJgMIAPgRIAbgmQAGgLAEgKIgLgMIgNgPQgaAQghAIQg6APhCgMIgGAJQgUAcgVAJQgQAHgkABQhQALgngDQg9gEgQgxIgjABQgWAAgDgEQgDgDgYgBQgYAAhMgZQhYgyB4iFQAUgFAagSQAhgYANgVIAEAAQATgCAcgJIAGAAIA9gOIABAAIAagJQgFgagCgXQgEgwABgbIAAgBQALACAagFQAggGAHgHIAEgEQgEAnADAsQADAsAQAHQAJABBWgLQBYgLAiABIAXAAIAqgJIAGgCQAQgFAZgPIALgHIAAgBIgBgFQAAgHADgNIAAgCQAFgJACgMQABgJgCgLQgCgNABgKQAAgKgCgBIABgLIABgCIAAAGQAGgHAHgEQgEAJADANQAZADAKADQAPAEARAIIAEgEQAAALgFANQgMAoACALQAMAAABALIAJAFIAAAHIgEAAIADAHIgGBFQgFAoAQAWQACALAHAIIAKAKIACAGIgDADIgKA5QgEAggJAUIAAAHIAAABIAFAIQAGAJACAHIACALIAAgGIAHAUQAFAVgZATQghAagIADQgVAHgUgYIgCgEIgDAFQgZAvAJALQgGAIgKAAQgJAAgIgGQgBAPgLABIgDAAQgJAAgEgLgAADC7QgTABACAOQAAAHANADQAJAEAHgCQALgBASgQIgOgJIgWgBIgFAAgAhFgEIgiAKIgBBVQACAuARAjQAGgFAPgCQARgCAGgDQAAgOgHgbIgJgqQgCgTACgaIAFgqQgMADgFADgAFjBgQgGATAUARQAVAQAKgRIgGgYIgSgKQgGgCgGAAQgFAAgEABgAlNgKIACgBIgCAAgAGmCkIABABIAAAAIgBgBg");
	this.shape_4.setTransform(44.6,36.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F6666").s().p("Al8B+QAWgPAUgJIANgJIABgBIACAAIALgBIABAAIAWgBQgNAVghAXQgbASgTAGgAljCdIACAAIgCAAgAF6AAIADAAIAAAHIgDgHgAiZhEIgFgBIAEgGIABgFQAEgLABgNIAAgPIgBgFQgBgJgDgFIgBgCIAzgMQAJABAQgDQARgEAKACIgCAFIgEAMIgFAVIAAAAIAAAAIgBABQgFAUgFAJIgEAEQgIAIgfAGQgTADgKAAIgIgBgAFThpQgLgCgYgEQgDgMAEgKIAEgHIAdgGQAFAFAbAPQATAPgOAPIgFADQgQgIgPgEg");
	this.shape_5.setTransform(46.8,19.3);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,87.5,66.7);


(lib.background = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bg3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,533,1902);


(lib.arm = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9B7E45").s().p("AhHBOIAAgGQgBgCgDgCIgBAAIAGgIIAAgBIAGgIIAEgEIACAIQABAFACADQAKgIAFgMQADgHAFgQQADgJANgLIAIgFIAKgKIAWgWQAPgNAMgFQgFgUgDgFIAEgEIABABIABATQACALAFAJIABADIALAOQAHAKACAIQgFACgHAGIgLAIIgWARIgYAVQgLANgNAEQgEgFAAgDQgMAEgNALIgOAHQgIAFgDAEQgCgDABgEg");
	this.shape.setTransform(21.2,35.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D2A05F").s().p("AhABGQgHgSAJgaQAEgMAIgKQAHgJALgIQAQgJAdgWIAYgNIAUgRIADgCIABABQADAFAFAUQgMAEgPAOIgWAWIgMAKIgIAFQgLALgDAIQgFARgDAHQgFAMgKAIQgCgDgBgFIgCgJIgEAFIgGAHIAAACIgGAHIgCAFIgEgHg");
	this.shape_1.setTransform(19.4,35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AkKCJIgLgOQAagSATgXQARgUAfgnQAfgnARgTIAOgQIAEgDIAJgJQAdgdATgPQAcgXAcgNIABgBIAHgDIACAAIAYAAQAXgBBNgGQBZgCA5APQAUBKhjgJQhXAAhqATQgMAQgYAQQgKAHgDAEIgbAXQhXBPhhBDQgCgIgHgKg");
	this.shape_2.setTransform(54.8,18.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F6666").s().p("AkQCZQgFgKgCgKIgBgTIAMgJIAVgWIArguQAGgNANgSQANgQAYgcQAaggAOgOQASgTASgMIALgIQAagOAhgPQApgDAhABIBPADIAXAAIAnABQAhABARALQARAMABARQg5gPhZADQhNAGgXAAIgYAAIgCAAIgJAEIAAABQgbAMgcAXQgTAPgdAdIgJAKIgEADIgOAQQgRAQgfApQgfAogRAUQgTAWgaASIgBgCg");
	this.shape_3.setTransform(54.2,15.6);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(12.6,0,70.2,44);


(lib.octopus_new_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween15("synched",0);
	this.instance.setTransform(20,20);

	this.instance_1 = new lib.Tween16("synched",0);
	this.instance_1.setTransform(20,20,1,1.395);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,scaleY:1.4},9).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},9).to({scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,40,40);


(lib.jellyfish_new_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween13("synched",0);
	this.instance.setTransform(20,25);

	this.instance_1 = new lib.Tween14("synched",0);
	this.instance_1.setTransform(20,25,1,1.49);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,scaleY:1.49},9).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},9).to({scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,40,50);


(lib.diver = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{swim:0,hitwall:15,die:30,lostlife:80});

	// timeline functions:
	this.frame_14 = function() {
		this.gotoAndPlay(0);
	}
	this.frame_15 = function() {
		playSound("hit");
	}
	this.frame_29 = function() {
		this.gotoAndPlay(0);
	}
	this.frame_30 = function() {
		playSound("endgame");
	}
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_80 = function() {
		playSound("lostlife");
	}
	this.frame_106 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1).call(this.frame_15).wait(14).call(this.frame_29).wait(1).call(this.frame_30).wait(49).call(this.frame_79).wait(1).call(this.frame_80).wait(26).call(this.frame_106).wait(1));

	// arm
	this.instance = new lib.arm("synched",0);
	this.instance.setTransform(40.2,-6,0.542,0.542,-17,0,0,79,5.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:5.7,rotation:-3.5,x:38.8,y:-3.3},6).to({scaleX:0.54,scaleY:0.54,rotation:-15.5,x:40.1,y:-5.8},8).wait(1).to({regX:79.1,regY:5.6,rotation:-45.2,x:40},0).to({regX:79.2,regY:5.5,scaleX:0.54,scaleY:0.54,rotation:-150.3,x:41.3,y:-12.3},2).to({scaleX:0.54,scaleY:0.54,rotation:-156.1,x:42,y:-15.4},1).to({regX:79.3,scaleX:0.54,scaleY:0.54,rotation:-156.9,x:41.6,y:-13.6},1).to({regX:79.4,rotation:-150.8,x:43,y:-9.6},1).to({regY:5.4,rotation:-18.4,x:40.9,y:-7.1},2).to({regX:79,regY:5.7,scaleX:0.54,scaleY:0.54,rotation:-15.5,x:40.1,y:-5.8},1).wait(7).to({startPosition:0},0).to({regX:78.9,regY:5.6,rotation:-51.2,x:40,y:-3.3},10).to({regX:79,regY:5.5,rotation:-79.9,y:-5.9},5).to({regX:78.9,rotation:-74.2,y:-3.6},17).to({regX:79,rotation:-79.9,y:-0.6},13).wait(5).to({regY:5.7,rotation:-15.5,x:40.1,y:-5.8},0).to({regX:79.1,regY:5.6,scaleX:0.54,scaleY:0.54,rotation:-133.4,x:35.6,y:-9.8},2).to({regX:79,regY:5.7,scaleX:0.54,scaleY:0.54,rotation:-224.8,x:28.7,y:-15.8},3).to({rotation:-290,x:25.2,y:-16.9},5).to({regX:78.9,rotation:-173.2,x:24.7,y:-11.7},6).to({regX:79,rotation:-15.5,x:40.1,y:-5.8},10).wait(1));

	// flipper1
	this.instance_1 = new lib.flipper1("synched",0);
	this.instance_1.setTransform(-38.8,-1.1,1.024,1.024,-64.7,0,0,33.4,15.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:-54.4,x:-39.5,y:-5.9},6).to({rotation:-44.7,x:-42,y:-2.7},3).to({rotation:-49.5,x:-39.9,y:-0.6},5).wait(1).to({startPosition:0},0).to({rotation:-41.5,x:-38.1,y:0.8},3).to({rotation:-49.5,x:-39.9,y:-0.6},5).wait(7).to({startPosition:0},0).to({rotation:-28.7,x:-32.4,y:-12.5},10).to({rotation:-32.4,x:-32.7,y:-11.1},5).to({regX:33.3,regY:15.4,rotation:-23.5,y:-11.8},17).to({regX:33.4,regY:15.3,rotation:-32.4,y:-11.1},13).wait(5).to({rotation:-49.5,x:-39.9,y:-0.6},0).to({regX:33.5,regY:15.4,rotation:-16.5,x:-38.5,y:-16.2},5).to({regY:15.3,rotation:-48.7,x:-43.6,y:1.8},11).to({regX:33.4,rotation:-49.5,x:-39.9,y:-0.6},10).wait(1));

	// lowerleg1
	this.instance_2 = new lib.lower_leg1("synched",0);
	this.instance_2.setTransform(-12.4,2.7,0.542,0.542,-23.5,0,0,44.5,28.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:44.6,regY:29,rotation:-13,x:-14.4},6).to({regX:44.5,regY:29.1,rotation:-18.7,y:2.8},3).to({scaleX:0.54,scaleY:0.54,rotation:-22.6,x:-12.7},5).wait(1).to({startPosition:0},0).to({regX:44.6,regY:29.2,rotation:-9.6,x:-10.5,y:7.6},3).to({regX:44.5,regY:29.1,rotation:-22.6,x:-12.7,y:2.8},5).wait(7).to({regX:44.4,regY:29.2,rotation:-28.5,x:-12.8},0).to({rotation:5.2,x:-10.5,y:4.4},10).to({rotation:0.5},5).to({regY:29.1,rotation:7.5},17).to({regY:29.2,rotation:5.2},13).wait(5).to({regX:44.5,regY:29.1,rotation:-22.6,x:-12.7,y:2.8},0).to({regX:44.6,regY:29.2,rotation:19.7,x:-21.8,y:4.3},5).to({rotation:-18.5,x:-16.1,y:9.4},11).to({regX:44.5,regY:29.1,rotation:-22.6,x:-12.7,y:2.8},10).wait(1));

	// upperleg1
	this.instance_3 = new lib.upper_leg("synched",0);
	this.instance_3.setTransform(9.3,0.6,0.542,0.542,0,0,0,54.5,9.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:2.5,x:7.5},6).to({rotation:0.3,x:9,y:0.7},8).wait(1).to({rotation:0.3},0).to({regX:54.4,regY:9.6,rotation:-16.4,x:10,y:-0.9},3).to({regX:54.5,regY:9.7,rotation:0.3,x:9,y:0.7},5).wait(7).to({rotation:0.3},0).to({rotation:-1.7,y:0.6},10).to({regY:9.5,rotation:-6,x:10,y:-0.6},5).to({regY:9.7,rotation:-1.7,x:9,y:0.6},17).to({regY:9.5,rotation:-6,x:10,y:-0.6},13).wait(5).to({regY:9.7,rotation:0.3,x:9,y:0.7},0).to({rotation:-14.2,x:2.5,y:-0.5},5).to({regY:9.6,scaleX:0.54,scaleY:0.54,rotation:-15.5,x:2.6,y:-0.6},2).to({regX:54.6,regY:9.7,scaleX:0.54,scaleY:0.54,rotation:-21.9,x:2.5},9).to({regX:54.5,rotation:0.3,x:9,y:0.7},10).wait(1));

	// body
	this.instance_4 = new lib.body("synched",0);
	this.instance_4.setTransform(26.9,-4.1,0.542,0.542,0,0,0,49.6,33.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regY:33.5,rotation:4.2,y:-4},6).to({rotation:0.3,x:26.8,y:-4.1},8).wait(1).to({rotation:0.3},0).to({rotation:-11.4,x:26.9,y:-8.8},2).wait(1).to({startPosition:0},0).to({rotation:0.3,x:26.8,y:-4.1},5).wait(7).to({rotation:0.3},0).to({regY:33.4,rotation:4.5,x:26.9,y:-2.7},10).to({regY:33.5,rotation:0.3,x:26.8,y:-4.1},5).to({regY:33.4,rotation:4.5,x:26.9,y:-2.7},17).to({regX:49.5,regY:33.5,rotation:8.8,x:27.6,y:-0.8},13).wait(5).to({regX:49.6,rotation:0.3,x:26.8,y:-4.1},0).to({regY:33.4,rotation:-23,x:17.3,y:-11.8},10).to({regY:33.5,rotation:0.3,x:26.8,y:-4.1},16).wait(1));

	// head
	this.instance_5 = new lib.head("synched",0);
	this.instance_5.setTransform(44.5,-1.6,0.542,0.542,0,0,0,0,19.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({rotation:-5.2},6).to({rotation:0},8).wait(1).to({rotation:-39.2,x:48.5,y:-6.9},0).to({regY:19.7,rotation:-50,x:45.3,y:-19.9},2).to({regY:19.5,scaleX:0.54,scaleY:0.54,rotation:-57.6,x:45.9,y:-18.9},1).to({regY:19.6,scaleX:0.54,scaleY:0.54,rotation:0,x:44.5,y:-1.6},5).wait(7).to({startPosition:0},0).to({regX:0.1,rotation:-12.7,x:44.6},2).to({regY:19.4,rotation:53.5,x:47.5,y:-0.6},8).to({regX:-0.1,regY:19.5,rotation:72,x:48.2,y:1.5},5).to({regX:0,rotation:77.2,x:48,y:0.3},17).to({regX:-0.1,rotation:89.9,x:48.2,y:0.8},13).wait(5).to({regX:0,regY:19.6,rotation:0,x:44.5,y:-1.6},0).to({rotation:-35.7,x:34.5,y:-16.1},10).to({scaleX:0.54,scaleY:0.54,rotation:-31.5,x:38.8,y:-12.7},6).to({scaleX:0.54,scaleY:0.54,rotation:0,x:44.5,y:-1.6},10).wait(1));

	// flipper2
	this.instance_6 = new lib.flipper2("synched",0);
	this.instance_6.setTransform(-32.8,-4.1,1.024,1.024,-36.7,0,0,37.6,3.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({rotation:-49.9,x:-34.8,y:-2.2},6).to({rotation:-60.4},3).to({rotation:-63.4,x:-35.9,y:-7.6},5).wait(1).to({startPosition:0},0).to({rotation:-52.6,x:-26.4,y:-8},3).to({rotation:-63.4,x:-35.9,y:-7.6},5).wait(7).to({startPosition:0},0).to({rotation:-46.9,x:-25,y:2.5},10).to({rotation:-46.1,x:-25.7,y:2.1},5).to({rotation:-47.9,x:-25.5,y:0.3},17).to({rotation:-46.1,x:-25.7,y:2.1},13).wait(5).to({rotation:-63.4,x:-35.9,y:-7.6},0).to({scaleX:1.02,scaleY:1.02,rotation:-37.6,x:-30.9,y:1.3},7).to({scaleX:1.02,scaleY:1.02,rotation:-23.2,x:-30.1,y:-10.8},9).to({rotation:-63.4,x:-35.9,y:-7.6},10).wait(1));

	// lowerleg2
	this.instance_7 = new lib.lower_leg1("synched",0);
	this.instance_7.setTransform(-10.5,0.4,0.542,0.542,-22.2,0,0,44.5,29.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({rotation:-28.5,y:1.8},6).to({regY:29.2,rotation:-32.4,x:-10.4},3).to({regY:29.1,rotation:-17.5,x:-12.6},5).wait(1).to({startPosition:0},0).to({regX:44.6,rotation:-17,x:-4.8,y:0.5},3).to({regX:44.5,rotation:-17.5,x:-12.6,y:1.8},5).wait(7).to({startPosition:0},0).to({x:-1.9,y:11.2},10).to({x:-2.6},5).to({regX:44.4,rotation:-13.3,x:-2.7,y:11.3},17).to({regX:44.5,rotation:-17.5,x:-2.6,y:11.2},13).wait(5).to({x:-12.6,y:1.8},0).to({regX:44.6,rotation:-2.5,x:-10.4,y:13.8},7).to({regX:44.7,rotation:7.2,x:-12.2,y:7.1},9).to({regX:44.5,rotation:-17.5,x:-12.6,y:1.8},10).wait(1));

	// upperleg2
	this.instance_8 = new lib.upper_leg("synched",0);
	this.instance_8.setTransform(-3.4,-0.1,0.542,0.542,0,0,0,31,12.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({regX:31.1,rotation:-7.5,y:1.1},6).to({regX:31.2,rotation:-0.8,y:0.1},8).wait(1).to({rotation:-0.8},0).to({regX:31.1,regY:12.6,rotation:-12.7,x:4.4,y:-1.2},3).to({regX:31.2,regY:12.7,rotation:-0.8,x:-3.4,y:0.1},5).wait(7).to({rotation:-0.8},0).to({regX:31.1,regY:12.8,rotation:-31,x:4.8,y:6.1},10).to({regX:31.2,regY:12.7,rotation:-39.7,x:3.1,y:5.3},5).to({regX:31.1,regY:12.8,rotation:-31,x:4.8,y:6.1},17).to({regX:31.2,regY:12.7,rotation:-39.7,x:3.1,y:5.3},13).wait(5).to({rotation:-0.8,x:-3.4,y:0.1},0).to({regX:31.3,rotation:-41.3,y:7.5},7).to({regX:31.4,rotation:-15.8,y:3.8},9).to({regX:31.2,rotation:-0.8,y:0.1},10).wait(1));

	// arm2
	this.instance_9 = new lib.arm("synched",0);
	this.instance_9.setTransform(38.7,-7.5,0.542,0.542,-6.3,0,0,78.9,5.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({regX:79,regY:5.8,rotation:-10.7,x:41.2,y:-7.8},6).to({regX:79.1,scaleX:0.54,scaleY:0.54,rotation:-6.6,x:39,y:-7.6},8).wait(1).to({regX:79,rotation:-28},0).to({regY:5.9,rotation:-108.2,x:42.3,y:-13.6},1).to({scaleX:0.54,scaleY:0.54,rotation:-149.8,x:46.7,y:-14.6},1).to({scaleX:0.54,scaleY:0.54,rotation:-159.7,x:51,y:-15.8},1).to({rotation:-147.5,x:46.2,y:-12.5},2).to({regX:78.9,rotation:-7.4,x:41.4,y:-9.2},2).to({regX:79.1,regY:5.8,rotation:-6.6,x:39,y:-7.6},1).wait(7).to({startPosition:0},0).to({regX:79.2,scaleX:0.35,rotation:6.6,x:34.8,y:-3},10).to({regX:79.4,regY:5.7,scaleX:0.32,scaleY:0.26,x:34.9},5).to({_off:true},1).wait(34).to({_off:false,regX:79.1,regY:5.8,scaleX:0.54,scaleY:0.54,rotation:-6.6,x:39,y:-7.6},0).to({regX:79.3,scaleX:0.47,scaleY:0.47,rotation:-138.3,x:31.5,y:-11.1},7).to({regX:79,regY:5.7,scaleX:0.44,scaleY:0.45,rotation:-157,x:29.5,y:-12.3},3).to({regX:79.2,rotation:-68.3,x:30,y:-14.8},8).to({regX:79.1,regY:5.8,scaleX:0.54,scaleY:0.54,rotation:-6.6,x:39,y:-7.6},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.4,-22.2,127.7,48.8);


(lib.crabs_new_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(20,20);

	this.instance_1 = new lib.Tween12("synched",0);
	this.instance_1.setTransform(20,20,1.326,1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,scaleX:1.33},9).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},9).to({scaleX:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,40,40);


(lib.control = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.circle();
	this.instance.setTransform(54.5,24);

	this.instance_1 = new lib.btn_up();
	this.instance_1.setTransform(74,-22.5);
	new cjs.ButtonHelper(this.instance_1, 0, 1, 1);

	this.instance_2 = new lib.btn_right();
	this.instance_2.setTransform(137.5,43);
	new cjs.ButtonHelper(this.instance_2, 0, 1, 1);

	this.instance_3 = new lib.btn_left();
	this.instance_3.setTransform(7,43);
	new cjs.ButtonHelper(this.instance_3, 0, 1, 1);

	this.instance_4 = new lib.btn_down();
	this.instance_4.setTransform(74,108);
	new cjs.ButtonHelper(this.instance_4, 0, 1, 1);

	this.addChild(this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(7,-22.5,169.5,169.5);


(lib.cells = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.diverIcon_mc = new lib.diver_icon();
	this.diverIcon_mc.setTransform(28,17);
	this.diverIcon_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.diverIcon_mc).wait(17));

	// Layer 3
	this.pearl = new lib.pearl();
	this.pearl.setTransform(39.3,36.3);

	this.timeline.addTween(cjs.Tween.get(this.pearl).wait(17));

	// Layer 1
	this.instance = new lib.transparent_cell();
	this.instance.setTransform(-1,0);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(27,16,39,47);


// stage content:



(lib.pearldiver01_v43 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		initCaves();//function in game.js
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(35));

	// fish
	this.instance = new lib.fish();
	this.instance.setTransform(1016,591);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(35));

	// Layer 2
	this.instance_1 = new lib.txt_score();
	this.instance_1.setTransform(1032,521.1,1,1,0,0,0,-200.5,-90);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},10).wait(25));

	// Layer 3
	this.instance_2 = new lib.control();
	this.instance_2.setTransform(720,867.1,1,1,0,0,0,91.8,61.3);

	this.instance_3 = new lib.background();
	this.instance_3.setTransform(276.5,1746,1,1,0,0,0,276.5,951);

	this.instance_4 = new lib.txt_numlife();
	this.instance_4.setTransform(1140.9,527.6,1,1,0,0,0,-223,-69.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.instance_3},{t:this.instance_2}]}).to({state:[]},11).to({state:[]},23).wait(1));

	// MC assets
	this.instance_5 = new lib.Symbol2();
	this.instance_5.setTransform(1025.2,779.9,1,1,0,0,0,90,40);

	this.instance_6 = new lib.octopus_new_1();
	this.instance_6.setTransform(1022,322.6,0.659,0.659);

	this.instance_7 = new lib.jellyfish_new_1();
	this.instance_7.setTransform(1018,270.5,0.585,0.585);

	this.instance_8 = new lib.crabs_new_1();
	this.instance_8.setTransform(1017.3,226,0.603,0.603);

	this.diver1 = new lib.diver();
	this.diver1.setTransform(1047.9,127.6,1,1,0,0,0,3,-2);

	this.instance_9 = new lib.cells();
	this.instance_9.setTransform(1024,34.7,1,1,0,0,0,35,34.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_9},{t:this.diver1},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5}]}).to({state:[]},2).wait(33));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(412.5,292.5,1405,2681);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;