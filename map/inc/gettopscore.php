<?
	require_once('connect.php');
	$gameId = $_REQUEST['gameId'];
	
	if (empty($gameId)){
		echo '{"success":false, "error":"No gameId"}';
	}else if ($gameId<1 || $gameId>3){
		echo '{"success":false, "error":"gameId out of range"}';
	}else{
		$sql = "SELECT score FROM user_score WHERE gameId=$gameId ORDER BY score DESC LIMIT 1";
		$result = mysql_query($sql);
		$scores = array();
		header('Content-Type: application/json');
		header("Access-Control-Allow-Origin: *");
		if ($result){
			 $row = mysql_fetch_row($result);
			echo '{"success":true, "score":'.$row[0].'}';
		}else{
			echo '{"success":true, "error":"'.$sql.'"}';
		}
	}
	mysql_close($conn);
?>