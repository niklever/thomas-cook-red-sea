﻿The games need to share the same cookie for this data. It is needed for posting a score. The launch page needs to set a flag that the game uses highscores or not depending on the current date and start a session, see index.php. Because only one of the games is the prize draw game, after a new game is launched the game stops being the prize draw game. It will show different messages. There is also a share mechanic that adds to the users score. To handle this call 

var game_score; //If this holds the game score

//Call this when a user shares the game
function share(){
	var delta = 50;
	game_score += delta;
	call inc.php?score=delta;
 	call sendscore.php?score=game_score;
	//see the flow diagram to see how screens need to be updated after this
}







register.php params: email, name, dob, option, postcode
returns 
{"success":true, "error_code":200, "msg":"User registered."}
{"success":false, "error_code":201, "error":"User with this email already exists"}
{"success":false, "error_code":202, "error":"'.$sql.'"}
{"success":false, "error_code":203, "error":"Enter full information for fields"}



















inc.php params: score - call this every time the score changes in the game
returns 
{"success":false, "error":"No session score found", "stored time":"'.$stored_time.’"}
{"success":true, "inc":'.$inc.', "score":'.$score.', "cheating":'.$cheating.’}
{"success":false, "error":"No inc found”}

sendscore.php params: email, score, gameId ( 1=diver, 2=driving, 3=pairs )
returns 
{"success":false, "error":"No session”}
{"success":false, "error":"User cheating”}
{"success":false, "error":"Session score different from sent score."}
{"success":true, "topten":'.$topten.', "saved_score_higher":true }
{"success":true, "topten":'.$topten.', "saved_score_higher":false}
{"success":false, "error":"'.$sql.'"}

gettopscore.php params:gameId
returns 
{"success":false, "error":"No gameId"}
{"success":false, "error":"gameId out of range"}
{"success":true, "error":"'.$sql.'"}
{"success":true, "score":100}

highscores.php params:gameId
returns 
{"success":false, "error":"No gameId"}
{"success":false, "error":"gameId out of range"}
{"success":true, "error":"'.$sql.'"}
{"success":true, "scores":[{"name":"Nik", "score":100}]}