 var base_API = "http://catalystpics.co.uk/clients/foundry/thomascook/v3/ws/";

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=171055569768916&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

function validate_number(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
function validate_date(evt)
{
	var $= jQuery;
	var flag = true;
	if(!/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/.test(evt)){
           flag = false;
  	}
	return flag;
}
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

var number_show_game = 0;
function register()
{
	var $=jQuery;
	var _first_name = $("#first-name").val();
	var _last_name = $("#last-name").val();
	var _birthday = $("#birthday").val();
	var _email    = $("#email-regis").val();

	var isError = true;


	if(!validate_date(_birthday))
	{
		var html ='<i class="fa fa-exclamation-triangle"></i><span class="errow-regis">invalid date of birth</span>'; 
		$(".div-errow-regis").html(html).fadeIn();	
	}
	else
	{
		if($('#form-register-user').find('input:checked').length == 0){
			var html ='<i class="fa fa-exclamation-triangle"></i><span class="errow-regis">Please choose your gender</span>'; 
			$(".div-errow-regis").html(html).fadeIn();	
		}else{
			if(validateEmail(_email)){
				_birthday = _birthday;
				$(".div-errow-regis").html('').fadeOut();
				isError = false;
			}else{
				var html ='<i class="fa fa-exclamation-triangle"></i><span class="errow-regis">invalid email</span>'; 
				$(".div-errow-regis").html(html).fadeIn();	
			}
		}
	}

	if(!isError){
		var _sex_regis = $(".checkbox_sex:checked").val();
		
		var _full_name =  _first_name+" "+_last_name;
		var data_ajax={
			  full_name     :  _full_name,
			  sex	  		:  _sex_regis,
			  birthday 		:  _birthday,
			  email 		:  _email,
			  gameId		: gameId
		};
		
		$.post(base_API+"/register.php",data_ajax,function(rdata){	
		  $(".errow-regis").html('');
		  var result = JSON.parse( JSON.stringify(rdata));
		  if(result.success== false)
		  {
		     var html ='<i class="fa fa-exclamation-triangle"></i><span class="errow-regis">'+rdata.error+'</span>'; 
		     $(".div-errow-regis").html(html).fadeIn();	
		  }
		  else
		  {
			 var html ='<i class="fa fa-check-square-o color-sucess"></i><span class="errow-regis color-sucess">'+rdata.msg+'</span>'; 
			 $("#birthday").removeClass('border-errow');
			 $(".div-errow-regis").html(html).fadeIn();	
			 change_page("game_play");
			 
			 $('#the-game').removeClass('hide');
       		// $('#the-game').css('visibility', 'visible');
			 $(".bg_enjoy_game").append("<a class='button-play-game' href='javascript:;' ><img src='images/ok_play.png' /></a>");
			 set_cookie("email", _email);
			 set_cookie("fullname", _full_name);
			 
			 document.getElementById('game-play').focus();

			 number_show_game++;
		  }
		},'json');
	}

	return false;
}
 function change_page(page_id){

        
        if(page_id=="game_play"){
        	// if(!$.cookie("fullname")){
        	if(gameId == 3){
	        	if(!localStorage.getItem("fullname")){
			  		change_page("register");
			  	}else{
		  			$.post(base_API+"init-game.php", {gameId : gameId} ,function(rdata){	
		  				$("#option-left").hide();
			        	$("#option-right").hide();
			        	/*if(number_show_game > 0){
			        		$('#the-game').css({'display': 'block', 'visibility': 'visible'});
			        	}else{
			        		console.log('change_page number_show_gamedkfjdfjk', number_show_game);
			        		$('#the-game').css('visibility', 'visible');
			        	}*/
			        	$('#the-game').removeClass('hide');
			        	number_show_game++;
			        	//fadeIn();
			      		$("#main-background-social-1").addClass("bg-play");
			      		document.getElementById('game-play').focus();
				     }).done(function() {
			        	
			      	})
			  	}
			}else{
				$("#option-left").hide();
	        	$("#option-right").hide();
	        	$('#the-game').removeClass('hide');
	        	number_show_game++;

	      		$("#main-background-social-1").addClass("bg-play");
	      		document.getElementById('game-play').focus();
			}
       
        }else{
        	/*console.log('change_page number_show_game', number_show_game);
        	if(number_show_game >= 1){
        		$('#the-game').css({'display': 'none','visibility': 'hidden'});
        	}else{
        		$('#the-game').css('visibility', 'hidden');
        	}*/

        	$('#the-game').addClass('hide');
        	
        	$("#option-left").show();
        	$("#option-right").show();
        	$("#main-background-social-1").removeClass("bg-play");
        	$(".class_page").hide(); 
          // $('#the-game').css('visibility', 'hidden');//fadeIn();
	        $("#"+page_id).fadeIn();
	        if(page_id=="top_ten"){
	            highscores();
	        }
        }
    }

    function highscores(){
        var html = "";
        var data_ajax= {
            'gameId' : gameId
        };
        $.post(base_API+"highscores.php",data_ajax,function(rdata){    
          if(rdata.success=="true")
          {
        	$("#divTopGame").html('');
             var i = 1;
             $.each(rdata.data, function( index, value ) {
               html  ='<tr><td class="stt">'+i+'</td><td class="name">'+value.name+'</td><td class="score">'+value.score+'</td></tr>';
               i++;
               $("#divTopGame").append(html);
            });
          }
          else
          {
              alert("Not found data");
          }
        },'json');
    }
    function set_cookie(key,value){
    	/*var options ={
		  	path: "/",
		  	expires:365
		  }
    	 $.cookie(key, value, options);*/
    	 localStorage.setItem(key, value);
    }
jQuery(function($){

  if(gameId == 3){
	$('#birthday').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'dd/mm/yy',
      yearRange: '1910:2010'
    });

	$(".fnsocial").click(function(){
		var datasocial = $(this).attr("data-social");
		var href = $(this).attr("data-href");
		var tile = '';
		if(number_show_game>0){
			tile ="I've just played #RedSeaStories. Play and share now or your chance to win a holiday and £500 @ThomasCookUK vouchers";
		}else{
			tile ="Play #RedSeaStories now for your chance to win a holiday and £500 @ThomasCookUK vouchers";
		}
		// if(typeof $.cookie("scores") == 'undefined'){
		if(!localStorage.getItem("scores")){
			set_cookie("scores",100);
			if(localStorage.getItem("email")){
				email = localStorage.getItem("email");

				$.ajax({
					url: base_API+'/socialscore_after.php',
					type: 'Post',
					dataType: 'json',
					data: {email: email, gameId:gameId},
				})
				.done(function() {
					console.log("send ajax share successfully!");
				});
				
			}
		}
		if(datasocial=="google"){
			url = "http://bit.ly/1wCif0c";
			window.open(href+"&url="+encodeURIComponent(url)+"&text="+encodeURIComponent(tile),'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
			return;
		}else if(datasocial=="twitter"){
			url = "http://bit.ly/1slLGgc";
			window.open(href+"&url="+encodeURIComponent(url)+"&text="+encodeURIComponent(tile),'_blank');
			return;
		}else if(datasocial=="facebook"){
			url = "http://bit.ly/1r7Y3Ai";
			window.open(href+"&link="+encodeURIComponent(url)+"&caption="+encodeURIComponent(tile),'facebook-share-dialog','width=626,height=436');
			return;
		}
	})
  
  $(".bg_enjoy_game").click(function(){
  	// if(!$.cookie("fullname")){
  	if(!localStorage.getItem("fullname")){
  		// console.log( window.location.href.substr(0, window.location.href.lastIndexOf('/') ) );
  		//window.location = window.location.href.substr(0, window.location.href.lastIndexOf('/') ) + '/register.php';
  		// window.location = 'http://localhost/Catalyst/THOMASCOOK/web_1.1/register.php';
  		change_page("register");
  	}else{
      	change_page("game_play");
  	}
  });

  	console.log(localStorage.getItem("fullname"), 'localStorage.getItem("fullname")');
  	if(localStorage.getItem("fullname")){
  // if( $.cookie("fullname")){
		 $(".bg_enjoy_game").append("<a class='button-play-game' href='javascript:;'  data-page-id='register'><img src='images/ok_play.png' /></a>");
	}
	$(".onlick_page").click(function(){
	    var page_id = $(this).attr("data-page-id");
	    change_page(page_id);
	});
  }else{
  	$(".bg_enjoy_game").click(function(){
	    change_page("game_play");
	 });
  }

  

  // $('#the-game').css('height', $(window).height()+'px');

   
})