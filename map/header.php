<?php
/// 1 - driving game, 2 - scuba game, 3 - 
	if( !isset($_REQUEST['gameId']) || empty($_REQUEST['gameId']) ){
		echo 'No game id found';exit;
	}

	$gameId = $_REQUEST['gameId'];

	if($gameId < 1 && $gameId > 3){
		echo 'No game id found';exit;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="Dive deep and collect the pearls, avoiding sea creatures along the way, for your chance to WIN a 1-week holiday to the Red Sea – plus other fantastic monthly prizes." />
<meta itemprop="description" name="og:description" content="Dive deep and collect the pearls, avoiding sea creatures along the way, for your chance to WIN a 1-week holiday to the Red Sea – plus other fantastic monthly prizes." />
<meta itemprop="image" name="og:image" content="http://catalystpics.co.uk/clients/foundry/thomascook/web_1.4/images/social-image.jpg" />
<meta name="og:site_name" content="Thomas Cook" />
<meta itemprop="name" name="og:title" content="Play and win a Red Sea holiday with Thomas Cook" />
<meta name="og:url" content="http://www.thomascook.com/holidays/holidays-egypt/red-sea-games/" />
<meta name="canonical" content="http://www.thomascook.com/holidays/holidays-egypt/red-sea-games/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@ThomasCookUK" />
<meta name="twitter:creator" content="@ThomasCookUK" />
<meta name="twitter:title" content="Play and win a Red Sea holiday with Thomas Cook" />
<meta name="twitter:description" content="Dive deep and collect the pearls, avoiding sea creatures along the way, for your chance to WIN a 1-week holiday to the Red Sea – plus other fantastic monthly prizes." />
<meta name="twitter:image" content="http://catalystpics.co.uk/clients/foundry/thomascook/web_1.4/images/social-image.jpg" />

<title>Thomas Cook - Games</title>
<script type="text/javascript">
	<?php
		echo "var gameId = $gameId;";
	?>

	var prize_draw = true;
</script>

<script src="js/jquery-1-11-0.js"></script>
<!-- <script src="js/jquery.easing.1.3.min.js"></script> -->
<link rel="stylesheet" href="js/jquery-ui-1.11.1/jquery-ui.min.css">
<link rel="stylesheet" href="js/jquery-ui-1.11.1/jquery-ui.theme.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- <link href="css/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet"> -->
<?php if($gameId == 1): ?>
	<link rel="stylesheet" href="css/style-driving.css">
<?php endif; ?>
<?php if($gameId == 3): ?>
	<link rel="stylesheet" href="css/style-culture.css">
<?php endif; ?>
<script src="js/jquery-ui-1.11.1/jquery-ui.min.js"></script>
<script src="js/jquery-functions.js"></script>

<script type="text/javascript">
	if(!prize_draw){
		document.write('<style>.text-share, .social-icon-share{display:none}</style>');
	}
</script>

</head>
<body>
    <div id="main-container">
    	
        <div id="main-background-social-1" class="container" style="width:960px;">
        	<div id="the-game" class="hide">
        		<?php if($gameId == 1){ ?>
        		<iframe id="game-play" width='100%' height='100%' scrolling="no" frameborder="0" src='../driving/'></iframe>
		    	<?php }else if($gameId == 2){ ?>
		    	<iframe id="game-play" width='100%' height='100%' scrolling="no" frameborder="0" src='../scuba/'></iframe>
		    	<?php } else if($gameId == 3){ ?>
		    	<iframe id="game-play" width='100%' height='100%' scrolling="no" frameborder="0" src='../pairs/'></iframe>
		    	<?php } ?>
		    </div>
            <div id="main-sub-background-log-1" class="div-set-paddingfor">

            	<div class="fl" id="option-left">
                    
                    