<?php 
	include 'header.php';

	switch ($gameId) {
		case 1:
			echo "<div class='container-driving-game'>";
			include 'driving-game.html';
			echo "</div>";
			break;

		case 2:
			echo "<div class='container-scuba-game'>";
			include 'scuba-game.html';
			echo "</div>";
			break;

		case 3:
			echo "<div class='container-culture-game'>";
			include 'culture-game.html';
			echo "</div>";
			break;
		
		default:
			# code...
			break;
	}
	
 
	include 'footer.php';
 ?>