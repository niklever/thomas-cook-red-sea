<?
	session_start();

	//include "fnc_log.php";
	
	if(!isset($_REQUEST['gameId'])){
		echo '{"success":false, "error":"No game id found"}';
	}

	$gameId = $_REQUEST['gameId'];

	if($gameId < 1 && $gameId > 3){
		echo '{"success":false, "error":"No game id found"}';
	}

	$gameData = $_SESSION['game_'.$gameId];

	// $gameData['time'] = microtime(true) * 1000;
	$stored_time = $gameData['time'];
	//echo '{"success":false, "error":"test"}';
	
	if (empty($stored_time)){
		echo '{"success":false, "error":"No session score found", "stored time":"'.$stored_time.'"}';
	}else{
		$inc = $_REQUEST['score'];
		if (isset($inc)){
			$time = microtime(true) * 1000;
			if (($time - $gameData['time'])<100 || $inc>1000) $gameData['cheating'] = true;
			if( !isset($gameData['score']) ){
				$gameData['score'] = 0;
			}
			$score = $gameData['score'] + $inc;
			$gameData['score'] = $score;
			$cheating = ($gameData['cheating']) ? 'true' : 'false';
			$_SESSION['game_'.$gameId] = $gameData;
			echo '{"success":true, "inc":'.$inc.', "score":'.$score.', "cheating":'.$cheating.'}';
		}else{
			echo '{"success":false, "error":"No inc found"}';
		}
	}
?>