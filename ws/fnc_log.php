<?php 
if(!function_exists('writeLogAndCreateFile')){
    function writeLogAndCreateFile($log){

        date_default_timezone_set('UTC');

        $file = dirname(__FILE__).'/logs/log-'.date('Y-m-d').'.txt';

        if(file_exists($file) && is_writable($file)){
            $fp = fopen($file, 'a');

            $date = date('Y-m-d H:i:s');

            $param_get = json_encode($_GET);
            $param_post = json_encode($_POST);
            $param_request = json_encode($_REQUEST);
            $header = json_encode(getallheaders());

            $log = is_string($log) ? $log : json_encode($log);

            // Get requested script
            if( ($request_uri = $_SERVER['REQUEST_URI']) == '') {
                $request_uri = "REQUEST_URI_UNKNOWN";
            }


            $current = "$date: $request_uri $log\n header:$header \n param method get:$param_get \n param method post:$param_post \n all param : $param_request \n\n\n";
            fwrite($fp, $current, strlen($current));
            fclose($fp);
        }else{
            $fp = fopen($file, "w") or die("Unable to open file $file!");
            
            $date = date('Y-m-d H:i:s');

            $param_get = json_encode($_GET);
            $param_post = json_encode($_POST);
            $param_request = json_encode($_REQUEST);
            $header = json_encode(getallheaders());

            $log = is_string($log) ? $log : json_encode($log);

            // Get requested script
            if( ($request_uri = $_SERVER['REQUEST_URI']) == '') {
                $request_uri = "REQUEST_URI_UNKNOWN";
            }


            $current = "$date: $request_uri $log\n\n header:$header \n\n param method get:$param_get \n param method post:$param_post \n all param : $param_request \n\n\n\n\n";
            fwrite($fp, $current, strlen($current));
            fclose($fp);
        }
    }
}


writeLogAndCreateFile('log');



?>