// Avoid `console` errors in browsers that lack a console.
if(!(window.console && console.log)) {
  console = {
    log: function(){},
    debug: function(){},
    info: function(){},
    warn: function(){},
    error: function(){}
  };
}

// JavaScript Document
var keys = { left:false, right:false, up:false, down:false };
var car = { car:null, speed:0, turn:0, road:null, bg:null, sea:null, land:null, gameWidth:0, gameHeight:0, row:5, col:1, lives:4, startTime:0, onroad:true, offroadtime:0, offRoadSoundPlayed:false, obstruction:"", offscreen:false };
var respawnInfo = { carX:0, carY:0, bgX:0, bgY:0 };
var road;
var obstructions = new Array();
var _MAXSPEED = 9;
var _MAXTURN = 5;
var messageID;
var score;
var sendTimeID;
var baseUrlApi = '../ws/';
var gameId = 1;

function touchDevice(){
	if( navigator.userAgent.match(/Android/i)
		 || navigator.userAgent.match(/webOS/i)
		 || navigator.userAgent.match(/iPhone/i)
		 || navigator.userAgent.match(/iPad/i)
		 || navigator.userAgent.match(/iPod/i)
		 || navigator.userAgent.match(/BlackBerry/i)
		 || navigator.userAgent.match(/Windows Phone/i) ){
		return true;
	}else {
		return false;
	}
}

function initGame(){
	console.log("initGame");
	
	exportRoot.sfx_mc.stop();
	
	exportRoot.start_btn.on("mousedown", function(event) {
		console.log('start mousedown');
		respawn();
		car.lives = 4;
		sentTime = 0;
		exportRoot.start_btn.visible = false;
		createjs.Ticker.addEventListener("tick", gameLoop);
		var d = new Date();
		car.startTime = d.getTime();
		sendTimeID = setInterval( sendTimeInt, 5000 );
		updateMessage(0);
	});
	
	//exportRoot.up_btn.visible = false;
	//exportRoot.left_btn.visible = false;
	//exportRoot.right_btn.visible = false;
	//exportRoot.down_btn.visible = false;
	if (touchDevice()) createjs.Touch.enable(stage);
	
	exportRoot.up_btn.on("mousedown", function(event) {
		console.log('up mousedown ' + event);
		event.preventDefault();
		keys.up = true;
	});
	
	exportRoot.up_btn.on("pressup", function(event) {
		console.log('up pressup');
		keys.up = false;
	});
	
	exportRoot.left_btn.on("mousedown", function(event) {
		
		if (event.preventDefault) event.preventDefault();
		keys.left = true;
	});
	
	exportRoot.left_btn.on("pressup", function(event) {
		keys.left = false;
	});
	
	exportRoot.right_btn.on("mousedown", function(event) {
		if (event.preventDefault) event.preventDefault();
		keys.right = true;
	});
	
	exportRoot.right_btn.on("pressup", function(event) {
		keys.right = false;
	});
	
	exportRoot.down_btn.on("mousedown", function(event) {
		if (event.preventDefault) event.preventDefault();
		keys.down = true;
	});
	
	exportRoot.down_btn.on("pressup", function(event) {
		keys.down = false;
	});
	
	car.car = exportRoot.game_mc.car_mc;
	car.road = exportRoot.game_mc.bg_mc.road_mc;
	car.sea = exportRoot.game_mc.bg_mc.sea_mc;
	car.land = exportRoot.game_mc.bg_mc.land_mc;
	car.bg = exportRoot.game_mc.bg_mc;
	car.gameWidth = 764;//exportRoot.game_mc.width;
	car.gameHeight = 637;//exportRoot.game_mc.height;
	
	respawnInfo.carX = car.car.x;
	respawnInfo.carY = car.car.y;
	respawnInfo.bgX = car.bg.x;
	respawnInfo.bgY = car.bg.y;
	
	exportRoot.lives_mc.stop();
	//car.road.visible = false;
	
	obstructions.push({ camels:{x:646, y:1530}, mummy:{x:1396, y:2416}, tree:{ x:1651, y:3235 }});
	obstructions.push({ camels:{x:1183, y:1535}, mummy:{x:645, y:2439}, tree:{ x:1469, y:2923 }});
	obstructions.push({ camels:{x:1930, y:1642}, mummy:{x:1923, y:2439}, tree:{ x:648, y:2923 }});
	obstructions.push({ camels:{x:1664, y:2580}, mummy:{x:1228, y:3452}, tree:{ x:1446, y:1858 }});
	obstructions.push({ camels:{x:1664, y:3458}, mummy:{x:430, y:2005}, tree:{ x:1530, y:1200 }});
	obstructions.push({ camels:{x:842, y:3458}, mummy:{x:1145, y:2065}, tree:{ x:867, y:1205 }});
	
	car.road.alpha = car.sea.alpha = car.land.alpha = 0.01;
	console.log(car.gameWidth + ", " + car.gameHeight);
	 
	document.onkeydown = keydown;
	document.onkeyup = keyup;
	
	positionMapCar();
}

function keydown(e) {
	// use e.which
	console.log("down " + e.which);
	if (e.preventDefault!=null) e.preventDefault();
	switch(e.which){
		case 81://q
		//gameOver(true);
		break;
		case 80://p
		//gameOver(false);
		break;
		case 32:
		console.log( "Car position (" + car.car.x + ", " + car.car.y + ")");
		break;
		case 37://left
		keys.left = true;
		break;
		case 38://up
		keys.up = true;
		break;
		case 39://right
		keys.right = true;
		break;
		case 40://down
		keys.down = true;
		break;
	}
}

function keyup(e) {
	// use e.which
	//console.log("up " + e.which);
	switch(e.which){
		case 37://left
		keys.left = false;
		break;
		case 38://up
		keys.up = false;
		break;
		case 39://right
		keys.right = false;
		break;
		case 40://down
		keys.down = false;
		break;
	}
}

function clearOffscreen(){
	car.offscreen = false;
}

function positionMapCar(){
	var padding = 8;
	var cell_size = { width:(147-padding)/3.0, height:(241-padding)/6.0 };
	var scale = cell_size.height/637.0;
	var pos = { x:car.col * cell_size.width + car.car.x * scale + 9, y:car.row * cell_size.height + car.car.y * scale + 11 };
	var car_mc = exportRoot.map_mc.car_mc;
	car_mc.x = pos.x;
	car_mc.y = pos.y;
	//console.log( "car:" + car.car.x + ", " + car.car.y + " dot:" + car_mc.x + ", " + car_mc.y + " calc:" + pos.x + ", " + pos.y);
}

function moveCarForward(){
	var theta = car.car.rotation * Math.PI / 180;
	car.car.x += car.speed * Math.sin(theta);
	car.car.y -= car.speed * Math.cos(theta);
	car.speed *= 0.9;
}

function gameLoop(){
	//console.log(keys);
	//car.road.alpha = 0.1;
	var pt1 = { x:car.car.x, y:car.car.y };
	var pt2 = exportRoot.game_mc.localToGlobal(pt1.x, pt1.y);
	pt = car.road.children[0].globalToLocal(pt2.x, pt2.y);
	
	positionMapCar(pt);
	
	var onroad = car.road.children[0].hitTest(pt.x, pt.y);
	//if (onroad) car.road.alpha = 0.1;
	//console.log( "mouse:(" + stage.mouseX + ", " + stage.mouseY + ") pt2:(" + pt2.x + ", " + pt2.y + ")");
	var insea = false;
	if (car.col==2){
		pt = car.sea.children[0].globalToLocal(pt2.x, pt2.y);
		insea = car.sea.children[0].hitTest(pt.x, pt.y);
		if (insea){
			updateMessage(2);
			createjs.Ticker.removeEventListener("tick", gameLoop);
			createjs.Ticker.addEventListener("tick", moveCarForward);
			playSound("splash");
			setTimeout(respawn, 1200);
			return;
		}
	}
		
	var speed = car.speed;
	var d = new Date();
	var now = d.getTime();
	
	if (!onroad && car.onroad){
		car.offroadtime = now;
	}else if (!onroad && !car.onroad){
		var elapsedTime = now - car.offroadtime;
		if (elapsedTime>800 && !car.offRoadSoundPlayed){
			playSound("skid");
			car.offRoadSoundPlayed = true;
		}
	}else if (onroad){
		car.offRoadSoundPlayed = false;
	}
	car.onroad = onroad;
	
	if (keys.up){
		if (onroad){
			car.speed += 2;
			if (car.speed>_MAXSPEED) car.speed = _MAXSPEED;
		}else{
			car.speed += 0.4;
			if (car.speed>(_MAXSPEED * 0.6)) car.speed *= 0.9;
		}
	}else if (keys.down){
		car.speed -= 5;//Braking
		if (onroad){
			if (car.speed<-(_MAXSPEED*0.3)) car.speed = -(_MAXSPEED*0.3);
		}else{
			if (car.speed<-(_MAXSPEED * 0.2)) car.speed = -(_MAXSPEED * 0.2);
		}
	}else{
		car.speed *= 0.99;
		if (!onroad && car.speed>1) car.speed *= 0.3;
		if (car.speed<1) car.speed = 0;
	}
	
	if (keys.right){
		car.turn += (car.speed+1)/(_MAXSPEED+1) * 5;
		if (car.turn>_MAXTURN) car.turn = _MAXTURN;
	}else if (keys.left){
		car.turn -= (car.speed+1)/(_MAXSPEED+1) * 5;
		if (car.turn<-_MAXTURN) car.turn = -_MAXTURN;
	}else{
		car.turn *= 0.6;
		if (car.turn<1) car.turn = 0;
	}
	
	if (car.obstruction!="" || car.offscreen) car.turn = 0;
	if (car.turn!=0) car.car.rotation += car.turn;
	
	if ((car.obstruction!=""||car.offscreen) && car.speed>0) car.speed = 0;
	
	if (car.speed!=0){
		var theta = car.car.rotation * Math.PI / 180;
		car.car.x += car.speed * Math.sin(theta);
		car.car.y -= car.speed * Math.cos(theta);
	}
	
	if (car.car.x<0){
		if (car.col>0){
			car.car.x += car.gameWidth;
			car.bg.x += car.gameWidth;
			car.col--;
		}else{
			car.speed = 0;
			car.x = pt1.x;
			car.y = pt1.y;
			if (!car.offscreen){
				playSound("clunk");
				car.offscreen = true;
				setTimeout(clearOffscreen, 1000);
			}
		}
	}else if(car.car.x>car.gameWidth){
		if (car.col<2){
			car.col++;
			car.car.x -= car.gameWidth;
			car.bg.x -= car.gameWidth;
		}else{
			//In the sea
			updateMessage(2);
			playSound("splash");
			respawn();
		}
	}
	
	if (car.car.y<0){
		if (car.row>0){
			car.row--;
			car.car.y += car.gameHeight;
			car.bg.y += car.gameHeight;
		}else{
			car.speed = 0;
			car.x = pt1.x;
			car.y = pt1.y;
			if (!car.offscreen){
				playSound("clunk");
				car.offscreen = true;
				setTimeout(clearOffscreen, 1000);
			}
		}
	}else if(car.car.y>car.gameHeight){
		if (car.row<5){
			car.car.y -= car.gameHeight;
			car.bg.y -= car.gameHeight;
			car.row++;
		}else{
			car.speed = 0;
			car.x = pt1.x;
			car.y = pt1.y;
			if (!car.offscreen){
				playSound("clunk");
				car.offscreen = true;
				setTimeout(clearOffscreen, 1000);
			}
		}
	}
	
	pt2 = exportRoot.game_mc.localToGlobal(car.car.x, car.car.y);
	pt = car.land.children[0].globalToLocal(pt2.x, pt2.y);
	var onland = car.land.children[0].hitTest(pt.x, pt.y);
	
	if (onland){
		car.car.x = pt1.x;
		car.car.y = pt1.y;
	}
	
	if (car.row==0 && car.col==2){
		pt1 = { x:148, y:220 };
		pt2 = { x:pt1.x - car.car.x, y:pt1.y - car.car.y };
		var dist = Math.sqrt( pt2.x*pt2.x + pt2.y*pt2.y);
		if (dist<100){
			gameOver(true);
			return;
		}
	}
	
	checkObstructions();
	checkTimeBonuses();
	rotateMummy();
	updateGameHUD();
}

function rotateMummy(){
	var mummy_mc = exportRoot.game_mc.bg_mc.mummy_mc;
	var pt1 = exportRoot.game_mc.localToGlobal(car.car.x, car.car.y);
	var pt2 = exportRoot.game_mc.bg_mc.localToGlobal(mummy_mc.x, mummy_mc.y);
	var delta = { x:pt1.x-pt2.x, y:pt1.y - pt2.y };
	var theta = Math.atan2(delta.y, delta.x);
	mummy_mc.rotation = theta * (180.0/Math.PI) + 90;
}

function checkTimeBonuses(){
	var pt1 = exportRoot.game_mc.localToGlobal(car.car.x, car.car.y);
	var mcs = new Array( exportRoot.game_mc.bg_mc.time1_mc, exportRoot.game_mc.bg_mc.time2_mc, exportRoot.game_mc.bg_mc.time3_mc );
	for(var i=1; i<=3; i++){
		var mc = mcs[i-1];
		if (mc.visible){
			var pt2 = exportRoot.game_mc.bg_mc.localToGlobal(mc.x, mc.y);
			var pt3 = { x:pt1.x-pt2.x, y:pt1.y-pt2.y };
			var dist = Math.sqrt( pt3.x*pt3.x + pt3.y*pt3.y );
			if (dist<80){
				mc.visible = false;
				var d = new Date();
				var now = d.getTime();
				car.startTime += 10000;
				if ((now - car.startTime)<0) car.startTime = now;
				incScore(-100);
				sentTime -= 100;
				updateMessage(3);
			}
		}
	}
}

function updateMessage(frm){
	var mc = exportRoot.message_mc;
	mc.gotoAndStop(frm);
	mc.visible = true;
	clearTimeout(messageID);
	messageID = setTimeout( hideMessage, 3000 );
}

function hideMessage(){
	var mc = exportRoot.message_mc;
	mc.visible = false;
}

function checkObstructions(){
	var obstruction = "";
	var pt1 = exportRoot.game_mc.localToGlobal(car.car.x, car.car.y);
	var pt2 = exportRoot.game_mc.bg_mc.localToGlobal(exportRoot.game_mc.bg_mc.camels_mc.x, exportRoot.game_mc.bg_mc.camels_mc.y);
	var pt3 = { x:pt1.x-pt2.x, y:pt1.y-pt2.y };
	var dist = Math.sqrt( pt3.x*pt3.x + pt3.y*pt3.y );
	if (dist<150) obstruction = "camel";
	if (obstruction==""){
		pt2 = exportRoot.game_mc.bg_mc.localToGlobal(exportRoot.game_mc.bg_mc.mummy_mc.x, exportRoot.game_mc.bg_mc.mummy_mc.y);
		pt3 = { x:pt1.x-pt2.x, y:pt1.y-pt2.y };
		dist = Math.sqrt( pt3.x*pt3.x + pt3.y*pt3.y );
		if (dist<100) obstruction = "mummy";
	}
	if (obstruction==""){
		pt2 = exportRoot.game_mc.bg_mc.localToGlobal(exportRoot.game_mc.bg_mc.tree_mc.x, exportRoot.game_mc.bg_mc.tree_mc.y);
		pt3 = { x:pt1.x-pt2.x, y:pt1.y-pt2.y };
		dist = Math.sqrt( pt3.x*pt3.x + pt3.y*pt3.y );
		if (dist<100) obstruction = "tree";
	}
	if (obstruction!=""){
		var sfx = (obstruction=="camel") ? "camel" : "clunk";
		if (car.obstruction != obstruction){
			car.speed = 0;
			playSound(sfx);
			car.lives--;
			if (car.lives==0){
				gameOver(false);
				return;
			}else{
				updateMessage(1);
			}
		}
	}
	
	car.obstruction = obstruction;
}

function updateGameHUD(){
	var d = new Date();
	var secs = Math.floor((d.getTime() - car.startTime)/1000.0);
	var mins = Math.floor(secs / 60);
	secs %= 60;
	exportRoot.time_txt.text = (secs>9) ? mins + ":" + secs : mins + ":0" + secs;
	var pt1 = {x:1927, y:643 };
	var pt2 = exportRoot.game_mc.localToLocal( car.car.x, car.car.y, car.bg );
	var pt3 = { x:pt1.x - pt2.x, y:pt1.y - pt2.y };
	var sqdist = pt3.x * pt3.x + pt3.y * pt3.y;
	var dist = Math.floor((sqdist/12057724.0) * 313);
	//console.log("car is at (" + pt.x + ", " + pt.y + ") sqdist:" + sqdist);
	exportRoot.distance_txt.text = dist + "m";
	exportRoot.lives_mc.gotoAndStop(car.lives);
}

function respawn(){
	car.col = 1;
	car.row = 5;
	car.speed = 0;
	car.car.rotation = 0;
	car.lives--;
	if (car.lives == 0){
		gameOver(false);
		return;
	}
	car.car.x = respawnInfo.carX;
	car.car.y = respawnInfo.carY;
	car.bg.x = respawnInfo.bgX;
	car.bg.y = respawnInfo.bgY;
	
	var idx = Math.floor(Math.random() * obstructions.length);
	do{
		var idx1 = Math.floor(Math.random() * obstructions.length);
	}while (idx1==idx);
	console.log("obstructions idx:" + idx + " idx1:" + idx1);
	car.bg.camels_mc.x = obstructions[idx].camels.x;
	car.bg.camels_mc.y = obstructions[idx].camels.y;
	car.bg.mummy_mc.x = obstructions[idx].mummy.x;
	car.bg.mummy_mc.y = obstructions[idx].mummy.y;
	car.bg.tree_mc.x = obstructions[idx].tree.x;
	car.bg.tree_mc.y = obstructions[idx].tree.y;
	car.bg.time1_mc.x = obstructions[idx1].camels.x;
	car.bg.time1_mc.y = obstructions[idx1].camels.y;
	car.bg.time2_mc.x = obstructions[idx1].mummy.x;
	car.bg.time2_mc.y = obstructions[idx1].mummy.y;
	car.bg.time3_mc.x = obstructions[idx1].tree.x;
	car.bg.time3_mc.y = obstructions[idx1].tree.y;
	
	createjs.Ticker.removeEventListener("tick", moveCarForward);
	createjs.Ticker.addEventListener("tick", gameLoop);
	
	positionMapCar();
}

function gameOver(completed){
	clearInterval(sendTimeID);	
	createjs.Sound.removeAllSounds();
	playSound("game_over");
	if (completed){
		var d = new Date();
		var now = d.getTime();
		var elapsedTime = Math.floor((now - car.startTime)/100);
		var offset = elapsedTime - sentTime;
		sentTime += offset;
		console.log("gameOver: sentTime=" + sentTime + " offset=" + offset);
		score = { offset:offset, time:elapsedTime};
		setTimeout(sendFinalScore, 100);
		//updateMessage("Well done!");
	}else{
		window.parent.change_page('sorry');
		//updateMessage("You ran out of lives!");
	}
	car.lives = 4;
	respawn();
	createjs.Ticker.removeEventListener("tick", gameLoop);
	updateGameHUD();
	exportRoot.lives_mc.gotoAndStop(0);
	exportRoot.start_btn.visible = true;
}

//Called after the small time update is sent
function sendFinalScore(){
	email = localStorage.getItem("email");
	
	console.log("sendFinalScore email:" + email);
	
	if (email!=null){
		console.log("sendFinalScore sendscore:" + score);
		// user win.
		$.ajax({
			type:'post',
			url: baseUrlApi + "sendscore.php",
			dataType:"json",
			data:{score:score.time, offset:score.offset, email:email, gameId:gameId},
			success: function(data){
				console.log("sendFinalScore:returned success:" + data.success + " error:" + data.error + " top_ten:" + data.topten + " is_nearhigh:" + data.is_nearhigh);
				if(data.topten == true){
					window.parent.change_page('congratulations');
				}else{
					if(data.is_nearhigh == true){
						window.parent.change_page('well_done');
					}else{
						window.parent.change_page('sorry');
					}
				}			
			}
		});
	}else{
		window.parent.change_page('sorry');
	}
}

//Called every 5 secs
function sendTimeInt(){
	var inc = 50;
	sentTime += inc;
	incScore(inc);
}

//Make small incremental increase/decrease to server score
function incScore(score){
	$.ajax({
		type:'post',
		url: baseUrlApi + 'inc.php',
		data:{score:score, gameId:gameId},
		success: function(data){
			console.log("inc:returned " + data);
		}
	});
}
