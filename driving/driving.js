(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 637,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bg.jpg", id:"bg"},
		{src:"images/camels.png", id:"camels"},
		{src:"images/car.png", id:"car"},
		{src:"images/down.png", id:"down"},
		{src:"images/heart.png", id:"heart"},
		{src:"images/heart2.png", id:"heart2"},
		{src:"images/left.png", id:"left"},
		{src:"images/map.png", id:"map"},
		{src:"images/message.png", id:"message"},
		{src:"images/messages_obstuctions.png", id:"messages_obstuctions"},
		{src:"images/messages_sea.png", id:"messages_sea"},
		{src:"images/messages_start.png", id:"messages_start"},
		{src:"images/messages_time.png", id:"messages_time"},
		{src:"images/mummy.png", id:"mummy"},
		{src:"images/right.png", id:"right"},
		{src:"images/sidepanel.jpg", id:"sidepanel"},
		{src:"images/start.png", id:"start"},
		{src:"images/time_bonus.png", id:"time_bonus"},
		{src:"images/tree.png", id:"tree"},
		{src:"images/up.png", id:"up"},
		{src:"sounds/camel.mp3", id:"camel"},
		{src:"sounds/car_1.mp3", id:"car_1"},
		{src:"sounds/clunk.mp3", id:"clunk"},
		{src:"sounds/game_over.mp3", id:"game_over"},
		{src:"sounds/skid.mp3", id:"skid"},
		{src:"sounds/splash.mp3", id:"splash"}
	]
};



// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2790,4671);


(lib.camels = function() {
	this.initialize(img.camels);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,208,243);


(lib.car = function() {
	this.initialize(img.car);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,183,296);


(lib.down = function() {
	this.initialize(img.down);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,35,39);


(lib.heart = function() {
	this.initialize(img.heart);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,22,22);


(lib.heart2 = function() {
	this.initialize(img.heart2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,22,22);


(lib.left = function() {
	this.initialize(img.left);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.map = function() {
	this.initialize(img.map);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,241);


(lib.message = function() {
	this.initialize(img.message);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,445,271);


(lib.messages_obstuctions = function() {
	this.initialize(img.messages_obstuctions);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,445,271);


(lib.messages_sea = function() {
	this.initialize(img.messages_sea);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,445,271);


(lib.messages_start = function() {
	this.initialize(img.messages_start);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,445,271);


(lib.messages_time = function() {
	this.initialize(img.messages_time);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,445,271);


(lib.mummy = function() {
	this.initialize(img.mummy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,82,106);


(lib.right = function() {
	this.initialize(img.right);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.sidepanel = function() {
	this.initialize(img.sidepanel);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,196,637);


(lib.start = function() {
	this.initialize(img.start);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,113,113);


(lib.time_bonus = function() {
	this.initialize(img.time_bonus);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,115,112);


(lib.tree = function() {
	this.initialize(img.tree);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,277,189);


(lib.up = function() {
	this.initialize(img.up);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.up_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.up();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.2,scaleY:1.2,x:-3.9,y:-3.5},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.tree_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.tree();
	this.instance.setTransform(-127.3,-95.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-127.3,-95.5,277,189);


(lib.time_bonus_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.time_bonus();
	this.instance.setTransform(20.2,19.6,0.65,0.65);

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj6D0QhnhmAAiOQAAiOBnhkQBohmCSAAQCTAABnBmQBpBkAACOQAACOhpBmQhnBkiTAAQiSAAhohkg");
	this.shape.setTransform(56.6,54.3);

	this.addChild(this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(20.2,19.6,74.8,72.8);


(lib.start_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.start();

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ao1I/IAAx9IRrAAIAAR9g");
	this.shape.setTransform(56.1,57.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{scaleX:1,scaleY:1,x:0,y:0}}]}).to({state:[{t:this.instance,p:{scaleX:1.13,scaleY:1.13,x:-7.3,y:-7.3}}]},1).to({state:[{t:this.instance,p:{scaleX:1.13,scaleY:1.13,x:-4.5,y:-4.5}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,113,113);


(lib.sfx = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		playSound("camel");
	}
	this.frame_2 = function() {
		playSound("car_1");
	}
	this.frame_3 = function() {
		playSound("clunk");
	}
	this.frame_4 = function() {
		playSound("game_over");
	}
	this.frame_5 = function() {
		playSound("skid");
	}
	this.frame_6 = function() {
		playSound("splash");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1).call(this.frame_5).wait(1).call(this.frame_6).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.seaMC = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#75C0EA").s().p("EgEPC5CIBnAAIhemtMACLhpcMgKuhkbMgNqhDSMAKug1pMAPRhEBIDcAAIG4voIm1AAICrr8MALnhAzIH6AcIA9AAMgB7JnnI/MBog");
	this.shape.setTransform(2401.4,2304.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(2233,327.5,337,3953.4);


(lib.roads_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F2BE63").s().p("EgDjEqIQlflagSiAMiFpAAAQjkhqgyjZMAAAh2EQgPrtosA6I0+AAIAAu3MAlVAAAQFNAPCUGYMAAAB3NQgjG4GqAxMB0wAAAQgajAE/kbQE8kbD1hMIAAtXQgGnsobghMhHZAAAQpCnTJbnUMBIFAAAQG6heganIIAAzIQACnUn7AEMhmWAAAQlih/hNkUMAAAhFrQAgnbodgvMg2uAAAIAAvKMB3kAAAQLyAFiSp/QAnofgcoTQBPoYpXg7Mg1vAAAQj4g4hJkTMAAAg2GQgLlRlwhiMg3VAAAIAAvDIXHAAQEBgzAUlHMAAAg4hQgKlVk4gTI3NAAIAAvoMAmKAAAQDcAqCbEuIAAUKQg+FIEbBjIICAAQGqgEgvnZMAAAgj8Qg4o+nLB2MgkmAAAQjIhdhvjjMgAXirPIQuAAMAAABDwQhIH5GTAeIUyAAQF1hjA2nJMAAAhDlIP6AAMABVAkUQgpGdGcAAMA15AAAQHSAXA3nOMAAAgjcIOHAAMAAAA7BMhRLAAAQj6IgD6JsMBc/AAAQHXhlgXm9MAAAhFnIQRAAMAAABFUQAkG9E5BaIGuAAQE+hbg2mGIB16aMAkdAAAQA6lPDIi3QDIi4GViZIAA9TIPOAAIAAd/QE9BIFSFGQFRFGhDJ8QhDJ8kvEMQkvEMj2CMIAAUnMj0oAAAQlpAGhEGXMAAAA1AQg3HHGVBOIVaAAQIHAug7qdIA6x2QBBkDDihkMDkxgAuMABjgwQIPAigMAAAhMQIQaAAMAAoBNiIYiAAIm4PoMgj9AAAMAAAAycI65AAQkfBAhHEZIAAGJQBrE+EaBLMAqXAAAIh4PoMgqiAAAIjhD8MgBxA+UMidoAAAQnEgTg6F1QgkC7AkDOQgeGCFyAHMBN8AAAIAAbkQBBFIGABPMA44AAAQEEh6BJmAIAA6AIcYAAQDXABBDlJMAAAg9YIU7AAIAAPTIlnCXMAAAAqVQARGFEaBYIIeAAIBtO2MgoLAAAQnpANg8HgMAAACsnIjjDQMhHKAAAQlXgVihEPQhODmAuDnQggHIGEArMBZYAAAQGqgdAPovMAAAhkAQgUnLFngrMAjfAAAICqPUI1wAAQmjApALHXMAAABV8QgKGeEqBCIU+AAIDcPuMg4RAAAQmngChCHYMAAABXzQgZDxjmANMhBSAAAQgmColgFaQl+F4odAAQocAAl9l4gEARTDH9MAAAA+wQEdATEjFRQEjFRgzDTMAx9AAAQF6g0A5nhMAAAhEtQBPojorghMgz4AAAQgpgDgnAAQokAAgYJRgEgfpBz5MAAAA0gQA5G7EhA3IWNAAQHmiVgYlxIAA0KQBLlNGjg6MBClAAAQJbAxgYoGIg4ywQgkoZpLgZMhkIAAAQnnAlALIXgEg4PCvPQKMAqhdp2MAAAg0UQBkmNnyglMgk4AAAQpKBXBLHbMAAAA0qQAZHYF4gigEgfoBChIAASxQAeFNFlByMBlqAAAQLjgnhjoHIAAyGQhBoro1gQMhjqAAAQpOALBBJ0gEiERg1wIAAUUQAAHSFlAfIUZAAQFEAcB4FAMAAAA2uQgQF6F+A7MAosAAAQD6glBZlIQAn2PghvGQHnuDH8NyMgAFAlbQgHDxE1B6IXWAAQGchNAQkUMAAAg2MQAxmLoPh8MhHGAAAQkChthXktIAAzjQBDnSoBgcMgkPAAAQoDAOASGagAS6L4IAAXaQgfFfFeAIMA3AAAAQHOAjAEmpIAA2uQhtkWkLAAMg4NAAAQkwAigcDngEhB7iKCMABbA41QA+E9DaAuIX6AAQFohvgknHMAAAhHRQgklSneAsMg0DAAAQm/gIgNFCMAAABHlQAJFmDICBIJjAAQD+gnAOk/MAAAg4TQEMk/D4AAQD4AADkE/gEgPWiXuMgANBEtQhWHJKMANMBXKAAAQFFgEAum6MAAAhGeQA7oIpiBKMhV6AAAQoEAaA/H9gEA0WjuHIAAF5QBWF/EHgFIY6AAIDgl8QjvhhjGjDQjZjXhhkHIxxAAQjsBkgrEng");
	this.shape.setTransform(1331.4,2266.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(197.3,320.4,2268.2,3891.4);


(lib.right_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.right();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.2,scaleY:1.2,x:-3.9,y:-3.5},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.mummy_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.mummy();
	this.instance.setTransform(-38.4,-58.9);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-38.4,-58.9,82,106);


(lib.message_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer 2
	this.instance = new lib.messages_start();
	this.instance.setTransform(-6,0);

	this.instance_1 = new lib.messages_obstuctions();
	this.instance_1.setTransform(0,-0.1);

	this.instance_2 = new lib.messages_sea();
	this.instance_2.setTransform(0,-1.1);

	this.instance_3 = new lib.messages_time();
	this.instance_3.setTransform(-5,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

	// Layer 1
	this.instance_4 = new lib.message();

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,0,451,271);


(lib.mapcarMC = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("AAeAAQAAAMgJAIQgJAKgMAAQgLAAgJgKQgJgIAAgMQAAgLAJgJQAJgJALAAQAMAAAJAJQAJAJAAALg");
	this.shape.setTransform(3,3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgUAVQgIgJgBgMQABgLAIgJQAJgIALgBQAMABAJAIQAJAJAAALQAAAMgJAJQgJAIgMABQgLgBgJgIg");
	this.shape_1.setTransform(3,3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8,8);


(lib.left_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.left();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.2,scaleY:1.2,x:-3.9,y:-3.5},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39,35);


(lib.land_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(117,192,234,0.098)").s().p("Egl1E5HIAAjdMiMZAAAMAAAiRMIQhAAMAAACCDMCCwAAAIAAJpMA72AAAIAAtEMBJpAAAMAAAhhvMA3vAAAMAAABy7MgvfAAAIAACxMhAsAAAIAACEgEiEzEG4MAAAiIQMguHAAAIAAxOMAy7AAAIAAEIIBZAAMAAABNGMBsCAAAIAAT8MhsCAAAIAAeSMBgWAAAIAASlMhgWAAAIAADdgEAWtD/TMAAAg9PMAvfAAAMAAAA9PgEASlCgWIAAjdMBTRAAAMAAAi1pIDcAAIAABYMAyPAAAMAAABEGMgyPAAAMAAABxjIiwAAIAACFgECLACfpMAAAhOdIZeAAMAAABOdgEhiZCfpMAAAgy7MAnPAAAMAAAAy7gEgc4CdlMAAAgvfILsAAIAAAsMBZcAAAIAAMZMhZcAAAMAAAAiagEiy6BMYMAAAhT7MAyPAAAMAAABEGMBAAAAAIAAP1gEgc4BLsIAAylMBjwAAAIAASlgATRaJIAAylMAtaAAAIAASlgEghBAaJMAAAgstI/qAAMAAAAsBMghCAAAMAAAg1qIkIAAIAArsI+SAAIAAzRMAgWAAAIAAQgIDcAAIAAOdIfqAAIAABYMAmjAAAIAAEJINxAAMAAAAw1gAT99kIAA62MhSjAAAIAAkJMClIAAAMAAAhCvIBYAAIAAgsMAnOAAAIAAE0MgkeAAAMAAABCEIiwAAIAAAsI/qAAIAAa2gEizmgtZMAAAgswINFAAMAAAAswgEhjFha1MAAAhFfICwAAIAAgsMAsvAAAIAACvIEIAAMAAABCFIwhAAMAAAg/VI+RAAMAAABAsgEiHjhc4IAA86MguHAAAIAAoRMA0/AAAMAAAAlLgEgK/hdkMAAAhBZMBRLAAAMAAABBZgEBnOic4IAAk1MAjGAAAMAAAgsuITRAAMAAAAsuIpoAAIAAE1gEiR4ijFMAAAgp+ICEAAIAAjdMD0+AAAIAANFMjrVAAAMAAAAgWgEBl2jCDMAAAgmiIE0AAMAAAAmigECYxjsuMAAAhKUIc6AAMAAABKUgEhe9jvdIAAlhMBPJAAAMAAAhDcII9AAMAAABIRIhYAAIAAAsgEByPjy6MAAAgomIlhAAIAA7iIFhAAIAAgsIG4AAMAAABE0gEiRMjzmMAAAhEIINxAAMAAABEIgEATRj2WMAAAhBYIBYAAIAAhYMAsCAAAMAAAAhuMgqqAAAMAAAAhCgEhdkkXYMAAAghCMAp+AAAMAAAAhCg");
	this.shape.setTransform(1367.5,2306);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(204.8,302.1,2325.5,4007.9);


(lib.heart_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(8));

	// Layer 1
	this.instance = new lib.heart();

	this.instance_1 = new lib.heart();

	this.instance_2 = new lib.heart();

	this.instance_3 = new lib.heart();

	this.instance_4 = new lib.heart();

	this.instance_5 = new lib.heart();

	this.instance_6 = new lib.heart();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance,p:{x:0}}]},1).to({state:[{t:this.instance_1,p:{x:0}},{t:this.instance,p:{x:18.6}}]},1).to({state:[{t:this.instance_2,p:{x:0}},{t:this.instance_1,p:{x:37.2}},{t:this.instance,p:{x:18.6}}]},1).to({state:[{t:this.instance_3,p:{x:0}},{t:this.instance_2,p:{x:55.8}},{t:this.instance_1,p:{x:37.2}},{t:this.instance,p:{x:18.6}}]},1).to({state:[{t:this.instance_4,p:{x:0}},{t:this.instance_3,p:{x:74.4}},{t:this.instance_2,p:{x:55.8}},{t:this.instance_1,p:{x:37.2}},{t:this.instance,p:{x:18.6}}]},1).to({state:[{t:this.instance_5,p:{x:0}},{t:this.instance_4,p:{x:93}},{t:this.instance_3,p:{x:74.4}},{t:this.instance_2,p:{x:55.8}},{t:this.instance_1,p:{x:37.2}},{t:this.instance,p:{x:18.6}}]},1).to({state:[{t:this.instance_6},{t:this.instance_5,p:{x:93}},{t:this.instance_4,p:{x:74.4}},{t:this.instance_3,p:{x:55.8}},{t:this.instance_2,p:{x:37.2}},{t:this.instance_1,p:{x:18.6}},{t:this.instance,p:{x:111.6}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.down_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.down();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.2,scaleY:1.2,x:-3.5,y:-3.9},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,35,39);


(lib.car_2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.car();
	this.instance.setTransform(-35,-57,0.37,0.37);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-35,-57,67.7,109.5);


(lib.camelanim3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#554718").s().p("AgNALIABgGQAAgGACgFQAEgHAGgDQAEgCAEAEIAAgCQAAABAAABQABAAAAABQAAAAAAABQABAAAAABQABAAABAAQAAABABAAQAAABAAAAQABAAAAABQABACgCAFQgBADgHAEIgFABIgEAEQgFAGgDABQgCgCABgFg");
	this.shape.setTransform(26.9,17.3,4.188,4.188);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#654815").s().p("AAAAEIgDgEQAAAAAAAAQAAAAAAgBQgBAAABgBQAAAAAAAAIACgCQgCAEAHAAQABAAgBABQAAABAAAAQAAABgBAAQAAABgBAAIgCABIAAgBg");
	this.shape_1.setTransform(33.2,20.6,4.188,4.188);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#654815").s().p("AgFABQgBgBAGgBQADgBACABIABABQgGACgCAAIgDgBg");
	this.shape_2.setTransform(19,14.9,4.188,4.188);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#654815").s().p("AAAACQgDAAAAgCQAAgCADAAIAEAAIAAADQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAgBAAIgBgBg");
	this.shape_3.setTransform(26.3,8.9,4.188,4.188);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#654815").s().p("AgCAQIABgFQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIACgBQAEgMAFgFQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAIABAAIgHAPQAEABgBAIQgCAHgCACIgCgCg");
	this.shape_4.setTransform(33.8,9.5,4.188,4.188);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B1833A").s().p("AAOAXIgDgKIgEgBQgDgBgCgEQgEACgDgCIgJACIgGACQgDABAAgEQAAgDACgDQADgCADABIADABQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIADgDQACgFAGgBIAAAAIAJgJIADgFQADgDACAAIAAAAIABAAQAEACAAADQABABgEAIIgDAJIAAABQADABAAADQAAAFgDADIAAABIgBABIAAAAIAAAAIAFAFQABADgCADQgBAAAAABQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAgBAAQAAgBAAgBg");
	this.shape_5.setTransform(26.3,11.1,4.188,4.188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CF9F59").s().p("AAAADQgCAEgGADQAFgHgCgCIgHACIADgDIADgGIAEgFQACgDACgBQAFAAACADQACACABAHIgBAIIACAIQgEgCgCgIIgBAGIAAAGIgCgDQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAAAAAABQgCAEgDABIADgNg");
	this.shape_6.setTransform(9.8,171.3,4.188,4.188);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CF9F59").s().p("AgCADIAAgJIABgGIABACIACABQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgCgKg");
	this.shape_7.setTransform(11.8,119.9,4.188,4.188);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CF9F59").s().p("AgGAEQABgEACAAIAFgGIADgCIACgCIABAGQgBAEgCAAQgEACAAADIgBAFQAAAAAAAAQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAAAAAQAAAAgBAAQgCAAgCAEIABgHg");
	this.shape_8.setTransform(7.1,57.8,4.188,4.188);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CF9F59").s().p("AAAASQAAgHgCAAIgEAEQgBgCADgIIACgIQgBABAAABQAAAAgBAAQAAAAgBAAQAAAAgBgBIgBgFIACgJIAAgJQACgFAAgDIABABQAJAKABAWQAAAGgDAMIgBAHIAAAJQgDgCgBgOg");
	this.shape_9.setTransform(16.3,90.5,4.188,4.188);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#654815").s().p("AgBCJIgBgIQAEAKAHgFQAHgFAAgJQAAgegLAHIACgGIACgFQABgEgDgCQgDgDgDADQgCAEgCAOQgDgKAAgQIABgPIADgUQABgVgLgVQgCgJgCgEIAJAKQAEAAgCgGIgDgIIADABQgDgNgOgLIAFABQABAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgDgCgEIgEgHIAAgBQgCgFAAgKQAAgZAHgPQANgeAJAEIgFAHIgEAJQgJANgEAVQgCAKACALQABAGADAFQACAEAGACQAGAHADATQABAIgCAOQAAAFADAGIAEAMQAIAVgDAcQgCAOADALQAEAMgCAMIgBAKQgCAGgDAEQgDAEgNAFIgBgHg");
	this.shape_10.setTransform(26.9,78.2,4.188,4.188);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B1833A").s().p("AgGCBIgCgYQgBgIgFgNQgGgPgBgGQgDgYACgVQAFgOgBgEIgEgIQgDgGAAgEQACgNAEgIQABgDAGgEQAGgDAAgEIACgSQAEgbACgPQAIgWAOgMIACAAQAAAFgEAHIgHALQgJASgEAZIgBAWIABAPIABANIAEAJIAEAJIAEASIAHAWQADAKgCAOIgDAMQgBAFACAMIAAAFIAAAHQgBADgEgEQgEgDgDADQgDAEADAFIAFAHQgFgDgFAGQgCADAAAIQAAAHAFAUQgJgDgEgQg");
	this.shape_11.setTransform(12.9,75.8,4.188,4.188);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#906829").s().p("AgUApQAAgGAGgMQACgEAHgHQAFgIADgEQAGgGgBgHIgBgHQgCgEgHgEIAWgMQACAEgGAIIgBAVQgCAGgGAFIgJAMIgJALQgHAJgBAFg");
	this.shape_12.setTransform(20,150.1,4.188,4.188);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#906829").s().p("AgNCQQgIgKgBgUQgBgTgGgOQgOggAKgvQAAgCgBgDIgEgHQgBgCAAgFQAAgLAEgGQAFgKAEgCIAEAAQAEgBAAgIIAAgWIADgSIACgFQAFgXAIgPQAIgLAFAAQAGABgBAMQgCAIgGAOQgGAOgCAHQgCAMADAPIADALQACADAHACQADAEAGANQAEAMgBAGIgCAKIAAAIQABAHAIAOQADALACAQQAAAGgCAPQgCAMADAJQAEAOgCATQgEATgNABIgMgBQgDgBgEACIgFADIgCABQgEAAgGgGg");
	this.shape_13.setTransform(19.3,76.3,4.188,4.188);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQAAAAAAgBQABgBAAAAQABAAAAgBQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_14.setTransform(35.6,119.4,4.188,4.188);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#906829").s().p("AgHAIQgEgFACgGQACgKAFAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_15.setTransform(34.8,118.3,4.188,4.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACACAAAHQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_16.setTransform(34.3,116.1,4.188,4.188);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQABgDADAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_17.setTransform(29.4,54.8,4.188,4.188);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#906829").s().p("AgHAIQgEgFACgGQADgKAEAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_18.setTransform(28.7,53.7,4.188,4.188);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACABAAAIQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_19.setTransform(28.2,51.6,4.188,4.188);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#654815").s().p("AgEAKQAEACgFgMQgCgDAGgBQgBgBAAgBQAAAAAAgBQABgBAAAAQAAAAABgBQAAAAAAAAQAAgBAAABQABAAAAAAQAAABABABIgBgEIAFAJIABAHQgBAFgFACIgBAAQgDAAgBgCg");
	this.shape_20.setTransform(8.5,114.2,4.188,4.188);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#906829").s().p("AAAANQgGgDgDgHQgEgHAHAAIAAgCIABgCIAEABQAAgHAEACQADABADAGQADAIgBAEQgDAGgGAAIgCAAg");
	this.shape_21.setTransform(6.5,113.9,4.188,4.188);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#B1833A").s().p("AAAANQgHgCgDgHQgCgIAFAAIAAgCIABgCIAEABQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQABgDACAAQABABAFAGQAEAGgDAHQgDAHgFAAIgCgBg");
	this.shape_22.setTransform(5,112.5,4.188,4.188);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#654815").s().p("AgFAKQADABgBgFIgCgGQgCgDAFgBQAAgBAAgBQAAAAAAgBQABgBAAAAQABAAAAgBQAAAAAAAAQAAgBAAABQABAAAAAAQABABAAABIgBgEIAEAJIACAHQgBAFgFACIgBAAQgDAAgCgCg");
	this.shape_23.setTransform(8.7,66.8,4.188,4.188);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#906829").s().p("AAAANQgGgDgCgHQgEgHAGAAIACgEQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABABQAAgHAEACQAEACACAFQADAJgBADQgDAGgFAAIgDAAg");
	this.shape_24.setTransform(6.7,66.5,4.188,4.188);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#B1833A").s().p("AAAANQgHgCgCgHQgEgIAGAAIACgEQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAIADgDQACABAEAGQAFAGgEAHQgDAHgFAAIgCgBg");
	this.shape_25.setTransform(5.2,65.1,4.188,4.188);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#554718").s().p("AgGAQIgBgIQgEgHAAgDQABgIAEgFQAEgFADADIAAgCQAAAAAAABQABAAAAAAQABABAAAAQABAAABAAIAEABQACABABAGQABAGgFADIgEAFIgDAFQgBAIgCACQgDgBgBgDg");
	this.shape_26.setTransform(20.3,18.6,4.189,4.189,5.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#654815").s().p("AABAEIgCgDQgBAAgBgBQAAAAgBAAQAAAAAAAAQAAAAAAAAIABgDQABADAHgBQABABgBACQgBAAAAABQAAAAgBABQAAAAAAAAQgBAAAAAAIgBAAg");
	this.shape_27.setTransform(27.7,19,4.189,4.189,5.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#654815").s().p("AgEAEQgCgDAFgBQACgDACAAIACAAIgFADQgBAEgCAAIgBAAg");
	this.shape_28.setTransform(12.6,18.7,4.189,4.189,5.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#654815").s().p("AgDACQAAAAAAgBQAAAAAAgBQAAAAABAAQAAAAABAAIADgDIACADQABACgEABIgBABQgBAAAAAAQAAgBgBAAQAAAAAAAAQgBAAAAgBg");
	this.shape_29.setTransform(17.4,10,4.189,4.189,5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#654815").s().p("AACARIgBgFQgBgDgBgBQgDABgCgCIABgFQAAgMACgHQAAAAABAAQAAAAAAAAQABABAAAAQAAAAABABIAAABIAAAAIAAACIAAAFIAAAHQAEAAACAHQACAHgCAEIgEgBg");
	this.shape_30.setTransform(25.2,7.8,4.189,4.189,5.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#B1833A").s().p("AgWAYQgCgDACgDQABgDADgBIADgBQABAAAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBIABgFQAAgGAGgCIAAAAIAEgMIABgGQABgEAAgBIAAAAIAAAAIABAAQAEAAACACIABAKIABAJIAAABQAEABABACQADAEgCAEIABABIAAABQAJACgBAGQgCAGgFgDIgHgIIgEABQgDAAgDgDQgEAFgDAAIgGAFIgFAFIgBAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAgBg");
	this.shape_31.setTransform(20.7,10.2,4.189,4.189,5.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CF9F59").s().p("AAAADQgBAEgDAHQAAgJgCABIgFAFQAAgDgBgCIgBgGIAAgGQAAgEADgDQADgDAEABQADACACAEIAGAIIAGADQgFABgFgDIAGAIIgDgBQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBAAAAABIgBADIgBAFIgCgNg");
	this.shape_32.setTransform(41.9,168.1,4.189,4.189,5.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CF9F59").s().p("AgBAIIgBgFIAAgJIABgGQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAAAIACABQAAAAABABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgBgFg");
	this.shape_33.setTransform(9.9,120.2,4.189,4.189,5.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CF9F59").s().p("AgGALQACgFADgCQACgCABgCIABgSQABACAAAGIADALQAAADgEAEIgDAFIAAAFQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQgCAAgCAEIABgIg");
	this.shape_34.setTransform(9,54.6,4.189,4.189,5.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#CF9F59").s().p("AABAaIgBgJIgBgHIgEAGIABgLIABgIQgBADgCgCQgBgBAAgEIABgJIAAgJIABgIIABABQAKAJACAXQABAEgDANIgBAIIABAJQgDgCgCgGg");
	this.shape_35.setTransform(15.9,91.1,4.189,4.189,5.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#654815").s().p("AAGCLIgCgHQAHAKAGgGQAHgHAAgIIgCgQQgDgMgHAGQAAgCACgEIACgFQAAgDgDgDQgDgDgDADQgFAGAAANQgCgNAAgOIAAgOIAAgHIAAgOQAAgUgKgVIgFgMIAKAJQADAAgCgHIgDgHIADAAQgEgNgOgJIAFAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAAAgBQABgEgHgKIgCgDIgEgKIgDgMIgBghQAAgJADgIQAGgNAEgBIgEASQgDAVACANIACARIAFAMQADAIABABIAJAFQAFAGAEAUQACAGgBAPQAAAEADAGIAFANQAJAUgCAcQgBANADAMQAFANgBALQgBAPgEAFQgEAFgNAGIgCgIg");
	this.shape_36.setTransform(26.6,78.1,4.189,4.189,5.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#B1833A").s().p("AAAB/QgBgGgCgSQgBgIgGgNQgGgOgCgGQgEgXACgWQADgOgBgEQAAgDgEgGQgEgFABgEQABgNADgIQABgCAEgDIAFgFQAEgHgDgJIgCgUIgBgSIABgTQABgPAEgEQABgBAAAAQABgBAAAAQABAAAAAAQABAAAAABQABACgBAEQgDAVAAAQQAAAIACAPQABAJADAQIAAAHQAAAGABAGIAFAJQAEAEABAEIAFASIAIAWQADAIgBAQIgDALQgBAGADALIAAAFIABAIQgBACgEgDQgFgEgCAEQgDAEAEAEQAFAHAAABQgGgDgEAGQgBACAAAKQAAAGACAHIADANQgKgDgEgPg");
	this.shape_37.setTransform(13.1,77.2,4.189,4.189,5.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#906829").s().p("AAbAgIgEgJQgEgGgIgFIgMgIIgIgFQgLgRgLgGIAYgNQABADgEAIQgFAJAKAMQAFAFAGAEIAIAFQAFADACADQAGAIACAEQAEAJgBACQgCAAgDgGg");
	this.shape_38.setTransform(28.4,150,4.189,4.189,5.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#906829").s().p("AgICTQgJgJgCgUQgDgVgGgMQgQgfAIgvQABgEgCgDIgEgFIgCgHQAAgEACgLQABgIAEgFIAEgDIADgEQABgFgBgKIgBgfIgBgYQAJgnAIAGQAEACACALIgBATQgBAaABAFIADAPQAGASAFAEQAGADADAEIAFALQAFALgBAHIgBASQACAIAIANQAFAMABAOQABAGgBAQQgCAMAEAJQAFAOgCASQgCAUgNACIgMgBQgEAAgEACIgGADIgCAAQgDAAgFgFg");
	this.shape_39.setTransform(20.8,76,4.189,4.189,5.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#554718").s().p("AgJAOIAAgHIgCgLQACgIAEgEQAFgEAEAEIgBgDQABAFAHABQABABAAAFQAAAGgFADIgFAEIgCAEIgGAKQgDgBAAgFg");
	this.shape_40.setTransform(13.1,22.3,4.187,4.187,-18.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#654815").s().p("AAAAEIgCgDQgBgBAAAAQAAAAgBAAQAAAAAAAAQAAgBAAAAIACgDQgBAEAIAAQABAAgCADQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAgBAAIgBgBg");
	this.shape_41.setTransform(20.1,21,4.187,4.187,-18.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#654815").s().p("AgFADQAAgDAEAAQADgDACABIACAAIgGADQgBACgCAAIgCAAg");
	this.shape_42.setTransform(5.3,24.7,4.187,4.187,-18.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#654815").s().p("AgDABQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAABgBIADgBIABACQABACgFABIAAAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBg");
	this.shape_43.setTransform(7.6,15.1,4.187,4.187,-18.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#654815").s().p("AABARIgBgFQAAgDgBgCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAAAAAgBIAAgCIABgDIADgIIABgKQAAAAAAAAQAAAAABAAQAAAAAAABQAAAAABABIAAgBIgBAQQAFABABAHQAAAHgCADIgEgBg");
	this.shape_44.setTransform(14.1,11.1,4.187,4.187,-18.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#B1833A").s().p("AARAXIgGgIIgEAAQgEgBgDgDQgDAFgDgCIgIAFIgFAEQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAgBgBgBQgBgDACgDQABgDAEAAQAEAAABgCIACgFQAAgEAHgCIABgBIADgMQAEgIACgBIABAAQAFABAAACIAAAUQADABABACQADAFgDADIABACIAAABQAIAEgDAFQAAABAAAAQgBABAAAAQgBABAAAAQAAAAgBAAIgDgCg");
	this.shape_45.setTransform(9.6,14.6,4.187,4.187,-18.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#CF9F59").s().p("AAFAEIgDAFIgBAGIgBgEQAAAAAAgBQAAAAAAAAQAAAAAAAAQAAAAAAAAQgFADgDAAIAIgLQgFACgGAAQAIgEgCAAIgIgBIAFgDIAFgFIADgEQAEgCAEABQAEABABAEIAAAJIgEAIIgBAHQgDgDAAgIg");
	this.shape_46.setTransform(24,170.3,4.187,4.187,-18.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#CF9F59").s().p("AgFAIIAFgOIACgFIACADIACABIgBADQgBADgEABQgBAEgEAIIAAgEg");
	this.shape_47.setTransform(11.4,120,4.187,4.187,-18.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#CF9F59").s().p("AgFAKQgBAAgEADQAFgJADgCIAFgCQAFAAAAgDIADgLQAAAOgCACIgCAEIgEABQgCABgBACIgDAFQAAgBAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAg");
	this.shape_48.setTransform(6.9,56,4.187,4.187,-18.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#CF9F59").s().p("AgGAQQACgGgBgCIgFAEIAFgJIAEgHQgCAEgBgFIABgFIADgIIADgIIAEgHIABABQAGAMgGAWIgIARIgGAPQgBgDABgPg");
	this.shape_49.setTransform(16.2,91.1,4.187,4.187,-18.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#654815").s().p("AgiCBIACgHQACALAIgDQAJgCADgJQAKgbgNACIAEgEIAEgFQABgDgBgDQgCgDgDABQgGACgGANQABgOAEgMIAJgUIAHgLQAFgUgCgXIABgOIAFANQAEABAAgHIAAgIQAAAAAAABQABAAAAAAQAAAAABAAQAAAAAAAAQACgMgKgQIAFADQABAAAAABQABAAAAAAQABAAAAgBQABAAAAgBQADgEgEgKIgBgLQgBgGAEgRQACgKAEgMQAGgOAHgDQABADgDAGIgEAKQgDAFgDAOQgBAHAAAPQAAAOABACIAGAIQAEAIgDAUQgBAIgHANQgBADAAAUQAAAYgMAZQgGAMgCALQgBANgCALQgIAOgFADQgEACgPAAIAAgHg");
	this.shape_50.setTransform(21.9,80.2,4.187,4.187,-18.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#B1833A").s().p("AgqBwQABgGAFgSQACgHAAgOQgBgQABgGQAFgZAJgSQAJgJABgGIAAgKQgCgGACgEQAHgMADgGQACgCAHgCQAGgBABgEIACgIIAAgIIAFgWQAKgnAKADQABAFgHAOIgGATQgDAOgEAbIgEAUIACAKIAAAJIgDATIAAAXQgBAKgHANQgBADgFAGQgCADgBAFIgCAJIAAAFIgBADIgBAEQgCACgDgFQgCgFgEACQgEADABAGIACAJQgEgFgGAEIgGAKIgFAbQgIgHABgQg");
	this.shape_51.setTransform(11.5,80,4.187,4.187,-18.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#906829").s().p("AgGAWQAFgGABgMQABgHgBgNIgBgIQgBgDgGgGIAZgDQAAACgDADIgEAEQgEADgBAFIAAANQAAAGgBAEQgDAOgCAGQgFAFgKAIQAAgDAKgMg");
	this.shape_52.setTransform(26.3,150.4,4.187,4.187,-18.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#906829").s().p("AgMCSIgLgGQgEgBgEAAIgHABQgGgBgDgJQgFgMAFgTQAGgUgBgNQgDgiAZgoQADgDgBgEIgBgIIABgHIAHgNQAFgHAFgDIACgBQABAAABAAQAAgBABAAQAAAAABAAQAAgBABAAQACgBACgKIAGgjIAJgXQAMgcAJAKQAJAKgJAUQgEAGgCAQQgEANAAAKQgDAPAEAEIAGAHQADAFAAAOQABAKgEAIIgFAHQgDAFAAAEQgCAIADAPQAAANgEAOQgCAGgHANQgGALAAAJQAAAPgJARQgIAOgIAAIgEAAg");
	this.shape_53.setTransform(14.5,78.5,4.187,4.187,-18.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:0,x:20,y:150.1}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:0,x:9.8,y:171.3}},{t:this.shape_5,p:{rotation:0,x:26.3,y:11.1}},{t:this.shape_4,p:{rotation:0,x:33.8,y:9.5}},{t:this.shape_3,p:{rotation:0,x:26.3,y:8.9}},{t:this.shape_2,p:{rotation:0,x:19,y:14.9}},{t:this.shape_1,p:{rotation:0,x:33.2,y:20.6}},{t:this.shape,p:{rotation:0,x:26.9,y:17.3}}]}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:-29,x:24.4,y:148.5}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:-14.7,x:25.3,y:169.8}},{t:this.shape_5,p:{rotation:-20.2,x:22.1,y:11.6}},{t:this.shape_4,p:{rotation:-20.2,x:28.6,y:7.5}},{t:this.shape_3,p:{rotation:-20.2,x:21.3,y:9.5}},{t:this.shape_2,p:{rotation:-20.2,x:16.5,y:17.7}},{t:this.shape_1,p:{rotation:-20.2,x:31.8,y:18.2}},{t:this.shape,p:{rotation:-20.2,x:24.8,y:17.2}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:28.4,y:150}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:41.9,y:168.1}},{t:this.shape_31,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.7,y:10.2}},{t:this.shape_30,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:25.2,y:7.8}},{t:this.shape_29,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:17.4,y:10}},{t:this.shape_28,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:12.6,y:18.7}},{t:this.shape_27,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:27.7,y:19}},{t:this.shape_26,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.3}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.188,scaleY:4.188,rotation:27.7,x:24.8,y:149.7}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:33.5,y:168.8}},{t:this.shape_31,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.2,y:10.1}},{t:this.shape_30,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:23.5,y:7.3}},{t:this.shape_29,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:15.9,y:10.3}},{t:this.shape_28,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:12.1,y:19.5}},{t:this.shape_27,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:27.1,y:18.2}},{t:this.shape_26,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.7}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:26.3,y:150.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:24}},{t:this.shape_45,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:9.6,y:14.6}},{t:this.shape_44,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:14.1,y:11.1}},{t:this.shape_43,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:7.6,y:15.1}},{t:this.shape_42,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:5.3,y:24.7}},{t:this.shape_41,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:20.1,y:21}},{t:this.shape_40,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:13.1}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.186,scaleY:4.186,rotation:-1.7,x:23.6,y:151.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.186,scaleY:4.186,rotation:-0.7,x:15.8}},{t:this.shape_45,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:14.6,y:13.8}},{t:this.shape_44,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:20.1,y:12.2}},{t:this.shape_43,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:12.6,y:13.6}},{t:this.shape_42,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:7.1,y:21.8}},{t:this.shape_41,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:22.2,y:23.5}},{t:this.shape_40,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:15.3}}]},6).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.7,178.2);


(lib.camelanim2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#554718").s().p("AgNALIABgGQAAgGACgFQAEgHAGgDQAEgCAEAEIAAgCQAAABAAABQABAAAAABQAAAAAAABQABAAAAABQABAAABAAQAAABABAAQAAABAAAAQABAAAAABQABACgCAFQgBADgHAEIgFABIgEAEQgFAGgDABQgCgCABgFg");
	this.shape.setTransform(26.9,17.3,4.188,4.188);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#654815").s().p("AAAAEIgDgEQAAAAAAAAQAAAAAAgBQgBAAABgBQAAAAAAAAIACgCQgCAEAHAAQABAAgBABQAAABAAAAQAAABgBAAQAAABgBAAIgCABIAAgBg");
	this.shape_1.setTransform(33.2,20.6,4.188,4.188);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#654815").s().p("AgFABQgBgBAGgBQADgBACABIABABQgGACgCAAIgDgBg");
	this.shape_2.setTransform(19,14.9,4.188,4.188);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#654815").s().p("AAAACQgDAAAAgCQAAgCADAAIAEAAIAAADQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAgBAAIgBgBg");
	this.shape_3.setTransform(26.3,8.9,4.188,4.188);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#654815").s().p("AgCAQIABgFQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIACgBQAEgMAFgFQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAIABAAIgHAPQAEABgBAIQgCAHgCACIgCgCg");
	this.shape_4.setTransform(33.8,9.5,4.188,4.188);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B1833A").s().p("AAOAXIgDgKIgEgBQgDgBgCgEQgEACgDgCIgJACIgGACQgDABAAgEQAAgDACgDQADgCADABIADABQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIADgDQACgFAGgBIAAAAIAJgJIADgFQADgDACAAIAAAAIABAAQAEACAAADQABABgEAIIgDAJIAAABQADABAAADQAAAFgDADIAAABIgBABIAAAAIAAAAIAFAFQABADgCADQgBAAAAABQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAgBAAQAAgBAAgBg");
	this.shape_5.setTransform(26.3,11.1,4.188,4.188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CF9F59").s().p("AAAADQgCAEgGADQAFgHgCgCIgHACIADgDIADgGIAEgFQACgDACgBQAFAAACADQACACABAHIgBAIIACAIQgEgCgCgIIgBAGIAAAGIgCgDQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAAAAAABQgCAEgDABIADgNg");
	this.shape_6.setTransform(9.8,171.3,4.188,4.188);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CF9F59").s().p("AgCADIAAgJIABgGIABACIACABQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgCgKg");
	this.shape_7.setTransform(11.8,119.9,4.188,4.188);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CF9F59").s().p("AgGAEQABgEACAAIAFgGIADgCIACgCIABAGQgBAEgCAAQgEACAAADIgBAFQAAAAAAAAQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAAAAAQAAAAgBAAQgCAAgCAEIABgHg");
	this.shape_8.setTransform(7.1,57.8,4.188,4.188);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CF9F59").s().p("AAAASQAAgHgCAAIgEAEQgBgCADgIIACgIQgBABAAABQAAAAgBAAQAAAAgBAAQAAAAgBgBIgBgFIACgJIAAgJQACgFAAgDIABABQAJAKABAWQAAAGgDAMIgBAHIAAAJQgDgCgBgOg");
	this.shape_9.setTransform(16.3,90.5,4.188,4.188);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#654815").s().p("AgBCJIgBgIQAEAKAHgFQAHgFAAgJQAAgegLAHIACgGIACgFQABgEgDgCQgDgDgDADQgCAEgCAOQgDgKAAgQIABgPIADgUQABgVgLgVQgCgJgCgEIAJAKQAEAAgCgGIgDgIIADABQgDgNgOgLIAFABQABAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgDgCgEIgEgHIAAgBQgCgFAAgKQAAgZAHgPQANgeAJAEIgFAHIgEAJQgJANgEAVQgCAKACALQABAGADAFQACAEAGACQAGAHADATQABAIgCAOQAAAFADAGIAEAMQAIAVgDAcQgCAOADALQAEAMgCAMIgBAKQgCAGgDAEQgDAEgNAFIgBgHg");
	this.shape_10.setTransform(26.9,78.2,4.188,4.188);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B1833A").s().p("AgGCBIgCgYQgBgIgFgNQgGgPgBgGQgDgYACgVQAFgOgBgEIgEgIQgDgGAAgEQACgNAEgIQABgDAGgEQAGgDAAgEIACgSQAEgbACgPQAIgWAOgMIACAAQAAAFgEAHIgHALQgJASgEAZIgBAWIABAPIABANIAEAJIAEAJIAEASIAHAWQADAKgCAOIgDAMQgBAFACAMIAAAFIAAAHQgBADgEgEQgEgDgDADQgDAEADAFIAFAHQgFgDgFAGQgCADAAAIQAAAHAFAUQgJgDgEgQg");
	this.shape_11.setTransform(12.9,75.8,4.188,4.188);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#906829").s().p("AgUApQAAgGAGgMQACgEAHgHQAFgIADgEQAGgGgBgHIgBgHQgCgEgHgEIAWgMQACAEgGAIIgBAVQgCAGgGAFIgJAMIgJALQgHAJgBAFg");
	this.shape_12.setTransform(20,150.1,4.188,4.188);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#906829").s().p("AgNCQQgIgKgBgUQgBgTgGgOQgOggAKgvQAAgCgBgDIgEgHQgBgCAAgFQAAgLAEgGQAFgKAEgCIAEAAQAEgBAAgIIAAgWIADgSIACgFQAFgXAIgPQAIgLAFAAQAGABgBAMQgCAIgGAOQgGAOgCAHQgCAMADAPIADALQACADAHACQADAEAGANQAEAMgBAGIgCAKIAAAIQABAHAIAOQADALACAQQAAAGgCAPQgCAMADAJQAEAOgCATQgEATgNABIgMgBQgDgBgEACIgFADIgCABQgEAAgGgGg");
	this.shape_13.setTransform(19.3,76.3,4.188,4.188);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQAAAAAAgBQABgBAAAAQABAAAAgBQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_14.setTransform(38.5,120.7,4.188,4.188);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#906829").s().p("AgHAIQgEgFACgGQACgKAFAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_15.setTransform(37.7,119.6,4.188,4.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACACAAAHQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_16.setTransform(37.2,117.5,4.188,4.188);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQABgDADAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_17.setTransform(32.5,76.2,4.188,4.188);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#906829").s().p("AgHAIQgEgFACgGQADgKAEAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_18.setTransform(31.8,75.1,4.188,4.188);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACABAAAIQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_19.setTransform(31.3,73,4.188,4.188);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#654815").s().p("AgEAKQAEACgFgMQgCgDAGgBQgBgBAAgBQAAAAAAgBQABgBAAAAQAAAAABgBQAAAAAAAAQAAgBAAABQABAAAAAAQAAABABABIgBgEIAFAJIABAHQgBAFgFACIgBAAQgDAAgBgCg");
	this.shape_20.setTransform(10.4,113.4,4.188,4.188);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#906829").s().p("AAAANQgGgDgDgHQgEgHAHAAIAAgCIABgCIAEABQAAgHAEACQADABADAGQADAIgBAEQgDAGgGAAIgCAAg");
	this.shape_21.setTransform(8.4,113.1,4.188,4.188);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#B1833A").s().p("AAAANQgHgCgDgHQgCgIAFAAIAAgCIABgCIAEABQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQABgDACAAQABABAFAGQAEAGgDAHQgDAHgFAAIgCgBg");
	this.shape_22.setTransform(6.9,111.7,4.188,4.188);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#654815").s().p("AgFAKQADABgBgFIgCgGQgCgDAFgBQAAgBAAgBQAAAAAAgBQABgBAAAAQABAAAAgBQAAAAAAAAQAAgBAAABQABAAAAAAQABABAAABIgBgEIAEAJIACAHQgBAFgFACIgBAAQgDAAgCgCg");
	this.shape_23.setTransform(4,62,4.188,4.188);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#906829").s().p("AAAANQgGgDgCgHQgEgHAGAAIACgEQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABABQAAgHAEACQAEACACAFQADAJgBADQgDAGgFAAIgDAAg");
	this.shape_24.setTransform(2,61.7,4.188,4.188);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#B1833A").s().p("AAAANQgHgCgCgHQgEgIAGAAIACgEQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAIADgDQACABAEAGQAFAGgEAHQgDAHgFAAIgCgBg");
	this.shape_25.setTransform(0.4,60.3,4.188,4.188);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#554718").s().p("AgGAQIgBgIQgEgHAAgDQABgIAEgFQAEgFADADIAAgCQAAAAAAABQABAAAAAAQABABAAAAQABAAABAAIAEABQACABABAGQABAGgFADIgEAFIgDAFQgBAIgCACQgDgBgBgDg");
	this.shape_26.setTransform(20.3,18.6,4.189,4.189,5.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#654815").s().p("AABAEIgCgDQgBAAgBgBQAAAAgBAAQAAAAAAAAQAAAAAAAAIABgDQABADAHgBQABABgBACQgBAAAAABQAAAAgBABQAAAAAAAAQgBAAAAAAIgBAAg");
	this.shape_27.setTransform(27.7,19,4.189,4.189,5.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#654815").s().p("AgEAEQgCgDAFgBQACgDACAAIACAAIgFADQgBAEgCAAIgBAAg");
	this.shape_28.setTransform(12.6,18.7,4.189,4.189,5.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#654815").s().p("AgDACQAAAAAAgBQAAAAAAgBQAAAAABAAQAAAAABAAIADgDIACADQABACgEABIgBABQgBAAAAAAQAAgBgBAAQAAAAAAAAQgBAAAAgBg");
	this.shape_29.setTransform(17.4,10,4.189,4.189,5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#654815").s().p("AACARIgBgFQgBgDgBgBQgDABgCgCIABgFQAAgMACgHQAAAAABAAQAAAAAAAAQABABAAAAQAAAAABABIAAABIAAAAIAAACIAAAFIAAAHQAEAAACAHQACAHgCAEIgEgBg");
	this.shape_30.setTransform(25.2,7.8,4.189,4.189,5.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#B1833A").s().p("AgWAYQgCgDACgDQABgDADgBIADgBQABAAAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBIABgFQAAgGAGgCIAAAAIAEgMIABgGQABgEAAgBIAAAAIAAAAIABAAQAEAAACACIABAKIABAJIAAABQAEABABACQADAEgCAEIABABIAAABQAJACgBAGQgCAGgFgDIgHgIIgEABQgDAAgDgDQgEAFgDAAIgGAFIgFAFIgBAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAgBg");
	this.shape_31.setTransform(20.7,10.2,4.189,4.189,5.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CF9F59").s().p("AAAADQgBAEgDAHQAAgJgCABIgFAFQAAgDgBgCIgBgGIAAgGQAAgEADgDQADgDAEABQADACACAEIAGAIIAGADQgFABgFgDIAGAIIgDgBQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBAAAAABIgBADIgBAFIgCgNg");
	this.shape_32.setTransform(41.9,168.1,4.189,4.189,5.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CF9F59").s().p("AgBAIIgBgFIAAgJIABgGQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAAAIACABQAAAAABABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgBgFg");
	this.shape_33.setTransform(9.9,120.2,4.189,4.189,5.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CF9F59").s().p("AgGALQACgFADgCQACgCABgCIABgSQABACAAAGIADALQAAADgEAEIgDAFIAAAFQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQgCAAgCAEIABgIg");
	this.shape_34.setTransform(9,54.6,4.189,4.189,5.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#CF9F59").s().p("AABAaIgBgJIgBgHIgEAGIABgLIABgIQgBADgCgCQgBgBAAgEIABgJIAAgJIABgIIABABQAKAJACAXQABAEgDANIgBAIIABAJQgDgCgCgGg");
	this.shape_35.setTransform(15.9,91.1,4.189,4.189,5.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#654815").s().p("AAGCLIgCgHQAHAKAGgGQAHgHAAgIIgCgQQgDgMgHAGQAAgCACgEIACgFQAAgDgDgDQgDgDgDADQgFAGAAANQgCgNAAgOIAAgOIAAgHIAAgOQAAgUgKgVIgFgMIAKAJQADAAgCgHIgDgHIADAAQgEgNgOgJIAFAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAAAgBQABgEgHgKIgCgDIgEgKIgDgMIgBghQAAgJADgIQAGgNAEgBIgEASQgDAVACANIACARIAFAMQADAIABABIAJAFQAFAGAEAUQACAGgBAPQAAAEADAGIAFANQAJAUgCAcQgBANADAMQAFANgBALQgBAPgEAFQgEAFgNAGIgCgIg");
	this.shape_36.setTransform(26.6,78.1,4.189,4.189,5.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#B1833A").s().p("AAAB/QgBgGgCgSQgBgIgGgNQgGgOgCgGQgEgXACgWQADgOgBgEQAAgDgEgGQgEgFABgEQABgNADgIQABgCAEgDIAFgFQAEgHgDgJIgCgUIgBgSIABgTQABgPAEgEQABgBAAAAQABgBAAAAQABAAAAAAQABAAAAABQABACgBAEQgDAVAAAQQAAAIACAPQABAJADAQIAAAHQAAAGABAGIAFAJQAEAEABAEIAFASIAIAWQADAIgBAQIgDALQgBAGADALIAAAFIABAIQgBACgEgDQgFgEgCAEQgDAEAEAEQAFAHAAABQgGgDgEAGQgBACAAAKQAAAGACAHIADANQgKgDgEgPg");
	this.shape_37.setTransform(13.1,77.2,4.189,4.189,5.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#906829").s().p("AAbAgIgEgJQgEgGgIgFIgMgIIgIgFQgLgRgLgGIAYgNQABADgEAIQgFAJAKAMQAFAFAGAEIAIAFQAFADACADQAGAIACAEQAEAJgBACQgCAAgDgGg");
	this.shape_38.setTransform(28.4,150,4.189,4.189,5.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#906829").s().p("AgICTQgJgJgCgUQgDgVgGgMQgQgfAIgvQABgEgCgDIgEgFIgCgHQAAgEACgLQABgIAEgFIAEgDIADgEQABgFgBgKIgBgfIgBgYQAJgnAIAGQAEACACALIgBATQgBAaABAFIADAPQAGASAFAEQAGADADAEIAFALQAFALgBAHIgBASQACAIAIANQAFAMABAOQABAGgBAQQgCAMAEAJQAFAOgCASQgCAUgNACIgMgBQgEAAgEACIgGADIgCAAQgDAAgFgFg");
	this.shape_39.setTransform(20.8,76,4.189,4.189,5.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#554718").s().p("AgJAOIAAgHIgCgLQACgIAEgEQAFgEAEAEIgBgDQABAFAHABQABABAAAFQAAAGgFADIgFAEIgCAEIgGAKQgDgBAAgFg");
	this.shape_40.setTransform(13.1,22.3,4.187,4.187,-18.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#654815").s().p("AAAAEIgCgDQgBgBAAAAQAAAAgBAAQAAAAAAAAQAAgBAAAAIACgDQgBAEAIAAQABAAgCADQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAgBAAIgBgBg");
	this.shape_41.setTransform(20.1,21,4.187,4.187,-18.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#654815").s().p("AgFADQAAgDAEAAQADgDACABIACAAIgGADQgBACgCAAIgCAAg");
	this.shape_42.setTransform(5.3,24.7,4.187,4.187,-18.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#654815").s().p("AgDABQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAABgBIADgBIABACQABACgFABIAAAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBg");
	this.shape_43.setTransform(7.6,15.1,4.187,4.187,-18.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#654815").s().p("AABARIgBgFQAAgDgBgCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAAAAAgBIAAgCIABgDIADgIIABgKQAAAAAAAAQAAAAABAAQAAAAAAABQAAAAABABIAAgBIgBAQQAFABABAHQAAAHgCADIgEgBg");
	this.shape_44.setTransform(14.1,11.1,4.187,4.187,-18.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#B1833A").s().p("AARAXIgGgIIgEAAQgEgBgDgDQgDAFgDgCIgIAFIgFAEQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAgBgBgBQgBgDACgDQABgDAEAAQAEAAABgCIACgFQAAgEAHgCIABgBIADgMQAEgIACgBIABAAQAFABAAACIAAAUQADABABACQADAFgDADIABACIAAABQAIAEgDAFQAAABAAAAQgBABAAAAQgBABAAAAQAAAAgBAAIgDgCg");
	this.shape_45.setTransform(9.6,14.6,4.187,4.187,-18.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#CF9F59").s().p("AAFAEIgDAFIgBAGIgBgEQAAAAAAgBQAAAAAAAAQAAAAAAAAQAAAAAAAAQgFADgDAAIAIgLQgFACgGAAQAIgEgCAAIgIgBIAFgDIAFgFIADgEQAEgCAEABQAEABABAEIAAAJIgEAIIgBAHQgDgDAAgIg");
	this.shape_46.setTransform(24,170.3,4.187,4.187,-18.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#CF9F59").s().p("AgFAIIAFgOIACgFIACADIACABIgBADQgBADgEABQgBAEgEAIIAAgEg");
	this.shape_47.setTransform(11.4,120,4.187,4.187,-18.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#CF9F59").s().p("AgFAKQgBAAgEADQAFgJADgCIAFgCQAFAAAAgDIADgLQAAAOgCACIgCAEIgEABQgCABgBACIgDAFQAAgBAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAg");
	this.shape_48.setTransform(6.9,56,4.187,4.187,-18.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#CF9F59").s().p("AgGAQQACgGgBgCIgFAEIAFgJIAEgHQgCAEgBgFIABgFIADgIIADgIIAEgHIABABQAGAMgGAWIgIARIgGAPQgBgDABgPg");
	this.shape_49.setTransform(16.2,91.1,4.187,4.187,-18.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#654815").s().p("AgiCBIACgHQACALAIgDQAJgCADgJQAKgbgNACIAEgEIAEgFQABgDgBgDQgCgDgDABQgGACgGANQABgOAEgMIAJgUIAHgLQAFgUgCgXIABgOIAFANQAEABAAgHIAAgIQAAAAAAABQABAAAAAAQAAAAABAAQAAAAAAAAQACgMgKgQIAFADQABAAAAABQABAAAAAAQABAAAAgBQABAAAAgBQADgEgEgKIgBgLQgBgGAEgRQACgKAEgMQAGgOAHgDQABADgDAGIgEAKQgDAFgDAOQgBAHAAAPQAAAOABACIAGAIQAEAIgDAUQgBAIgHANQgBADAAAUQAAAYgMAZQgGAMgCALQgBANgCALQgIAOgFADQgEACgPAAIAAgHg");
	this.shape_50.setTransform(21.9,80.2,4.187,4.187,-18.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#B1833A").s().p("AgqBwQABgGAFgSQACgHAAgOQgBgQABgGQAFgZAJgSQAJgJABgGIAAgKQgCgGACgEQAHgMADgGQACgCAHgCQAGgBABgEIACgIIAAgIIAFgWQAKgnAKADQABAFgHAOIgGATQgDAOgEAbIgEAUIACAKIAAAJIgDATIAAAXQgBAKgHANQgBADgFAGQgCADgBAFIgCAJIAAAFIgBADIgBAEQgCACgDgFQgCgFgEACQgEADABAGIACAJQgEgFgGAEIgGAKIgFAbQgIgHABgQg");
	this.shape_51.setTransform(11.5,80,4.187,4.187,-18.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#906829").s().p("AgGAWQAFgGABgMQABgHgBgNIgBgIQgBgDgGgGIAZgDQAAACgDADIgEAEQgEADgBAFIAAANQAAAGgBAEQgDAOgCAGQgFAFgKAIQAAgDAKgMg");
	this.shape_52.setTransform(26.3,150.4,4.187,4.187,-18.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#906829").s().p("AgMCSIgLgGQgEgBgEAAIgHABQgGgBgDgJQgFgMAFgTQAGgUgBgNQgDgiAZgoQADgDgBgEIgBgIIABgHIAHgNQAFgHAFgDIACgBQABAAABAAQAAgBABAAQAAAAABAAQAAgBABAAQACgBACgKIAGgjIAJgXQAMgcAJAKQAJAKgJAUQgEAGgCAQQgEANAAAKQgDAPAEAEIAGAHQADAFAAAOQABAKgEAIIgFAHQgDAFAAAEQgCAIADAPQAAANgEAOQgCAGgHANQgGALAAAJQAAAPgJARQgIAOgIAAIgEAAg");
	this.shape_53.setTransform(14.5,78.5,4.187,4.187,-18.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:0,x:20,y:150.1}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:0,x:9.8,y:171.3}},{t:this.shape_5,p:{rotation:0,x:26.3,y:11.1}},{t:this.shape_4,p:{rotation:0,x:33.8,y:9.5}},{t:this.shape_3,p:{rotation:0,x:26.3,y:8.9}},{t:this.shape_2,p:{rotation:0,x:19,y:14.9}},{t:this.shape_1,p:{rotation:0,x:33.2,y:20.6}},{t:this.shape,p:{rotation:0,x:26.9,y:17.3}}]}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:-29,x:24.4,y:148.5}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:-14.7,x:25.3,y:169.8}},{t:this.shape_5,p:{rotation:-20.2,x:22.1,y:11.6}},{t:this.shape_4,p:{rotation:-20.2,x:28.6,y:7.5}},{t:this.shape_3,p:{rotation:-20.2,x:21.3,y:9.5}},{t:this.shape_2,p:{rotation:-20.2,x:16.5,y:17.7}},{t:this.shape_1,p:{rotation:-20.2,x:31.8,y:18.2}},{t:this.shape,p:{rotation:-20.2,x:24.8,y:17.2}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:28.4,y:150}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:41.9,y:168.1}},{t:this.shape_31,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.7,y:10.2}},{t:this.shape_30,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:25.2,y:7.8}},{t:this.shape_29,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:17.4,y:10}},{t:this.shape_28,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:12.6,y:18.7}},{t:this.shape_27,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:27.7,y:19}},{t:this.shape_26,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.3}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.188,scaleY:4.188,rotation:27.7,x:24.8,y:149.7}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:33.5,y:168.8}},{t:this.shape_31,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.2,y:10.1}},{t:this.shape_30,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:23.5,y:7.3}},{t:this.shape_29,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:15.9,y:10.3}},{t:this.shape_28,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:12.1,y:19.5}},{t:this.shape_27,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:27.1,y:18.2}},{t:this.shape_26,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.7}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:26.3,y:150.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:24}},{t:this.shape_45,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:9.6,y:14.6}},{t:this.shape_44,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:14.1,y:11.1}},{t:this.shape_43,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:7.6,y:15.1}},{t:this.shape_42,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:5.3,y:24.7}},{t:this.shape_41,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:20.1,y:21}},{t:this.shape_40,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:13.1}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.186,scaleY:4.186,rotation:-1.7,x:23.6,y:151.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.186,scaleY:4.186,rotation:-0.7,x:15.8}},{t:this.shape_45,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:14.6,y:13.8}},{t:this.shape_44,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:20.1,y:12.2}},{t:this.shape_43,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:12.6,y:13.6}},{t:this.shape_42,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:7.1,y:21.8}},{t:this.shape_41,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:22.2,y:23.5}},{t:this.shape_40,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:15.3}}]},6).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.7,0,47.3,178.2);


(lib.camelanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#554718").s().p("AgNALIABgGQAAgGACgFQAEgHAGgDQAEgCAEAEIAAgCQAAABAAABQABAAAAABQAAAAAAABQABAAAAABQABAAABAAQAAABABAAQAAABAAAAQABAAAAABQABACgCAFQgBADgHAEIgFABIgEAEQgFAGgDABQgCgCABgFg");
	this.shape.setTransform(26.9,17.3,4.188,4.188);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#654815").s().p("AAAAEIgDgEQAAAAAAAAQAAAAAAgBQgBAAABgBQAAAAAAAAIACgCQgCAEAHAAQABAAgBABQAAABAAAAQAAABgBAAQAAABgBAAIgCABIAAgBg");
	this.shape_1.setTransform(33.2,20.6,4.188,4.188);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#654815").s().p("AgFABQgBgBAGgBQADgBACABIABABQgGACgCAAIgDgBg");
	this.shape_2.setTransform(19,14.9,4.188,4.188);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#654815").s().p("AAAACQgDAAAAgCQAAgCADAAIAEAAIAAADQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAgBAAIgBgBg");
	this.shape_3.setTransform(26.3,8.9,4.188,4.188);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#654815").s().p("AgCAQIABgFQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIACgBQAEgMAFgFQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAIABAAIgHAPQAEABgBAIQgCAHgCACIgCgCg");
	this.shape_4.setTransform(33.8,9.5,4.188,4.188);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B1833A").s().p("AAOAXIgDgKIgEgBQgDgBgCgEQgEACgDgCIgJACIgGACQgDABAAgEQAAgDACgDQADgCADABIADABQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIADgDQACgFAGgBIAAAAIAJgJIADgFQADgDACAAIAAAAIABAAQAEACAAADQABABgEAIIgDAJIAAABQADABAAADQAAAFgDADIAAABIgBABIAAAAIAAAAIAFAFQABADgCADQgBAAAAABQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAgBAAQAAgBAAgBg");
	this.shape_5.setTransform(26.3,11.1,4.188,4.188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CF9F59").s().p("AAAADQgCAEgGADQAFgHgCgCIgHACIADgDIADgGIAEgFQACgDACgBQAFAAACADQACACABAHIgBAIIACAIQgEgCgCgIIgBAGIAAAGIgCgDQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAAAAAABQgCAEgDABIADgNg");
	this.shape_6.setTransform(9.8,171.3,4.188,4.188);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CF9F59").s().p("AgCADIAAgJIABgGIABACIACABQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgCgKg");
	this.shape_7.setTransform(11.8,119.9,4.188,4.188);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CF9F59").s().p("AgGAEQABgEACAAIAFgGIADgCIACgCIABAGQgBAEgCAAQgEACAAADIgBAFQAAAAAAAAQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAAAAAQAAAAgBAAQgCAAgCAEIABgHg");
	this.shape_8.setTransform(7.1,57.8,4.188,4.188);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CF9F59").s().p("AAAASQAAgHgCAAIgEAEQgBgCADgIIACgIQgBABAAABQAAAAgBAAQAAAAgBAAQAAAAgBgBIgBgFIACgJIAAgJQACgFAAgDIABABQAJAKABAWQAAAGgDAMIgBAHIAAAJQgDgCgBgOg");
	this.shape_9.setTransform(16.3,90.5,4.188,4.188);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#654815").s().p("AgBCJIgBgIQAEAKAHgFQAHgFAAgJQAAgegLAHIACgGIACgFQABgEgDgCQgDgDgDADQgCAEgCAOQgDgKAAgQIABgPIADgUQABgVgLgVQgCgJgCgEIAJAKQAEAAgCgGIgDgIIADABQgDgNgOgLIAFABQABAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgDgCgEIgEgHIAAgBQgCgFAAgKQAAgZAHgPQANgeAJAEIgFAHIgEAJQgJANgEAVQgCAKACALQABAGADAFQACAEAGACQAGAHADATQABAIgCAOQAAAFADAGIAEAMQAIAVgDAcQgCAOADALQAEAMgCAMIgBAKQgCAGgDAEQgDAEgNAFIgBgHg");
	this.shape_10.setTransform(26.9,78.2,4.188,4.188);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B1833A").s().p("AgGCBIgCgYQgBgIgFgNQgGgPgBgGQgDgYACgVQAFgOgBgEIgEgIQgDgGAAgEQACgNAEgIQABgDAGgEQAGgDAAgEIACgSQAEgbACgPQAIgWAOgMIACAAQAAAFgEAHIgHALQgJASgEAZIgBAWIABAPIABANIAEAJIAEAJIAEASIAHAWQADAKgCAOIgDAMQgBAFACAMIAAAFIAAAHQgBADgEgEQgEgDgDADQgDAEADAFIAFAHQgFgDgFAGQgCADAAAIQAAAHAFAUQgJgDgEgQg");
	this.shape_11.setTransform(12.9,75.8,4.188,4.188);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#906829").s().p("AgUApQAAgGAGgMQACgEAHgHQAFgIADgEQAGgGgBgHIgBgHQgCgEgHgEIAWgMQACAEgGAIIgBAVQgCAGgGAFIgJAMIgJALQgHAJgBAFg");
	this.shape_12.setTransform(20,150.1,4.188,4.188);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#906829").s().p("AgNCQQgIgKgBgUQgBgTgGgOQgOggAKgvQAAgCgBgDIgEgHQgBgCAAgFQAAgLAEgGQAFgKAEgCIAEAAQAEgBAAgIIAAgWIADgSIACgFQAFgXAIgPQAIgLAFAAQAGABgBAMQgCAIgGAOQgGAOgCAHQgCAMADAPIADALQACADAHACQADAEAGANQAEAMgBAGIgCAKIAAAIQABAHAIAOQADALACAQQAAAGgCAPQgCAMADAJQAEAOgCATQgEATgNABIgMgBQgDgBgEACIgFADIgCABQgEAAgGgGg");
	this.shape_13.setTransform(19.3,76.3,4.188,4.188);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQAAAAAAgBQABgBAAAAQABAAAAgBQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_14.setTransform(37.4,109.8,4.188,4.188);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#906829").s().p("AgHAIQgEgFACgGQACgKAFAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_15.setTransform(36.7,108.7,4.188,4.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACACAAAHQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_16.setTransform(36.2,106.5,4.188,4.188);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#654815").s().p("AgDAJQgGgBABgFQACADABgEIACgFQACgGADADQABgDADAAQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABIABgEIgBAJIgFAHQgCADgCAAIgCgBg");
	this.shape_17.setTransform(32.4,62.2,4.188,4.188);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#906829").s().p("AgHAIQgEgFACgGQADgKAEAEIACgCIACADQAFgFADAEQACAEgCAFQgCAJgEACIgEABQgEAAgDgEg");
	this.shape_18.setTransform(31.7,61.1,4.188,4.188);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#B1833A").s().p("AgIAIQgEgGACgFQADgKAEAEIADgCIACADQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAABAAQAEAAABABQACABAAAIQgBAGgIAFQgDABgBAAQgEAAgDgEg");
	this.shape_19.setTransform(31.2,59,4.188,4.188);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#654815").s().p("AgEAKQAEACgFgMQgCgDAGgBQgBgBAAgBQAAAAAAgBQABgBAAAAQAAAAABgBQAAAAAAAAQAAgBAAABQABAAAAAAQAAABABABIgBgEIAFAJIABAHQgBAFgFACIgBAAQgDAAgBgCg");
	this.shape_20.setTransform(12.4,123.3,4.188,4.188);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#906829").s().p("AAAANQgGgDgDgHQgEgHAHAAIAAgCIABgCIAEABQAAgHAEACQADABADAGQADAIgBAEQgDAGgGAAIgCAAg");
	this.shape_21.setTransform(10.4,123,4.188,4.188);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#B1833A").s().p("AAAANQgHgCgDgHQgCgIAFAAIAAgCIABgCIAEABQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQABgDACAAQABABAFAGQAEAGgDAHQgDAHgFAAIgCgBg");
	this.shape_22.setTransform(8.9,121.6,4.188,4.188);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#654815").s().p("AgFAKQADABgBgFIgCgGQgCgDAFgBQAAgBAAgBQAAAAAAgBQABgBAAAAQABAAAAgBQAAAAAAAAQAAgBAAABQABAAAAAAQABABAAABIgBgEIAEAJIACAHQgBAFgFACIgBAAQgDAAgCgCg");
	this.shape_23.setTransform(8.7,79.1,4.188,4.188);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#906829").s().p("AAAANQgGgDgCgHQgEgHAGAAIACgEQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABABQAAgHAEACQAEACACAFQADAJgBADQgDAGgFAAIgDAAg");
	this.shape_24.setTransform(6.7,78.8,4.188,4.188);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#B1833A").s().p("AAAANQgHgCgCgHQgEgIAGAAIACgEQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAAAAAAAAQAAgBAAAAQAAgBABAAQAAgBABAAIADgDQACABAEAGQAFAGgEAHQgDAHgFAAIgCgBg");
	this.shape_25.setTransform(5.2,77.4,4.188,4.188);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#554718").s().p("AgGAQIgBgIQgEgHAAgDQABgIAEgFQAEgFADADIAAgCQAAAAAAABQABAAAAAAQABABAAAAQABAAABAAIAEABQACABABAGQABAGgFADIgEAFIgDAFQgBAIgCACQgDgBgBgDg");
	this.shape_26.setTransform(20.3,18.6,4.189,4.189,5.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#654815").s().p("AABAEIgCgDQgBAAgBgBQAAAAgBAAQAAAAAAAAQAAAAAAAAIABgDQABADAHgBQABABgBACQgBAAAAABQAAAAgBABQAAAAAAAAQgBAAAAAAIgBAAg");
	this.shape_27.setTransform(27.7,19,4.189,4.189,5.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#654815").s().p("AgEAEQgCgDAFgBQACgDACAAIACAAIgFADQgBAEgCAAIgBAAg");
	this.shape_28.setTransform(12.6,18.7,4.189,4.189,5.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#654815").s().p("AgDACQAAAAAAgBQAAAAAAgBQAAAAABAAQAAAAABAAIADgDIACADQABACgEABIgBABQgBAAAAAAQAAgBgBAAQAAAAAAAAQgBAAAAgBg");
	this.shape_29.setTransform(17.4,10,4.189,4.189,5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#654815").s().p("AACARIgBgFQgBgDgBgBQgDABgCgCIABgFQAAgMACgHQAAAAABAAQAAAAAAAAQABABAAAAQAAAAABABIAAABIAAAAIAAACIAAAFIAAAHQAEAAACAHQACAHgCAEIgEgBg");
	this.shape_30.setTransform(25.2,7.8,4.189,4.189,5.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#B1833A").s().p("AgWAYQgCgDACgDQABgDADgBIADgBQABAAAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBIABgFQAAgGAGgCIAAAAIAEgMIABgGQABgEAAgBIAAAAIAAAAIABAAQAEAAACACIABAKIABAJIAAABQAEABABACQADAEgCAEIABABIAAABQAJACgBAGQgCAGgFgDIgHgIIgEABQgDAAgDgDQgEAFgDAAIgGAFIgFAFIgBAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAgBg");
	this.shape_31.setTransform(20.7,10.2,4.189,4.189,5.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CF9F59").s().p("AAAADQgBAEgDAHQAAgJgCABIgFAFQAAgDgBgCIgBgGIAAgGQAAgEADgDQADgDAEABQADACACAEIAGAIIAGADQgFABgFgDIAGAIIgDgBQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBAAAAABIgBADIgBAFIgCgNg");
	this.shape_32.setTransform(41.9,168.1,4.189,4.189,5.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CF9F59").s().p("AgBAIIgBgFIAAgJIABgGQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAAAIACABQAAAAABABQAAAAAAAAQAAABAAAAQAAABAAAAQAAACgDAEQAAADAAAKIgBgFg");
	this.shape_33.setTransform(9.9,120.2,4.189,4.189,5.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CF9F59").s().p("AgGALQACgFADgCQACgCABgCIABgSQABACAAAGIADALQAAADgEAEIgDAFIAAAFQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQgCAAgCAEIABgIg");
	this.shape_34.setTransform(9,54.6,4.189,4.189,5.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#CF9F59").s().p("AABAaIgBgJIgBgHIgEAGIABgLIABgIQgBADgCgCQgBgBAAgEIABgJIAAgJIABgIIABABQAKAJACAXQABAEgDANIgBAIIABAJQgDgCgCgGg");
	this.shape_35.setTransform(15.9,91.1,4.189,4.189,5.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#654815").s().p("AAGCLIgCgHQAHAKAGgGQAHgHAAgIIgCgQQgDgMgHAGQAAgCACgEIACgFQAAgDgDgDQgDgDgDADQgFAGAAANQgCgNAAgOIAAgOIAAgHIAAgOQAAgUgKgVIgFgMIAKAJQADAAgCgHIgDgHIADAAQgEgNgOgJIAFAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAAAgBQABgEgHgKIgCgDIgEgKIgDgMIgBghQAAgJADgIQAGgNAEgBIgEASQgDAVACANIACARIAFAMQADAIABABIAJAFQAFAGAEAUQACAGgBAPQAAAEADAGIAFANQAJAUgCAcQgBANADAMQAFANgBALQgBAPgEAFQgEAFgNAGIgCgIg");
	this.shape_36.setTransform(26.6,78.1,4.189,4.189,5.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#B1833A").s().p("AAAB/QgBgGgCgSQgBgIgGgNQgGgOgCgGQgEgXACgWQADgOgBgEQAAgDgEgGQgEgFABgEQABgNADgIQABgCAEgDIAFgFQAEgHgDgJIgCgUIgBgSIABgTQABgPAEgEQABgBAAAAQABgBAAAAQABAAAAAAQABAAAAABQABACgBAEQgDAVAAAQQAAAIACAPQABAJADAQIAAAHQAAAGABAGIAFAJQAEAEABAEIAFASIAIAWQADAIgBAQIgDALQgBAGADALIAAAFIABAIQgBACgEgDQgFgEgCAEQgDAEAEAEQAFAHAAABQgGgDgEAGQgBACAAAKQAAAGACAHIADANQgKgDgEgPg");
	this.shape_37.setTransform(13.1,77.2,4.189,4.189,5.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#906829").s().p("AAbAgIgEgJQgEgGgIgFIgMgIIgIgFQgLgRgLgGIAYgNQABADgEAIQgFAJAKAMQAFAFAGAEIAIAFQAFADACADQAGAIACAEQAEAJgBACQgCAAgDgGg");
	this.shape_38.setTransform(28.4,150,4.189,4.189,5.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#906829").s().p("AgICTQgJgJgCgUQgDgVgGgMQgQgfAIgvQABgEgCgDIgEgFIgCgHQAAgEACgLQABgIAEgFIAEgDIADgEQABgFgBgKIgBgfIgBgYQAJgnAIAGQAEACACALIgBATQgBAaABAFIADAPQAGASAFAEQAGADADAEIAFALQAFALgBAHIgBASQACAIAIANQAFAMABAOQABAGgBAQQgCAMAEAJQAFAOgCASQgCAUgNACIgMgBQgEAAgEACIgGADIgCAAQgDAAgFgFg");
	this.shape_39.setTransform(20.8,76,4.189,4.189,5.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#554718").s().p("AgJAOIAAgHIgCgLQACgIAEgEQAFgEAEAEIgBgDQABAFAHABQABABAAAFQAAAGgFADIgFAEIgCAEIgGAKQgDgBAAgFg");
	this.shape_40.setTransform(13.1,22.3,4.187,4.187,-18.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#654815").s().p("AAAAEIgCgDQgBgBAAAAQAAAAgBAAQAAAAAAAAQAAgBAAAAIACgDQgBAEAIAAQABAAgCADQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAgBAAIgBgBg");
	this.shape_41.setTransform(20.1,21,4.187,4.187,-18.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#654815").s().p("AgFADQAAgDAEAAQADgDACABIACAAIgGADQgBACgCAAIgCAAg");
	this.shape_42.setTransform(5.3,24.7,4.187,4.187,-18.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#654815").s().p("AgDABQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAABgBIADgBIABACQABACgFABIAAAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBg");
	this.shape_43.setTransform(7.6,15.1,4.187,4.187,-18.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#654815").s().p("AABARIgBgFQAAgDgBgCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAAAAAgBIAAgCIABgDIADgIIABgKQAAAAAAAAQAAAAABAAQAAAAAAABQAAAAABABIAAgBIgBAQQAFABABAHQAAAHgCADIgEgBg");
	this.shape_44.setTransform(14.1,11.1,4.187,4.187,-18.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#B1833A").s().p("AARAXIgGgIIgEAAQgEgBgDgDQgDAFgDgCIgIAFIgFAEQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAgBgBgBQgBgDACgDQABgDAEAAQAEAAABgCIACgFQAAgEAHgCIABgBIADgMQAEgIACgBIABAAQAFABAAACIAAAUQADABABACQADAFgDADIABACIAAABQAIAEgDAFQAAABAAAAQgBABAAAAQgBABAAAAQAAAAgBAAIgDgCg");
	this.shape_45.setTransform(9.6,14.6,4.187,4.187,-18.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#CF9F59").s().p("AAFAEIgDAFIgBAGIgBgEQAAAAAAgBQAAAAAAAAQAAAAAAAAQAAAAAAAAQgFADgDAAIAIgLQgFACgGAAQAIgEgCAAIgIgBIAFgDIAFgFIADgEQAEgCAEABQAEABABAEIAAAJIgEAIIgBAHQgDgDAAgIg");
	this.shape_46.setTransform(24,170.3,4.187,4.187,-18.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#CF9F59").s().p("AgFAIIAFgOIACgFIACADIACABIgBADQgBADgEABQgBAEgEAIIAAgEg");
	this.shape_47.setTransform(11.4,120,4.187,4.187,-18.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#CF9F59").s().p("AgFAKQgBAAgEADQAFgJADgCIAFgCQAFAAAAgDIADgLQAAAOgCACIgCAEIgEABQgCABgBACIgDAFQAAgBAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAg");
	this.shape_48.setTransform(6.9,56,4.187,4.187,-18.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#CF9F59").s().p("AgGAQQACgGgBgCIgFAEIAFgJIAEgHQgCAEgBgFIABgFIADgIIADgIIAEgHIABABQAGAMgGAWIgIARIgGAPQgBgDABgPg");
	this.shape_49.setTransform(16.2,91.1,4.187,4.187,-18.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#654815").s().p("AgiCBIACgHQACALAIgDQAJgCADgJQAKgbgNACIAEgEIAEgFQABgDgBgDQgCgDgDABQgGACgGANQABgOAEgMIAJgUIAHgLQAFgUgCgXIABgOIAFANQAEABAAgHIAAgIQAAAAAAABQABAAAAAAQAAAAABAAQAAAAAAAAQACgMgKgQIAFADQABAAAAABQABAAAAAAQABAAAAgBQABAAAAgBQADgEgEgKIgBgLQgBgGAEgRQACgKAEgMQAGgOAHgDQABADgDAGIgEAKQgDAFgDAOQgBAHAAAPQAAAOABACIAGAIQAEAIgDAUQgBAIgHANQgBADAAAUQAAAYgMAZQgGAMgCALQgBANgCALQgIAOgFADQgEACgPAAIAAgHg");
	this.shape_50.setTransform(21.9,80.2,4.187,4.187,-18.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#B1833A").s().p("AgqBwQABgGAFgSQACgHAAgOQgBgQABgGQAFgZAJgSQAJgJABgGIAAgKQgCgGACgEQAHgMADgGQACgCAHgCQAGgBABgEIACgIIAAgIIAFgWQAKgnAKADQABAFgHAOIgGATQgDAOgEAbIgEAUIACAKIAAAJIgDATIAAAXQgBAKgHANQgBADgFAGQgCADgBAFIgCAJIAAAFIgBADIgBAEQgCACgDgFQgCgFgEACQgEADABAGIACAJQgEgFgGAEIgGAKIgFAbQgIgHABgQg");
	this.shape_51.setTransform(11.5,80,4.187,4.187,-18.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#906829").s().p("AgGAWQAFgGABgMQABgHgBgNIgBgIQgBgDgGgGIAZgDQAAACgDADIgEAEQgEADgBAFIAAANQAAAGgBAEQgDAOgCAGQgFAFgKAIQAAgDAKgMg");
	this.shape_52.setTransform(26.3,150.4,4.187,4.187,-18.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#906829").s().p("AgMCSIgLgGQgEgBgEAAIgHABQgGgBgDgJQgFgMAFgTQAGgUgBgNQgDgiAZgoQADgDgBgEIgBgIIABgHIAHgNQAFgHAFgDIACgBQABAAABAAQAAgBABAAQAAAAABAAQAAgBABAAQACgBACgKIAGgjIAJgXQAMgcAJAKQAJAKgJAUQgEAGgCAQQgEANAAAKQgDAPAEAEIAGAHQADAFAAAOQABAKgEAIIgFAHQgDAFAAAEQgCAIADAPQAAANgEAOQgCAGgHANQgGALAAAJQAAAPgJARQgIAOgIAAIgEAAg");
	this.shape_53.setTransform(14.5,78.5,4.187,4.187,-18.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:0,x:20,y:150.1}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:0,x:9.8,y:171.3}},{t:this.shape_5,p:{rotation:0,x:26.3,y:11.1}},{t:this.shape_4,p:{rotation:0,x:33.8,y:9.5}},{t:this.shape_3,p:{rotation:0,x:26.3,y:8.9}},{t:this.shape_2,p:{rotation:0,x:19,y:14.9}},{t:this.shape_1,p:{rotation:0,x:33.2,y:20.6}},{t:this.shape,p:{rotation:0,x:26.9,y:17.3}}]}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{rotation:-29,x:24.4,y:148.5}},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{rotation:-14.7,x:25.3,y:169.8}},{t:this.shape_5,p:{rotation:-20.2,x:22.1,y:11.6}},{t:this.shape_4,p:{rotation:-20.2,x:28.6,y:7.5}},{t:this.shape_3,p:{rotation:-20.2,x:21.3,y:9.5}},{t:this.shape_2,p:{rotation:-20.2,x:16.5,y:17.7}},{t:this.shape_1,p:{rotation:-20.2,x:31.8,y:18.2}},{t:this.shape,p:{rotation:-20.2,x:24.8,y:17.2}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:28.4,y:150}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:41.9,y:168.1}},{t:this.shape_31,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.7,y:10.2}},{t:this.shape_30,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:25.2,y:7.8}},{t:this.shape_29,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:17.4,y:10}},{t:this.shape_28,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:12.6,y:18.7}},{t:this.shape_27,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:27.7,y:19}},{t:this.shape_26,p:{scaleX:4.189,scaleY:4.189,rotation:5.7,x:20.3}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_39},{t:this.shape_38,p:{scaleX:4.188,scaleY:4.188,rotation:27.7,x:24.8,y:149.7}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:33.5,y:168.8}},{t:this.shape_31,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.2,y:10.1}},{t:this.shape_30,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:23.5,y:7.3}},{t:this.shape_29,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:15.9,y:10.3}},{t:this.shape_28,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:12.1,y:19.5}},{t:this.shape_27,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:27.1,y:18.2}},{t:this.shape_26,p:{scaleX:4.188,scaleY:4.188,rotation:-0.3,x:19.7}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:26.3,y:150.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:24}},{t:this.shape_45,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:9.6,y:14.6}},{t:this.shape_44,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:14.1,y:11.1}},{t:this.shape_43,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:7.6,y:15.1}},{t:this.shape_42,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:5.3,y:24.7}},{t:this.shape_41,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:20.1,y:21}},{t:this.shape_40,p:{scaleX:4.187,scaleY:4.187,rotation:-18.7,x:13.1}}]},6).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_53},{t:this.shape_52,p:{scaleX:4.186,scaleY:4.186,rotation:-1.7,x:23.6,y:151.4}},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46,p:{scaleX:4.186,scaleY:4.186,rotation:-0.7,x:15.8}},{t:this.shape_45,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:14.6,y:13.8}},{t:this.shape_44,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:20.1,y:12.2}},{t:this.shape_43,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:12.6,y:13.6}},{t:this.shape_42,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:7.1,y:21.8}},{t:this.shape_41,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:22.2,y:23.5}},{t:this.shape_40,p:{scaleX:4.186,scaleY:4.186,rotation:1.8,x:15.3}}]},6).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,41.5,178.2);


(lib.map_mc = function() {
	this.initialize();

	// Layer 1
	this.car_mc = new lib.mapcarMC();
	this.car_mc.setTransform(74.2,225.8,1,1,0,0,0,3,3);

	this.instance = new lib.map();
	this.instance.setTransform(0,0,1.068,1.041);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("AAoAAQAAARgNAMQgLAMgQAAQgPAAgMgMQgMgMAAgRQAAgPAMgNQAMgMAPAAQAQAAALAMQANANAAAPg");
	this.shape.setTransform(72.5,224.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbAdQgMgMABgRQgBgQAMgMQAMgMAPAAQAQAAALAMQAMAMAAAQQAAARgMAMQgLAMgQAAQgPAAgMgMg");
	this.shape_1.setTransform(72.5,224.6);

	this.addChild(this.shape_1,this.shape,this.instance,this.car_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,157,251);


(lib.camels_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.camelanim2("single",0);
	this.instance.setTransform(72.7,44.5,1,1,-6,0,0,20.7,89);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({mode:"synched",loop:false},0).wait(94));

	// Layer 4
	this.instance_1 = new lib.camelanim2("single",24);
	this.instance_1.setTransform(-20.4,-20.6,1,1,18.7,0,0,20.7,89.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(30).to({mode:"synched"},0).wait(36).to({mode:"single"},0).wait(44));

	// Layer 2
	this.instance_2 = new lib.camelanim3("single",12);
	this.instance_2.setTransform(24.4,12.4,1,1,-4.7,0,0,20.8,89.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({mode:"synched"},0).wait(42).to({mode:"single"},0).wait(61));

	// camel 1
	this.instance_3 = new lib.camelanim("synched",0,false);
	this.instance_3.setTransform(-75.7,12.4,1,1,0,0,0,20.8,89.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(36).to({mode:"single"},0).wait(18).to({mode:"synched",loop:false},0).wait(36).to({mode:"single"},0).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.1,-105.8,208,243);


(lib.roadBg = function() {
	this.initialize();

	// Layer 5
	this.land_mc = new lib.land_mc();
	this.land_mc.setTransform(1313.9,2217.3,1,1,0,0,0,1314.8,2209.1);

	// Layer 3
	this.time3_mc = new lib.time_bonus_1();
	this.time3_mc.setTransform(2007.8,4744.9,1,1,0,0,0,57.5,56);

	this.time2_mc = new lib.time_bonus_1();
	this.time2_mc.setTransform(1878.9,4744.9,1,1,0,0,0,57.5,56);

	this.time1_mc = new lib.time_bonus_1();
	this.time1_mc.setTransform(966.9,4762.2,1,1,0,0,0,57.5,56);

	this.tree_mc = new lib.tree_1();
	this.tree_mc.setTransform(1657.1,4718.5,0.85,0.85,0,0,0,6,-0.8);

	this.camels_mc = new lib.camels_1();
	this.camels_mc.setTransform(1386.5,4729.7,0.85,0.85,0,0,0,2.4,2);

	this.mummy_mc = new lib.mummy_1();
	this.mummy_mc.setTransform(1126.1,4754.4,0.85,0.85,0,0,0,0.4,-1.6);

	// Layer 4
	this.sea_mc = new lib.seaMC();
	this.sea_mc.setTransform(1313.9,2217.3,1,1,0,0,0,1314.8,2209.1);

	// Layer 2
	this.road_mc = new lib.roads_mc();
	this.road_mc.setTransform(1313.9,2217.3,1,1,0,0,0,1314.8,2209.1);

	// Layer 1
	this.instance = new lib.bg();
	this.instance.setTransform(206.5,329.5,0.85,0.85);

	this.addChild(this.instance,this.road_mc,this.sea_mc,this.mummy_mc,this.camels_mc,this.tree_mc,this.time1_mc,this.time2_mc,this.time3_mc,this.land_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(196.4,310.2,2381.6,4532.5);


(lib.road = function() {
	this.initialize();

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Eg7qAxxMAAAhjhMB3VAAAMAAABjhg");
	mask.setTransform(382,318.5);

	// Layer 3
	this.car_mc = new lib.car_2();
	this.car_mc.setTransform(372.3,466);

	this.car_mc.mask = mask;

	// Layer 2
	this.bg_mc = new lib.roadBg();
	this.bg_mc.setTransform(362.1,-1276.5,1,1,0,0,0,1388.5,2332.5);

	this.bg_mc.mask = mask;

	this.addChild(this.bg_mc,this.car_mc);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,764,637);


// stage content:
(lib.driving02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		initGame();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 9
	this.message_mc = new lib.message_1();
	this.message_mc.setTransform(263.9,245.5,1,1,0,0,0,100,78);
	this.message_mc.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.message_mc).wait(1));

	// Layer 8
	this.map_mc = new lib.map_mc();
	this.map_mc.setTransform(856,328.5,1,1,0,0,0,73.5,120.5);

	this.timeline.addTween(cjs.Tween.get(this.map_mc).wait(1));

	// Layer 7
	this.sfx_mc = new lib.sfx();
	this.sfx_mc.setTransform(1185.7,158.7);

	this.timeline.addTween(cjs.Tween.get(this.sfx_mc).wait(1));

	// Layer 5
	this.distance_txt = new cjs.Text("313m", "bold 24px 'Thomas Headline'", "#D40000");
	this.distance_txt.name = "distance_txt";
	this.distance_txt.lineHeight = 21;
	this.distance_txt.lineWidth = 153;
	this.distance_txt.setTransform(795,160.3);

	this.text = new cjs.Text("TO CAIRO", "bold 16px 'Thomas Headline'", "#FFFFFF");
	this.text.lineHeight = 13;
	this.text.lineWidth = 153;
	this.text.setTransform(795,140);

	this.lives_mc = new lib.heart_1();
	this.lives_mc.setTransform(801,122.8,1,1,0,0,0,11,11);

	this.text_1 = new cjs.Text("LIVES", "bold 18px 'Thomas Headline'", "#FFFFFF");
	this.text_1.lineHeight = 15;
	this.text_1.lineWidth = 153;
	this.text_1.setTransform(795,92.8);

	this.time_txt = new cjs.Text("0:00", "bold 24px 'Thomas Headline'", "#D40000");
	this.time_txt.name = "time_txt";
	this.time_txt.lineHeight = 21;
	this.time_txt.lineWidth = 153;
	this.time_txt.setTransform(795,58.3);

	this.text_2 = new cjs.Text("TIME", "bold 18px 'Thomas Headline'", "#FFFFFF");
	this.text_2.lineHeight = 15;
	this.text_2.lineWidth = 153;
	this.text_2.setTransform(795,38.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.time_txt},{t:this.text_1},{t:this.lives_mc},{t:this.text},{t:this.distance_txt}]}).wait(1));

	// Layer 3
	this.game_mc = new lib.road();
	this.game_mc.setTransform(380.9,318.5,1,1,0,0,0,381.9,318.5);

	this.timeline.addTween(cjs.Tween.get(this.game_mc).wait(1));

	// Layer 4
	this.start_btn = new lib.start_btn();
	this.start_btn.setTransform(858.4,544.5,0.63,0.63,0,0,0,56.5,56.5);
	new cjs.ButtonHelper(this.start_btn, 0, 1, 2, false, new lib.start_btn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.start_btn).wait(1));

	// Layer 1
	this.instance = new lib.heart();
	this.instance.setTransform(253,311);

	this.instance_1 = new lib.heart();
	this.instance_1.setTransform(253,311);

	this.down_btn = new lib.down_btn();
	this.down_btn.setTransform(857.4,601,1,1,0,0,0,17.5,19.5);
	new cjs.ButtonHelper(this.down_btn, 0, 1, 2);

	this.right_btn = new lib.right_btn();
	this.right_btn.setTransform(912.9,544.5,1,1,0,0,0,19.5,17.5);
	new cjs.ButtonHelper(this.right_btn, 0, 1, 2);

	this.left_btn = new lib.left_btn();
	this.left_btn.setTransform(801.9,544.5,1,1,0,0,0,19.5,17.5);
	new cjs.ButtonHelper(this.left_btn, 0, 1, 2);

	this.up_btn = new lib.up_btn();
	this.up_btn.setTransform(855.4,487,1,1,0,0,0,19.5,17.5);
	new cjs.ButtonHelper(this.up_btn, 0, 1, 2);

	this.instance_2 = new lib.heart2();
	this.instance_2.setTransform(687.9,185);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.up_btn},{t:this.left_btn},{t:this.right_btn},{t:this.down_btn},{t:this.instance_1},{t:this.instance}]}).wait(1));

	// Layer 6
	this.instance_3 = new lib.sidepanel();
	this.instance_3.setTransform(763,0,1.005,1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-351.1,-2980.3,2381.6,4532.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;